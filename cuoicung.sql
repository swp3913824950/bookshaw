USE [master]
GO
use master
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = N'BookShop')
BEGIN
	ALTER DATABASE [BookShop] SET OFFLINE WITH ROLLBACK IMMEDIATE;
	ALTER DATABASE [BookShop] SET ONLINE;
	DROP DATABASE [BookShop];
END

GO

/****** Object:  Database [BookShop]    Script Date: 10/23/2023 11:16:31 AM ******/
CREATE DATABASE [BookShop]
GO
ALTER DATABASE [BookShop] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BookShop].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BookShop] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BookShop] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BookShop] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BookShop] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BookShop] SET ARITHABORT OFF 
GO
ALTER DATABASE [BookShop] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BookShop] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BookShop] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BookShop] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BookShop] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BookShop] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BookShop] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BookShop] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BookShop] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BookShop] SET  ENABLE_BROKER 
GO
ALTER DATABASE [BookShop] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BookShop] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BookShop] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BookShop] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BookShop] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BookShop] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BookShop] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BookShop] SET RECOVERY FULL 
GO
ALTER DATABASE [BookShop] SET  MULTI_USER 
GO
ALTER DATABASE [BookShop] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BookShop] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BookShop] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BookShop] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BookShop] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [BookShop] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'BookShop', N'ON'
GO
ALTER DATABASE [BookShop] SET QUERY_STORE = ON
GO
ALTER DATABASE [BookShop] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [BookShop]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[accountID] [int] IDENTITY(1,1) NOT NULL,
	[fullname] [nvarchar](50) NULL,
	[dob] [date] NULL,
	[phone] [nvarchar](15) NULL,
	[address] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL,
	[username] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[accountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountRole]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountRole](
	[roleID] [int] NULL,
	[accountID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cart]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cart](
	[cartID] [int] IDENTITY(1,1) NOT NULL,
	[customerID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[cartID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CartDetail]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CartDetail](
	[cartID] [int] NULL,
	[productID] [int] NULL,
	[quantity] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[cateID] [int] IDENTITY(1,1) NOT NULL,
	[cateName] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[cateID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comment](
	[commentID] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](50) NULL,
	[customerID] [int] NULL,
	[productID] [int] NULL,
	[date] [date] NULL,
	[star] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[commentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Feedback]    Script Date: 10/21/2023 4:42:15 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedback](
	[feedbackID] [int] IDENTITY(1,1) NOT NULL,
	[content] [nvarchar](50) NULL,
	[customerID] [int] NULL,
	[orderID] [int] NULL,
	[date] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[feedbackID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[customerID] [int] IDENTITY(1,1) NOT NULL,
	[accountID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[customerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerOrder]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerOrder](
	[orderID] [int] IDENTITY(1,1) NOT NULL,
	[customerID] [int] NULL,
	[sellerID] [int] NULL,
	[shipAddress] [nvarchar](50) NULL,
	[orderDate] [date] NULL,
	[requiredDate] [date] NULL,
	[shippedDate] [date] NULL,
	[status] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[orderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Manager]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Manager](
	[managerID] [int] IDENTITY(1,1) NOT NULL,
	[salary] [decimal](10, 2) NULL,
	[accountID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[managerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[productID] [int] NULL,
	[orderID] [int] NULL,
	[price] [decimal](10, 2) NULL,
	[quantity] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[productID] [int] IDENTITY(1,1) NOT NULL,
	[proName] [nvarchar](50) NULL,
	[authorName] [nvarchar](50) NULL,
	[maniDate] [date] NULL,
	[discount] [decimal](10, 2) NULL,
	[price] [decimal](10, 2) NULL,
	[quantity] [int] NULL,
	[categoryID] [int] NULL,
	[sellerID] [int] NULL,
	[image_url] [nvarchar](max) NULL,
 CONSTRAINT [PK__Product__2D10D14ADDE4E54A] PRIMARY KEY CLUSTERED 
(
	[productID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[roleID] [int] IDENTITY(1,1) NOT NULL,
	[roleName] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[roleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Seller]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Seller](
	[sellerID] [int] IDENTITY(1,1) NOT NULL,
	[salary] [decimal](10, 2) NULL,
	[accountID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[sellerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 10/23/2023 11:16:31 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[status] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([accountID], [fullname], [dob], [phone], [address], [password], [username], [email]) VALUES (9, N'Lê Ngọc Anh', CAST(N'1990-01-01' AS Date), N'0987654321', N'123 Main Street', N'password1', N'BanhMiFan88', N'BanhMi@example.com')
INSERT [dbo].[Account] ([accountID], [fullname], [dob], [phone], [address], [password], [username], [email]) VALUES (10, N'Nguyễn Quang Minh', CAST(N'1991-02-02' AS Date), N'0123456789', N'456 Elm Street', N'password2', N'AoDaiLover27', N'AoDai@example.com')
INSERT [dbo].[Account] ([accountID], [fullname], [dob], [phone], [address], [password], [username], [email]) VALUES (11, N'Trần Thanh Hương', CAST(N'1990-01-01' AS Date), N'0901122334', N'123 Main Street', N'password3', N'HanoiHiker', N'Hanoi@example.com')
INSERT [dbo].[Account] ([accountID], [fullname], [dob], [phone], [address], [password], [username], [email]) VALUES (12, N'Dương Quang Tuấn', CAST(N'1991-02-02' AS Date), N'0888777666', N'456 Elm Street', N'password4', N'DuongTuan', N'tuandqhe176834@fpt.edu.vn')
INSERT [dbo].[Account] ([accountID], [fullname], [dob], [phone], [address], [password], [username], [email]) VALUES (13, N'Võ Trung Kiên', CAST(N'1992-03-03' AS Date), N'0777888999', N'789 Oak Street', N'password5', N'RiverRun', N'RiverRun@example.com')
INSERT [dbo].[Account] ([accountID], [fullname], [dob], [phone], [address], [password], [username], [email]) VALUES (14, N'Đặng Thu Hà', CAST(N'1993-04-04' AS Date), N'0912345678', N'101 Pine Street', N'password6', N'MoonStar', N'MoonStar@example.com')
INSERT [dbo].[Account] ([accountID], [fullname], [dob], [phone], [address], [password], [username], [email]) VALUES (15, N'Bùi Văn Đức', CAST(N'1994-05-05' AS Date), N'0898989898', N'202 Birch Street', N'password7', N'GreenTea', N'GreenTea@example.com')
INSERT [dbo].[Account] ([accountID], [fullname], [dob], [phone], [address], [password], [username], [email]) VALUES (16, N'Nguyễn Thị Lan', CAST(N'1995-06-06' AS Date), N'0969696969', N'303 Maple Street', N'password8', N'StarFish', N'StarFish@example.com')
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
INSERT [dbo].[AccountRole] ([roleID], [accountID]) VALUES (2, 9)
INSERT [dbo].[AccountRole] ([roleID], [accountID]) VALUES (2, 10)
INSERT [dbo].[AccountRole] ([roleID], [accountID]) VALUES (2, 11)
INSERT [dbo].[AccountRole] ([roleID], [accountID]) VALUES (1, 12)
INSERT [dbo].[AccountRole] ([roleID], [accountID]) VALUES (1, 13)
INSERT [dbo].[AccountRole] ([roleID], [accountID]) VALUES (1, 14)
INSERT [dbo].[AccountRole] ([roleID], [accountID]) VALUES (1, 15)
INSERT [dbo].[AccountRole] ([roleID], [accountID]) VALUES (4, 16)
GO
SET IDENTITY_INSERT [dbo].[Cart] ON 

INSERT [dbo].[Cart] ([cartID], [customerID]) VALUES (2, 1)
INSERT [dbo].[Cart] ([cartID], [customerID]) VALUES (3, 2)
INSERT [dbo].[Cart] ([cartID], [customerID]) VALUES (4, 3)
INSERT [dbo].[Cart] ([cartID], [customerID]) VALUES (5, 4)
SET IDENTITY_INSERT [dbo].[Cart] OFF
GO
INSERT [dbo].[CartDetail] ([cartID], [productID], [quantity]) VALUES (2, 317, 2)
INSERT [dbo].[CartDetail] ([cartID], [productID], [quantity]) VALUES (3, 318, 2)
INSERT [dbo].[CartDetail] ([cartID], [productID], [quantity]) VALUES (4, 319, 2)
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (1, N'Kinh dị, giật gân')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (2, N'Khoa học viễn tưởng')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (3, N'Văn học Việt Nam')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (4, N'Phát triển bản thân')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (5, N'Kinh tế')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (6, N'Tiểu thuyết')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (7, N'Tự truyện, hồi ký')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (8, N'Sách học tiếng Anh')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (9, N'Tâm lý học')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (10, N'Quản trị - lãnh dạo')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (11, N'Nuôi dạy con')
INSERT [dbo].[Category] ([cateID], [cateName]) VALUES (12, N'Truyện tranh')
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([customerID], [accountID]) VALUES (1, 12)
INSERT [dbo].[Customer] ([customerID], [accountID]) VALUES (2, 13)
INSERT [dbo].[Customer] ([customerID], [accountID]) VALUES (3, 14)
INSERT [dbo].[Customer] ([customerID], [accountID]) VALUES (4, 15)
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[CustomerOrder] ON 

INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (1, 1, 3, N'456 Elm Street', CAST(N'2023-09-19' AS Date), CAST(N'2023-09-25' AS Date), CAST(N'2023-09-22' AS Date), N'Shipping')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (2, 1, 3, N'456 Elm Street', CAST(N'2023-09-19' AS Date), CAST(N'2023-09-25' AS Date), CAST(N'2023-09-22' AS Date), N'Accepted')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (3, 1, 3, N'456 Elm Street', CAST(N'2023-09-19' AS Date), CAST(N'2023-09-25' AS Date), CAST(N'2023-09-22' AS Date), N'Rejected')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (4, 2, 2, N'789 Oak Street', CAST(N'2023-09-20' AS Date), CAST(N'2023-09-26' AS Date), CAST(N'2023-09-23' AS Date), N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (5, 3, 3, N'101 Pine Street', CAST(N'2023-09-21' AS Date), CAST(N'2023-09-27' AS Date), CAST(N'2023-09-24' AS Date), N'Processing')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (6, 4, 1, N'202 Birch Street', CAST(N'2023-09-22' AS Date), CAST(N'2023-09-28' AS Date), CAST(N'2023-09-25' AS Date), N'Shipping')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (7, 4, 1, N'202 Birch Street', CAST(N'2023-09-22' AS Date), CAST(N'2023-09-28' AS Date), CAST(N'2023-09-25' AS Date), N'Accepted')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (8, 4, 1, N'202 Birch Street', CAST(N'2023-09-22' AS Date), CAST(N'2023-09-28' AS Date), CAST(N'2023-09-25' AS Date), N'Rejected')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (9, 1, 2, N'456 Elm Street', CAST(N'2023-09-23' AS Date), CAST(N'2023-09-29' AS Date), CAST(N'2023-09-26' AS Date), N'Processing')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (10, 2, 2, N'789 Oak Street', CAST(N'2023-09-23' AS Date), CAST(N'2023-09-29' AS Date), CAST(N'2023-09-26' AS Date), N'Shipping')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (11, 3, 2, N'101 Pine Street', CAST(N'2023-09-23' AS Date), CAST(N'2023-09-29' AS Date), CAST(N'2023-09-26' AS Date), N'Accepted')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (12, 4, 1, N'202 Birch Street', CAST(N'2023-09-24' AS Date), CAST(N'2023-09-30' AS Date), CAST(N'2023-09-27' AS Date), N'Processing')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (13, 1, 1, N'456 Elm Street', CAST(N'2023-09-24' AS Date), CAST(N'2023-09-30' AS Date), CAST(N'2023-09-27' AS Date), N'Shipping')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (14, 2, 1, N'789 Oak Street', CAST(N'2023-09-24' AS Date), CAST(N'2023-09-30' AS Date), CAST(N'2023-09-27' AS Date), N'Accepted')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (15, 3, 3, N'101 Pine Street', CAST(N'2023-09-25' AS Date), CAST(N'2023-10-01' AS Date), CAST(N'2023-09-28' AS Date), N'Rejected')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (16, 1, 1, N'456 Elm Street', '2022-08-16', '2022-08-22', '2022-08-19', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (17, 2, 2, N'789 Oak Street', '2022-09-16', '2022-09-22', '2022-09-19', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (18, 3, 3, N'101 Pine Street', '2022-10-16', '2022-10-22', '2022-10-19', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (19, 4, 1, N'202 Birch Street', '2022-08-16', '2022-08-22', '2022-08-19', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (20, 1, 1, N'456 Elm Street', '2022-09-17', '2022-09-23', '2022-09-20', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (21, 1, 2, N'456 Elm Street', '2022-10-18', '2022-10-24', '2022-10-21', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (22, 1, 3, N'456 Elm Street', '2022-11-10', '2022-11-13', '2022-11-14', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (23, 1, 1, N'456 Elm Street', '2022-09-20', '2022-09-26', '2022-09-23', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (24, 2, 1, N'789 Oak Street', '2022-09-17', '2022-09-23', '2022-09-20', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (25, 2, 2, N'789 Oak Street', '2022-10-18', '2022-10-24', '2022-10-21', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (26, 2, 3, N'789 Oak Street', '2022-08-19', '2022-08-25', '2022-08-22', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (27, 2, 1, N'789 Oak Street', '2022-11-11', '2022-11-14', '2022-11-15', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (28, 3, 1, N'101 Pine Street', '2022-09-17', '2022-09-23', '2022-09-20', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (29, 3, 2, N'101 Pine Street', '2022-10-18', '2022-10-24', '2022-10-21', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (30, 3, 3, N'101 Pine Street', '2022-08-19', '2022-08-25', '2022-08-22', N'Received')
INSERT [dbo].[CustomerOrder] ([orderID], [customerID], [sellerID], [shipAddress], [orderDate], [requiredDate], [shippedDate], [status]) VALUES (31, 3, 1, N'101 Pine Street', '2022-09-20', '2022-09-26', '2022-09-23', N'Received')

SET IDENTITY_INSERT [dbo].[CustomerOrder] OFF
GO
SET IDENTITY_INSERT [dbo].[Manager] ON 

SET IDENTITY_INSERT [dbo].[Manager] OFF
GO
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (319, 3, CAST(60000.00 AS Decimal(10, 2)), 1)
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (317, N'Sharp Objects', N'Gillian Flynn', CAST(N'2002-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(25000.00 AS Decimal(10, 2)), 100, 1, 1, N'https://target.scene7.com/is/image/Target/GUEST_2e2555d1-18db-4bd6-b2f6-3ee38da410c4?wid=488&hei=488&fmt=pjpeg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (318, N'Big Little Lies', N' Liane Moriarty', CAST(N'2013-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(40000.00 AS Decimal(10, 2)), 120, 1, 1, N'https://static.ybox.vn/2021/11/4/1637251196446-9781405916363.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (319, N'Alias Grace', N'Margaret Atwood', CAST(N'2023-06-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(30000.00 AS Decimal(10, 2)), 110, 1, 1, N'https://images-na.ssl-images-amazon.com/images/I/61AbK1ZUFrL.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (320, N'The Haunting of Hill House', N'Shirley Jackson', CAST(N'2023-09-14' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(45000.00 AS Decimal(10, 2)), 111, 1, 1, N'https://static.ybox.vn/2021/11/4/1637251410741-91aLs7he6GL.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (321, N'The Snowman', N'Jo Nesbø', CAST(N'2023-09-15' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(50000.00 AS Decimal(10, 2)), 102, 1, 1, N'https://trangtrinhtham.files.wordpress.com/2016/05/the_snowman_jo_nesbo.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (322, N'Misery', N'Stephen King', CAST(N'2023-09-11' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(55000.00 AS Decimal(10, 2)), 104, 1, 1, N'https://images-na.ssl-images-amazon.com/images/I/91OarMckmbL.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (323, N'Into The Water', N'Paula Hawkins', CAST(N'2023-09-12' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(65000.00 AS Decimal(10, 2)), 141, 1, 1, N'http://sun9-72.userapi.com/sun9-12/impf/c639120/v639120533/2f374/7hBYMdxcZeA.jpg?size=400x604&quality=96&sign=cbe7abae4839705edb90d64bb4a55949&type=album')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (324, N'The Woman in the Window', N'A.J. Finn', CAST(N'2023-09-13' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(70000.00 AS Decimal(10, 2)), 150, 1, 1, N'https://static.ybox.vn/2021/11/4/1637251761793-81D7dAhez6L.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (325, N'We Need to Talk About Kevin', N'Gillian Flynn', CAST(N'2023-09-19' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(75000.00 AS Decimal(10, 2)), 300, 1, 1, N'https://static.ybox.vn/2021/11/4/1637251852748-51oe50NCIGL.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (326, N'Room', N'Gillian Flynn', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(40000.00 AS Decimal(10, 2)), 100, 1, 1, N'https://images-na.ssl-images-amazon.com/images/I/71CYhjhgzpL.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (327, N'The Last Mrs. Parrish', N'Liv Constantine', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(40000.00 AS Decimal(10, 2)), 100, 1, 1, N'https://static.ybox.vn/2021/11/4/1637252247612-71fw4MYHYlL.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (337, N'Chí Phèo', N'Nam Cao', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 3, 1, N'https://ghiensach.com/wp-content/uploads/2021/11/tac-pham-van-hoc-noi-tieng-cua-viet-nam.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (338, N'Những Ngày Thơ Ấu', N'Nguyên Hương', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 3, 1, N'https://ghiensach.com/wp-content/uploads/2021/11/nhung-tac-pham-van-hoc-noi-tieng-cua-viet-nam.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (339, N'Cánh Đồng Bất Tận', N'Nguyễn Ngọc Tu', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 3, 1, N'https://ghiensach.com/wp-content/uploads/2021/11/cac-tac-pham-van-hoc-viet-nam.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (340, N'Làm Đi', N'Vu Trọng Phụng', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 3, 1, N'https://ghiensach.com/wp-content/uploads/2021/11/tac-pham-van-hoc-viet-nam-lam-di.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (341, N'Hà Nội Bảy Sáu Phố Phường', N'Thạch Lam', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 3, 1, N'https://ghiensach.com/wp-content/uploads/2021/11/tac-pham-van-hoc-viet-nam-kinh-dien.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (342, N'Nhật Ký Đặng Thùy Trâm', N'Đặng Thùy Trâm', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 3, 1, N'https://ghiensach.com/wp-content/uploads/2021/11/nhung-tac-pham-van-hoc-kinh-dien-cua-viet-nam.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (343, N'Đất Rừng Phương Nam', N'Đoàn Giỏi', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 3, 1, N'https://ghiensach.com/wp-content/uploads/2021/11/sach-van-hoc-viet-nam.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (344, N'Dế Mèn Phiêu Lưu Ký', N'Tô Hoài', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 3, 1, N'https://ghiensach.com/wp-content/uploads/2021/11/van-hoc-viet-nam.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (345, N'Số Đỏ', N'Vu Trọng Phụng', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 3, 1, N'https://ghiensach.com/wp-content/uploads/2021/11/so-do.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (346, N'Thương Nhớ Mười Hai', N'Vu Bằng', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 3, 1, N'https://ghiensach.com/wp-content/uploads/2021/11/thuong-nho-muoi-hai.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (347, N'Trên Đường Băng', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(50000.00 AS Decimal(10, 2)), 100, 4, 1, N'https://cdn.diemnhangroup.com/seoulacademy/2022/07/sach-phat-trien-ban-than-6.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (348, N'Đắc Nhân Tâm', N'Dale Carnegie', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(60000.00 AS Decimal(10, 2)), 100, 4, 1, N'https://cdn.diemnhangroup.com/seoulacademy/2022/07/sach-phat-trien-ban-than-7.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (349, N'Tuổi Trẻ Đáng Giá Bao Nhiêu', N'Rosie Nguyễn', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(45000.00 AS Decimal(10, 2)), 100, 4, 1, N'https://cdn.diemnhangroup.com/seoulacademy/2022/07/sach-phat-trien-ban-than-8.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (350, N'Đổi Thay Để Thay Đổi', N'Wayne Dyer', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(55000.00 AS Decimal(10, 2)), 100, 4, 1, N'https://cdn.diemnhangroup.com/seoulacademy/2022/07/sach-phat-trien-ban-than-9.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (351, N'Tôi Tài Giỏi, Bạn Cùng Thể!', N'Adam Khoo', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(48000.00 AS Decimal(10, 2)), 100, 4, 2, N'https://cdn.diemnhangroup.com/seoulacademy/2022/07/sach-phat-trien-ban-than-10.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (352, N'Cách Sống', N'Robin Sharma', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(52000.00 AS Decimal(10, 2)), 100, 4, 2, N'https://cdn.diemnhangroup.com/seoulacademy/2022/07/sach-phat-trien-ban-than-11.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (353, N'Đổi Ngắn Đổi Dài', N'Robin Sharma', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(54000.00 AS Decimal(10, 2)), 100, 4, 2, N'https://cdn.diemnhangroup.com/seoulacademy/2022/07/sach-phat-trien-ban-than-12.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (354, N'Cân bằng cảm xúc cố lúc bão giông', N'Richard Nicholls', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(61000.00 AS Decimal(10, 2)), 100, 4, 2, N'https://cdn.diemnhangroup.com/seoulacademy/2022/07/sach-phat-trien-ban-than-15.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (355, N'Quẳng gánh lo đi mà vui sống', N'Dale Carnegie', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(57000.00 AS Decimal(10, 2)), 100, 4, 2, N'https://cdn.diemnhangroup.com/seoulacademy/2022/07/sach-phat-trien-ban-than-13.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (356, N'7 Thói Quen Hiệu Quả', N'Stephen R. Covey', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(56000.00 AS Decimal(10, 2)), 100, 4, 2, N'https://cdn.diemnhangroup.com/seoulacademy/2022/07/sach-phat-trien-ban-than-14.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (357, N'Đắc Nhân Tâm', N'Dale Carnegie', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(60000.00 AS Decimal(10, 2)), 100, 5, 2, N'https://marketingai.mediacdn.vn/wp-content/uploads/2019/10/noi-dung-sach-dac-nhan-tam-min.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (358, N'Người Bán Hàng Vi Điểm Nhất Thế Giới', N'Og Mandino', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(55000.00 AS Decimal(10, 2)), 100, 5, 2, N'https://marketingai.mediacdn.vn/wp-content/uploads/2019/10/sachhayvekinhdoanh-5.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (359, N'Nghĩ Giàu Và Làm Giàu', N'Napoleon Hill', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(58000.00 AS Decimal(10, 2)), 100, 5, 2, N'https://marketingai.mediacdn.vn/wp-content/uploads/2019/10/sachhayvekinhdoanh-4-658x370.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (360, N'48 Nguyên Tắc Chó Chất Của Quyền Lực', N'Robert Greene', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(62000.00 AS Decimal(10, 2)), 100, 5, 2, N'https://marketingai.mediacdn.vn/wp-content/uploads/2019/10/sachhayvekinhdoanh-9-539x370.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (361, N'Dạy Con Làm Giàu', N'Robert T. Kiyosaki', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(54000.00 AS Decimal(10, 2)), 100, 5, 2, N'https://marketingai.mediacdn.vn/wp-content/uploads/2019/10/sachhayvekinhdoanh-1.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (362, N'Tuần Làm Việc 4 Giờ', N'Timothy Ferriss', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(63000.00 AS Decimal(10, 2)), 100, 5, 2, N'https://marketingai.mediacdn.vn/wp-content/uploads/2019/10/Tuan-lam-viec-4-gio-2-min.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (363, N'7 Thói Quen Ðể Thành Ðạt', N'Stephen R. Covey', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(57000.00 AS Decimal(10, 2)), 100, 5, 2, N'https://marketingai.mediacdn.vn/wp-content/uploads/2019/10/sachhayvekinhdoanh-3.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (364, N'Ðể Xây Dựng Doanh Nghiệp Hiệu Quả', N'Michael E. Gerber', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(65000.00 AS Decimal(10, 2)), 100, 5, 2, N'https://marketingai.mediacdn.vn/wp-content/uploads/2019/10/sachhayvekinhdoanh-6-436x370.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (365, N'Khác Biệt Ðể Bứt Phá', N'Jason Fried', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(56000.00 AS Decimal(10, 2)), 100, 5, 2, N'https://marketingai.mediacdn.vn/wp-content/uploads/2019/10/sachhayvekinhdoanh-8.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (366, N'Kinh Tế Hàc Hài Hước', N'Steven D. Levitt', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(59000.00 AS Decimal(10, 2)), 100, 5, 2, N'https://marketingai.mediacdn.vn/wp-content/uploads/2019/10/sachhayvekinhdoanh-7.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (367, N'Giết Con Chim Nhại', N'Harper Lee', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(45000.00 AS Decimal(10, 2)), 100, 6, 2, N'https://icdn.dantri.com.vn/a3HWDOlTcvMNT73KRccc/Image/2014/07/45-8768d.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (368, N'Vụ Án', N'Franz Kafka', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(48000.00 AS Decimal(10, 2)), 100, 6, 2, N'https://icdn.dantri.com.vn/a3HWDOlTcvMNT73KRccc/Image/2014/07/46-8768d.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (369, N'Bắt Trẻ Ðồng Xanh', N'J.D. Salinger', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(49000.00 AS Decimal(10, 2)), 100, 6, 3, N'https://icdn.dantri.com.vn/a3HWDOlTcvMNT73KRccc/Image/2014/07/47-8768d.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (370, N'Anh Em Nhà Karamazov', N'Fyodor Dostoevsky', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(51000.00 AS Decimal(10, 2)), 100, 6, 3, N'https://icdn.dantri.com.vn/a3HWDOlTcvMNT73KRccc/Image/2014/07/48-8768d.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (371, N'Anna Karenina', N'Leo Tolstoy', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(53000.00 AS Decimal(10, 2)), 100, 6, 3, N'https://icdn.dantri.com.vn/a3HWDOlTcvMNT73KRccc/Image/2014/07/49-8768d.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (372, N'Hận Mạnh Đêm Giáng Sinh', N'Charles Dickens', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(46000.00 AS Decimal(10, 2)), 100, 6, 3, N'https://example.com/image26.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (373, N'Không Gia Đình', N'Hector Malot', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(55000.00 AS Decimal(10, 2)), 100, 6, 3, N'https://icdn.dantri.com.vn/a3HWDOlTcvMNT73KRccc/Image/2014/07/50-8768d.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (374, N'Ông Già Và Biển Cả', N'Ernest Hemingway', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(59000.00 AS Decimal(10, 2)), 100, 6, 3, N'https://icdn.dantri.com.vn/a3HWDOlTcvMNT73KRccc/Image/2014/07/51-8768d.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (375, N'Âm Thanh Và Cuộc Nổi Loạn', N'William Faulkner', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(58000.00 AS Decimal(10, 2)), 100, 6, 3, N'https://icdn.dantri.com.vn/a3HWDOlTcvMNT73KRccc/Image/2014/07/52-8768d.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (376, N'Thép Đã Tôi Thấy', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(60000.00 AS Decimal(10, 2)), 100, 6, 3, N'https://icdn.dantri.com.vn/a3HWDOlTcvMNT73KRccc/Image/2014/07/53-8768d.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (377, N'Câu Lạc Bộ Liars', N'Mary Karr', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(46000.00 AS Decimal(10, 2)), 100, 7, 3, N'https://static.ybox.vn/2018/8/0/1534652681182-1-2.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (378, N'Mái Đầu Vui Vẻ', N'Alison Bechdel', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(48000.00 AS Decimal(10, 2)), 100, 7, 3, N'https://static.ybox.vn/2018/8/0/1534652432121-3-4.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (379, N'Nơi Hoang Dã', N'Cheryl Strayed', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(52000.00 AS Decimal(10, 2)), 100, 7, 2, N'https://static.ybox.vn/2018/8/0/1534652442068-5-6.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (380, N'Đêm Súng', N'David Carr', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(51000.00 AS Decimal(10, 2)), 100, 7, 3, N'https://static.ybox.vn/2018/8/0/1534652449008-7-8.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (381, N'Ăn, Cầu Nguyện, Yêu', N'Elizabeth Gilbert', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(55000.00 AS Decimal(10, 2)), 100, 7, 3, N'https://static.ybox.vn/2018/8/0/1534652456648-9-10.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (382, N'Một Ông Bố, Hai Đứa Con Trai', N'Ta-Nehisi Coates', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(57000.00 AS Decimal(10, 2)), 100, 7, 3, N'https://static.ybox.vn/2018/8/0/1534652467113-11-12.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (383, N'Gián Đoạn', N'Susanna Kaysen', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(49000.00 AS Decimal(10, 2)), 100, 7, 3, N'https://static.ybox.vn/2018/8/0/1534652481303-13-14.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (384, N'Số Đắt Phá Của Thiên Tài Đáng Kinh Ngạc', N'Dave Eggers', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(53000.00 AS Decimal(10, 2)), 100, 7, 3, N'https://static.ybox.vn/2018/8/0/1534652499380-15-16.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (385, N'Đọc Lolita Ở Tehran', N'Azar Nafisi', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(50000.00 AS Decimal(10, 2)), 100, 7, 3, N'https://static.ybox.vn/2018/8/0/1534652508310-17-18.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (386, N'Sao Không Phải Là Tôi?', N'Mindy Kaling', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(54000.00 AS Decimal(10, 2)), 100, 7, 3, N'https://static.ybox.vn/2018/8/0/1534652520123-19-20.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (387, N'Luyện Siêu Trí Như Từ Vựng Tiếng Anh', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(45000.00 AS Decimal(10, 2)), 100, 8, 3, N'https://sachhay24h.com/uploads/images/2020/T5/476/top-10-sach-hoc-tieng-anh.PNG')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (388, N'Grammar For You (Basic)', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(48000.00 AS Decimal(10, 2)), 100, 8, 2, N'https://sachhay24h.com/uploads/images/2020/T5/476/top-10-sach-hoc-tieng-anh-1.PNG')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (389, N'Speak English Like an American', N'Amy Gillet', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(52000.00 AS Decimal(10, 2)), 100, 8, 3, N'https://sachhay24h.com/uploads/images/2020/T5/476/top-10-sach-hoc-tieng-anh-2.PNG')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (390, N'Vừa Lười Vừa Bận Vượt Giới Tiếng Anh', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(51000.00 AS Decimal(10, 2)), 100, 8, 2, N'https://sachhay24h.com/uploads/images/2020/T5/476/top-10-sach-hoc-tieng-anh-3.PNG')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (391, N'Very Easy Toeic', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(55000.00 AS Decimal(10, 2)), 100, 8, 2, N'https://example.com/image45.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (392, N'Luyện Nói Tiếng Anh Như Người Bản Ngữ', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(57000.00 AS Decimal(10, 2)), 100, 8, 3, N'https://sachhay24h.com/uploads/images/2020/T5/476/top-10-sach-hoc-tieng-anh-4.PNG')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (393, N'Tactics For Listening', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(49000.00 AS Decimal(10, 2)), 100, 8, 2, N'https://sachhay24h.com/uploads/images/2020/T5/476/top-10-sach-hoc-tieng-anh-5.PNG')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (394, N'5000 Từ Vựng Tiếng Anh Thông Dụng Nhất', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(53000.00 AS Decimal(10, 2)), 100, 8, 3, N'https://sachhay24h.com/uploads/images/2020/T5/476/top-10-sach-hoc-tieng-anh-6.PNG')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (395, N'Hacking Your English Speaking', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(50000.00 AS Decimal(10, 2)), 100, 8, 2, N'https://sachhay24h.com/uploads/images/2020/T5/476/top-10-sach-hoc-tieng-anh-8.PNG')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (396, N'Cẩm Nang Luyện Thi IELTS 10', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(54000.00 AS Decimal(10, 2)), 100, 8, 2, N'https://sachhay24h.com/uploads/images/2020/T5/476/top-10-sach-hoc-tieng-anh-9.PNG')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (397, N'Tư Duy Nhanh Và Chậm', N'Daniel Kahneman', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(45000.00 AS Decimal(10, 2)), 100, 9, 2, N'https://simg.zalopay.com.vn/zlp-website/assets/Tu_Duy_Nhanh_Va_Cham_Daniel_Kahneman_d023ac6ba1.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (398, N'Phi Lý Trí', N'Dan Ariely', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(48000.00 AS Decimal(10, 2)), 100, 9, 2, N'https://simg.zalopay.com.vn/zlp-website/assets/Phi_Ly_Tri_Dan_Ariely_b96027a158.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (399, N'Im Lặng - Sức Mạnh Của Người Hướng Nội', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(52000.00 AS Decimal(10, 2)), 100, 9, 2, N'https://simg.zalopay.com.vn/zlp-website/assets/sach_tam_ly_Im_lang_Suc_manh_cua_nguoi_huong_noi_1_9210e9e8cd.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (400, N'Thay Đổi - Bí Quyết Thay Đổi Khi Trở Nên Khó Khăn', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(51000.00 AS Decimal(10, 2)), 100, 9, 2, N'https://simg.zalopay.com.vn/zlp-website/assets/sach_tam_ly_hay_nhat_Thay_doi_3c77deeab9.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (401, N'Tâm Lý Học Đám Đông', N'Gustave Le Bon', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(55000.00 AS Decimal(10, 2)), 100, 9, 2, N'https://simg.zalopay.com.vn/zlp-website/assets/sach_tam_ly_hay_nhat_Tam_ly_hoc_dam_dong_33cba04cf8.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (402, N'Sức Mạnh Của Thói Quen', N'Charles Duhigg', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(57000.00 AS Decimal(10, 2)), 100, 9, 2, N'https://simg.zalopay.com.vn/zlp-website/assets/sach_tam_ly_hay_nhat_Suc_manh_cua_thoi_quen_2e73be3f0b.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (403, N'Tình Cảm Gặp Hạnh Phúc', N'Daniel Gilbert', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(49000.00 AS Decimal(10, 2)), 100, 9, 2, N'https://simg.zalopay.com.vn/zlp-website/assets/Tinh_Co_Gap_Hanh_Phuc_Daniel_Gilbert_c62610d039.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (404, N'Bản Chất Của Đời Trẻ', N'Dan Ariely', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(53000.00 AS Decimal(10, 2)), 100, 9, 2, N'https://simg.zalopay.com.vn/zlp-website/assets/Ban_Chat_Cua_Doi_Tra_Dan_Ariely_37e4bfe18f.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (405, N'Nghịch Lý Của Sự Lựa Chọn', N'Barry Schwartz', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(50000.00 AS Decimal(10, 2)), 100, 9, 2, N'https://simg.zalopay.com.vn/zlp-website/assets/Nghich_Ly_Cua_Su_Lua_Chon_Barry_Schwartz_0017539634.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (406, N'Đi Tìm Lẽ Sống', N'Viktor Emil Frankl', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(54000.00 AS Decimal(10, 2)), 100, 9, 2, N'https://simg.zalopay.com.vn/zlp-website/assets/Di_Tim_Le_Song_Viktor_Emil_Frankl_41dc8b1c4a.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (407, N'21 Nguyên Tắc Vàng Của Nghệ Thuật Lãnh Đạo', N'John C. Maxwell', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(45000.00 AS Decimal(10, 2)), 100, 9, 3, N'https://bizweb.dktcdn.net/100/197/269/files/img228-16.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (408, N'Nhà Lãnh Đạo 360 Độ', N'John C. Maxwell', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(48000.00 AS Decimal(10, 2)), 100, 9, 3, N'https://bizweb.dktcdn.net/100/197/269/files/nha-lanh-dao-360-do-u2673-d20160823-t101151-801941.jpg?v=1496816052952')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (409, N'Phát Triển Kỹ Năng Lãnh Đạo', N'John C. Maxwell', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(52000.00 AS Decimal(10, 2)), 100, 9, 3, N'https://bizweb.dktcdn.net/100/197/269/files/1-149-2.png?v=1496816135840')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (410, N'Tinh Hoa Lãnh Đạo', N'John C. Maxwell', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(51000.00 AS Decimal(10, 2)), 100, 9, 3, N'https://bizweb.dktcdn.net/100/197/269/files/img227-1-3.jpg?v=1496816236839')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (411, N'Lãnh Đạo Phong Cách Barack Obama', N'Shel Leanne', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(55000.00 AS Decimal(10, 2)), 100, 9, 1, N'https://bizweb.dktcdn.net/100/197/269/files/obama-outline-14-5-2016-01-u335-d20160516-t134115.jpg?v=1496816378338')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (412, N'90 Ngày Đầu Tiên Làm Sếp', N'Michael Watkins', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(57000.00 AS Decimal(10, 2)), 100, 9, 3, N'https://bizweb.dktcdn.net/100/197/269/files/90-ngay-dau-lam-sep.jpg?v=1496816668784')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (413, N'Để Xây Dựng Doanh Nghiệp Hiệu Quả', N'Michael E. Gerber', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(49000.00 AS Decimal(10, 2)), 100, 9, 3, N'https://bizweb.dktcdn.net/100/197/269/files/the-emyth-de-xay-dung-doanh-nghiep-hieu-qua.jpg?v=1496816857404')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (414, N'MBA Cơ Bản', N'Tom Gorman', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(53000.00 AS Decimal(10, 2)), 100, 9, 3, N'https://bizweb.dktcdn.net/100/197/269/files/mba-co-ban-a.jpg?v=1496816960820')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (415, N'Lý Thuyết Trò Chơi', N'Lê Hồng Nhật', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(50000.00 AS Decimal(10, 2)), 100, 9, 1, N'https://bizweb.dktcdn.net/100/197/269/files/lythuyettrochoi-sachkhaitam.png?v=1496817049664')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (416, N'Những Quy Tắc Trong Quản Lý', N'Richard Templar', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(54000.00 AS Decimal(10, 2)), 100, 9, 1, N'https://bizweb.dktcdn.net/100/197/269/files/nhung-quy-tac-trong-quan-ly-bo-tui-a-copy.png?v=1496817228819')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (417, N'Vô Cùng Tàn Nhẫn Vô Cùng Yêu Thương', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(45000.00 AS Decimal(10, 2)), 100, 11, 1, N'https://forum.waka.vn/images/img-xbz0ecpm.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (418, N'Dạy Con Kiểu Do Thái', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(48000.00 AS Decimal(10, 2)), 100, 11, 1, N'https://forum.waka.vn/images/img-p8cz0hrn.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (419, N'Cha Mẹ Nhất Dạy Con Từ Lập', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(52000.00 AS Decimal(10, 2)), 100, 11, 1, N'https://forum.waka.vn/images/img-fbp4d7iy.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (420, N'Cha Mẹ Pháp Không Đuổi Hàng', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(51000.00 AS Decimal(10, 2)), 100, 11, 1, N'https://forum.waka.vn/images/img-jmthac8g.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (421, N'Để Con Được Ốm', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(55000.00 AS Decimal(10, 2)), 100, 11, 1, N'https://forum.waka.vn/images/img-2xgujm2f.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (422, N'Người Mẹ Tốt Hơn Người Thầy Tốt', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(57000.00 AS Decimal(10, 2)), 100, 11, 1, N'https://forum.waka.vn/images/img-9bv0lie9.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (423, N'Người Cha Tốt Hơn Là Người Thầy Tốt', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(49000.00 AS Decimal(10, 2)), 100, 11, 1, N'https://forum.waka.vn/images/img-r6mtxrom.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (424, N'Con Nghĩ Đi, Mẹ Không Biết!', N'Chưa biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(53000.00 AS Decimal(10, 2)), 100, 11, 1, N'https://forum.waka.vn/images/img-mwcwwo3f.jpg')
GO
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (425, N'Cha Mẹ Thời Ðại Ki Thuật Số', N'Chua biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(50000.00 AS Decimal(10, 2)), 100, 11, 1, N'https://forum.waka.vn/images/img-utope9zl.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (426, N'Ba Mẹ Oi, Con Bị Bặt Nạt', N'Chua biết tác giả', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(54000.00 AS Decimal(10, 2)), 100, 11, 1, N'https://forum.waka.vn/images/img-8zpxa3vu.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (427, N'Doremon', N'Fujiko F. Fujio', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(25000.00 AS Decimal(10, 2)), 100, 12, 1, N'https://laodongnhatban.com.vn/images/2018/06/13/0-doremon.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (428, N'Thám tử lừng danh Conan', N'Gosho Aoyama', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(28000.00 AS Decimal(10, 2)), 100, 12, 1, N'https://laodongnhatban.com.vn/images/2018/06/13/0-conam.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (429, N'Bảy viên ngọc rồng - Dragon Ball', N'Akira Toriyama', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(32000.00 AS Decimal(10, 2)), 100, 12, 1, N'https://laodongnhatban.com.vn/images/2018/06/14/1-bay-vien-ngoc-rong.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (430, N'Ðảo hải tặc - One Piece', N'Eiichiro Oda', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(31000.00 AS Decimal(10, 2)), 100, 12, 1, N'https://laodongnhatban.com.vn/images/2018/06/14/dao-hai-tac.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (431, N'Naruto', N'Masashi Kishimoto', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(35000.00 AS Decimal(10, 2)), 100, 12, 1, N'https://laodongnhatban.com.vn/images/2018/06/15/naruto.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (432, N'Nữ hoàng Ai Cập', N'Chie Shinohara', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(37000.00 AS Decimal(10, 2)), 100, 12, 1, N'https://laodongnhatban.com.vn/images/2018/06/14/nu-hoang-ai-cap.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (433, N'Truyện tranh "Bleach"', N'Tite Kubo', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(29000.00 AS Decimal(10, 2)), 100, 12, 1, N'https://laodongnhatban.com.vn/images/2018/06/14/bleach.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (434, N'SLAM DUNK', N'Takehiko Inoue', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(33000.00 AS Decimal(10, 2)), 100, 12, 1, N'https://laodongnhatban.com.vn/images/2018/06/14/slam%20dunk.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (435, N'FAIRY TAIL', N'Hiro Mashima', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(30000.00 AS Decimal(10, 2)), 100, 12, 1, N'https://laodongnhatban.com.vn/images/2018/06/14/fairy%20tail.png')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (436, N'DEATH NOTE', N'Tsugumi Ohba và Takeshi Obata', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(34000.00 AS Decimal(10, 2)), 100, 12, 1, N'https://laodongnhatban.com.vn/images/2018/06/14/death-note.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (449, N'Neil Gaiman', N'George R. R. Martin', CAST(N'2023-09-18' AS Date), CAST(0.20 AS Decimal(10, 2)), CAST(42000.00 AS Decimal(10, 2)), 100, 2, 1, N'https://ghiensach.com/wp-content/uploads/2022/03/sach-khoa-hoc-vien-tuong.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (450, N'Frank Herbert', N'Liv Constantine', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(41000.00 AS Decimal(10, 2)), 100, 2, 1, N'https://ghiensach.com/wp-content/uploads/2022/03/xu-cat.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (451, N'Matt Haig', N'Nora Seed', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(43000.00 AS Decimal(10, 2)), 100, 2, 2, N'https://ghiensach.com/wp-content/uploads/2022/03/thu-vien-nua-dem.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (452, N'Glendy Vanderah', N'Nora Seed', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(49000.00 AS Decimal(10, 2)), 100, 2, 2, N'https://ghiensach.com/wp-content/uploads/2022/03/truyen-khoa-hoc-vien-tuong.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (453, N'Yasutaka Tsutsui', N'Nora Seed', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(48000.00 AS Decimal(10, 2)), 100, 2, 2, N'https://ghiensach.com/wp-content/uploads/2022/03/ke-trom-giac-mo.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (454, N'Ray Bradbury', N'Nora Seed', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(47000.00 AS Decimal(10, 2)), 100, 2, 2, N'https://ghiensach.com/wp-content/uploads/2022/03/451-do-f.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (455, N'H.G.Wells', N'Liv Constantine', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(46000.00 AS Decimal(10, 2)), 100, 2, 2, N'https://ghiensach.com/wp-content/uploads/2022/03/co-may-thoi-gian.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (456, N'Haruki Murakami', N'Liv Constantine', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(45000.00 AS Decimal(10, 2)), 100, 2, 2, N'https://ghiensach.com/wp-content/uploads/2022/03/truyen-khoa-hoc-vien-tuong-noi-tieng.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (457, N'Jules Verne', N'Liv Constantine', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 2, 2, N'https://ghiensach.com/wp-content/uploads/2022/03/truyen-vien-tuong.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (458, N'Douglas Adams', N'Arthur Dent', CAST(N'2023-09-18' AS Date), CAST(0.10 AS Decimal(10, 2)), CAST(44000.00 AS Decimal(10, 2)), 100, 2, 2, N'https://ghiensach.com/wp-content/uploads/2022/03/tieu-thuyet-vien-tuong.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (459, N'Life of the wild', N'Sanchit Howdy', CAST(N'2023-11-11' AS Date), NULL, CAST(50000.00 AS Decimal(10, 2)), 120, 1, 3, N'https://demo.templatesjungle.com/booksaw/images/main-banner1.jpg')
INSERT [dbo].[Product] ([productID], [proName], [authorName], [maniDate], [discount], [price], [quantity], [categoryID], [sellerID], [image_url]) VALUES (460, N'Bird gonna be happy', N'Timbủr Hood', CAST(N'2023-11-11' AS Date), NULL, CAST(44000.00 AS Decimal(10, 2)), 120, 1, 3, N'https://demo.templatesjungle.com/booksaw/images/main-banner2.jpg')

SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([roleID], [roleName]) VALUES (1, N'Khách hàng')
INSERT [dbo].[Role] ([roleID], [roleName]) VALUES (2, N'Nhân viên bán hàng')
INSERT [dbo].[Role] ([roleID], [roleName]) VALUES (3, N'Quản lý')
INSERT [dbo].[Role] ([roleID], [roleName]) VALUES (4, N'Admin')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO

INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Hàng chất lượng tốt', 1, 1, '2023-09-22')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Giao hàng nhanh', 1, 2, '2023-09-22')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Sản phẩm không đúng mô tả', 1, 3, '2023-09-22')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Dịch vụ tốt', 2, 4, '2023-09-23')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Đóng gói cẩn thận', 3, 5, '2023-09-24')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Giao hàng đúng hẹn', 4, 6, '2023-09-25')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Sản phẩm tốt', 4, 7, '2023-09-25')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Không hài lòng với chất lượng', 4, 8, '2023-09-25')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Giá rẻ, chất lượng tốt', 1, 9, '2023-09-26')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Giao hàng nhanh', 2, 10, '2023-09-26')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Sẽ mua lại ở đây', 3, 11, '2023-09-27')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Tốt', 4, 12, '2023-09-28')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Hàng đẹp', 1, 13, '2023-09-28')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Đóng gói kỹ', 2, 14, '2023-09-28')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Không nhận được hàng', 3, 15, '2023-09-29')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Sản phẩm tốt, giao hàng nhanh', 1, 16, '2022-08-20')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Dịch vụ tồi tệ, phải chờ lâu mới nhận được hàng', 2, 17, '2023-09-21')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Hài lòng với chất lượng sản phẩm', 3, 18, '2023-10-20')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Giao hàng nhanh, sẽ mua lại ở đây', 4, 19, '2022-08-21')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Sản phẩm đẹp, đóng gói cẩn thận', 1, 20, '2023-09-21')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Chất lượng tốt, giá cả hợp lý', 1, 21, '2023-10-22')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Đóng gói cẩn thận, hài lòng với sản phẩm', 2, 24, '2023-09-21')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Giao hàng nhanh, sẽ tiếp tục mua ở đây', 2, 25, '2023-10-22')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Sản phẩm đẹp, giá rẻ', 2, 26, '2022-08-23')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Tệ quá, hàng bị hỏng nhiều', 2, 27, '2022-11-16')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Giao hàng đúng hẹn', 3, 28, '2023-09-21')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Sản phẩm chất lượng tốt', 3, 29, '2023-10-22')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Giá cả phải chăng', 3, 30, '2022-08-23')
INSERT [dbo].[Feedback] ([content], [customerID], [orderID], [date]) VALUES (N'Nhanh gọn, sẽ quay lại mua tiếp', 3, 31, '2023-09-24')
GO
SET IDENTITY_INSERT [dbo].[Seller] ON 

INSERT [dbo].[Seller] ([sellerID], [salary], [accountID]) VALUES (1, CAST(50000.00 AS Decimal(10, 2)), 9)
INSERT [dbo].[Seller] ([sellerID], [salary], [accountID]) VALUES (2, CAST(55000.00 AS Decimal(10, 2)), 10)
INSERT [dbo].[Seller] ([sellerID], [salary], [accountID]) VALUES (3, CAST(60000.00 AS Decimal(10, 2)), 11)
SET IDENTITY_INSERT [dbo].[Seller] OFF
GO
INSERT [dbo].[Status] ([status]) VALUES (N'Processing')
INSERT [dbo].[Status] ([status]) VALUES (N'Rejected')
INSERT [dbo].[Status] ([status]) VALUES (N'Shipping')
INSERT [dbo].[Status] ([status]) VALUES (N'Accepted')
INSERT [dbo].[Status] ([status]) VALUES (N'Received')
GO
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (317, 1, 25000.00, 2)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (318, 1, 40000.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (319, 2, 30000.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (320, 3, 45000.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (321, 4, 50000.00, 2)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (322, 5, 55000.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (323, 6, 65000.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (324, 7, 70000.00, 2)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (325, 8, 75000.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (326, 9, 40000.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (327, 10, 40000.00, 2)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (338, 11, 44000.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (339, 12, 44000.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (340, 13, 44000.00, 2)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (341, 14, 44000.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (342, 15, 44000.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (368, 16, 43200.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (369, 16, 44100.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (370, 17, 45900.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (371, 17, 47700.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (372, 18, 41400.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (373, 18, 49500.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (374, 19, 53100.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (375, 19, 52200.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (376, 20, 54000.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (377, 20, 41400.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (378, 21, 43200.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (379, 21, 46800.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (380, 22, 45900.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (368, 22, 43200.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (369, 23, 44100.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (370, 23, 45900.00, 1)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (368, 24, 43200.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (369, 24, 44100.00, 4)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (370, 25, 45900.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (371, 25, 47700.00, 2)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (372, 26, 41400.00, 4)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (373, 26, 49500.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (374, 27, 53100.00, 2)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (375, 27, 52200.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (376, 28, 54000.00, 2)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (377, 28, 41400.00, 4)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (378, 29, 43200.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (379, 29, 46800.00, 2)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (380, 30, 45900.00, 4)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (368, 30, 43200.00, 3)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (369, 31, 44100.00, 2)
INSERT [dbo].[OrderDetail] ([productID], [orderID], [price], [quantity]) VALUES (370, 31, 45900.00, 3)
GO
SET ANSI_PADDING ON
GO
INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Sản phẩm tốt', 1, 317, '2022-09-24', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Sản phẩm tốt', 2, 317, '2022-09-24', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Sản phẩm tốt', 3, 317, '2022-09-24', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Giao hàng nhanh', 1, 318, '2022-09-24', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Giá rẻ', 2, 319, '2022-09-25', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Đóng gói kỹ càng', 3, 320, '2022-09-26', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Sản phẩm đẹp', 4, 321, '2022-09-27', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Chất lượng tốt', 1, 322, '2022-09-28', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Giao hàng nhanh', 2, 323, '2022-09-29', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Bao bì đẹp', 3, 324, '2022-09-30', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Hàng chất lượng', 4, 325, '2022-10-01', 5)
INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Sản phẩm tốt', 1, 317, '2022-09-24', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Giao hàng nhanh', 1, 318, '2022-09-24', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Giá rẻ', 2, 319, '2022-09-25', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Đóng gói kỹ càng', 3, 320, '2022-09-26', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Sản phẩm đẹp', 4, 321, '2022-09-27', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Chất lượng tốt', 1, 322, '2022-09-28', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Giao hàng nhanh', 2, 323, '2022-09-29', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Bao bì đẹp', 3, 324, '2022-09-30', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Hàng chất lượng', 4, 325, '2022-10-01', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Giá tốt', 1, 326, '2022-10-02', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Giao hàng nhanh', 2, 327, '2022-10-03', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Sản phẩm đẹp', 3, 338, '2022-10-04', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Chất lượng tốt', 4, 339, '2022-10-05', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Giao hàng nhanh', 1, 340, '2022-10-06', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Bao bì đẹp', 2, 341, '2022-10-07', 4)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Hàng chất lượng', 3, 342, '2022-10-08', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])

VALUES (N'Sản phẩm tốt', 1, 368, '2022-08-21', 5)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Giao hàng chậm', 2, 369, '2022-09-22', 3)

INSERT [dbo].[Comment] ([content], [customerID], [productID], [date], [star])
VALUES (N'Giá hợp lý', 3, 370, '2022-10-21', 4)

/**/
ALTER TABLE [dbo].[AccountRole]  WITH CHECK ADD FOREIGN KEY([accountID])
REFERENCES [dbo].[Account] ([accountID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AccountRole]  WITH CHECK ADD  CONSTRAINT [FK__AccountRole__roleID__656C112C] FOREIGN KEY([roleID])
REFERENCES [dbo].[Role] ([roleID])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[AccountRole] CHECK CONSTRAINT [FK__AccountRole__roleID__656C112C]
/**/
GO
ALTER TABLE [dbo].[Cart]  WITH CHECK ADD  CONSTRAINT [FK__Cart__customerID__66603565] FOREIGN KEY([customerID])
REFERENCES [dbo].[Customer] ([customerID])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Cart] CHECK CONSTRAINT [FK__Cart__customerID__66603565]
GO
/**/
GO
ALTER TABLE [dbo].[CartDetail]  WITH CHECK ADD  CONSTRAINT [FK_CartDetail_Cart] FOREIGN KEY([cartID])
REFERENCES [dbo].[Cart] ([cartID])
GO
ALTER TABLE [dbo].[CartDetail] CHECK CONSTRAINT [FK_CartDetail_Cart]
/**/
GO
/****/
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK__Comment__produc__6A30C649] FOREIGN KEY([productID])
REFERENCES [dbo].[Product] ([productID])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK__Comment__produc__6A30C649]
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD  CONSTRAINT [FK__Comment__custom__6A30C649] FOREIGN KEY([customerID])
REFERENCES [dbo].[Customer] ([customerID])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Comment] CHECK CONSTRAINT [FK__Comment__custom__6A30C649]
GO
/*****/
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK__Feedback__order__6A30C649] FOREIGN KEY([orderID])
REFERENCES [dbo].[CustomerOrder] ([orderID])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Feedback] CHECK CONSTRAINT [FK__Feedback__order__6A30C649]
GO
ALTER TABLE [dbo].[Feedback]  WITH CHECK ADD  CONSTRAINT [FK__Feedback__custom__6B40C649] FOREIGN KEY([customerID])
REFERENCES [dbo].[Customer] ([customerID])

GO
ALTER TABLE [dbo].[Feedback] CHECK CONSTRAINT [FK__Feedback__custom__6B40C649]
GO
/**

**/

ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK__Customer__accoun__6754599E] FOREIGN KEY([accountID])
REFERENCES [dbo].[Account] ([accountID])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK__Customer__accoun__6754599E]
GO

ALTER TABLE [dbo].[CustomerOrder]  WITH CHECK ADD  CONSTRAINT [FK__CustomerO__custo__68487DD7] FOREIGN KEY([customerID])
REFERENCES [dbo].[Customer] ([customerID])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[CustomerOrder] CHECK CONSTRAINT [FK__CustomerO__custo__68487DD7]
GO
ALTER TABLE [dbo].[CustomerOrder]  WITH CHECK ADD  CONSTRAINT [FK__CustomerO__selle__693CA210] FOREIGN KEY([sellerID])
REFERENCES [dbo].[Seller] ([sellerID])
GO
ALTER TABLE [dbo].[CustomerOrder] CHECK CONSTRAINT [FK__CustomerO__selle__693CA210]
GO
ALTER TABLE [dbo].[CustomerOrder]  WITH CHECK ADD FOREIGN KEY([status])
REFERENCES [dbo].[Status] ([status])
GO
/**/
ALTER TABLE [dbo].[Manager]  WITH CHECK ADD  CONSTRAINT [FK__Manager__account__6B24EA82] FOREIGN KEY([accountID])
REFERENCES [dbo].[Account] ([accountID])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Manager] CHECK CONSTRAINT [FK__Manager__account__6B24EA82]
GO
/**/
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK__OrderDeta__produ__6FE99F9F] FOREIGN KEY([productID])
REFERENCES [dbo].[Product] ([productID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK__OrderDeta__produ__6FE99F9F]
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD  CONSTRAINT [FK_orderID] FOREIGN KEY([orderID])
REFERENCES [dbo].[CustomerOrder] ([orderID])
GO
ALTER TABLE [dbo].[OrderDetail] CHECK CONSTRAINT [FK_orderID]
GO
/**/
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK__Product__categor__70DDC3D8] FOREIGN KEY([categoryID])
REFERENCES [dbo].[Category] ([cateID])
ON UPDATE SET NULL
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK__Product__categor__70DDC3D8]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_sellerID] FOREIGN KEY([sellerID])
REFERENCES [dbo].[Seller] ([sellerID])

GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_sellerID]
GO
/**/
ALTER TABLE [dbo].[Seller]  WITH CHECK ADD  CONSTRAINT [FK__Seller__accountI__73BA3083] FOREIGN KEY([accountID])
REFERENCES [dbo].[Account] ([accountID])
ON UPDATE NO ACTION
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Seller] CHECK CONSTRAINT [FK__Seller__accountI__73BA3083]
GO

USE [master]
GO
ALTER DATABASE [BookShop] SET  READ_WRITE 
GO
