/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.CartDetail;
import Entity.Product;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LuuTu
 */
public class DAOOrderDetail extends DBContext {

    public void createOrderDetails(int orderId, int cartId) {
        try {
            DAOCartDetail dao = new DAOCartDetail();
            List<CartDetail> cartDetails = dao.findCartDetailByID(cartId);

            // Insert order details for each product in the cart
            for (CartDetail cartDetail : cartDetails) {
                int productId = cartDetail.getProductID();
                int quantity = cartDetail.getQuantity();
                double price = getProductPrice(productId); // Get the product price

                // Insert order detail SQL
                String sql = "INSERT INTO OrderDetail (orderId, productId, price, quantity) VALUES (?, ?, ?, ?)";

                try ( PreparedStatement stmt = connection.prepareStatement(sql)) {
                    stmt.setInt(1, orderId);
                    stmt.setInt(2, productId);
                    stmt.setDouble(3, price * 0.9); // Set the product price
                    stmt.setInt(4, quantity);

                    stmt.executeUpdate();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void createOrderDetailsBuyNow(int orderId, int productID, int quantity, double price) {

        DAOCartDetail dao = new DAOCartDetail();

        // Insert order details for each product in the cart
        // Insert order detail SQL
        String sql = "INSERT INTO OrderDetail (orderId, productId, price, quantity) VALUES (?, ?, ?, ?)";

        try ( PreparedStatement stmt = connection.prepareStatement(sql)) {
            stmt.setInt(1, orderId);
            stmt.setInt(2, productID);
            stmt.setDouble(3, price * 0.9); // Set the product price
            stmt.setInt(4, quantity);

            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOOrderDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

// Method to retrieve product price by productId
    private double getProductPrice(int productId) {
        DAOProduct dao = new DAOProduct();
        return dao.getProductByID(productId).getPrice();
    }

    public static void main(String[] args) {
        DAOOrderDetail dao = new DAOOrderDetail();
        dao.createOrderDetails(8, 2);
    }

}
