/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import Entity.Product;
import Model.DAOProduct;

/**
 *
 * @author FPT
 */
public class DAOProduct extends DBContext {

    public int calculateTotalQuantitySold(int targetProductID) {
        int totalQuantitySold = 0;

        String sql = "SELECT SUM(od.quantity) AS total_quantity_sold "
                + "FROM CustomerOrder co "
                + "JOIN OrderDetail od ON co.orderID = od.orderID "
                + "WHERE co.status = 'Received' AND od.productID = ?";

        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, targetProductID);

            try ( ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    totalQuantitySold = resultSet.getInt("total_quantity_sold");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return totalQuantitySold;
    }

    public void reduceProductQuantityBuyNow(int productID, int quantity) {
        String updateSql = "UPDATE Product SET quantity = quantity - ? WHERE productID = ?";
        try ( PreparedStatement updateStatement = connection.prepareStatement(updateSql)) {
            updateStatement.setInt(1, quantity);
            updateStatement.setInt(2, productID);
            updateStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateQuantityRejected(int productID, int quantity) {
        String updateSql = "UPDATE Product SET quantity = quantity + ? WHERE productID = ?";
        try ( PreparedStatement updateStatement = connection.prepareStatement(updateSql)) {
            updateStatement.setInt(1, quantity);
            updateStatement.setInt(2, productID);
            updateStatement.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void reduceProductQuantity(int cartID) {
        PreparedStatement selectStatement = null;
        PreparedStatement updateStatement = null;
        ResultSet resultSet = null;

        try {
            // SQL query to select the cart quantities
            String selectSql = "SELECT productID, quantity FROM CartDetail WHERE cartID = ?";
            selectStatement = connection.prepareStatement(selectSql);
            selectStatement.setInt(1, cartID);
            resultSet = selectStatement.executeQuery();

            // Iterate through the result set and update product quantities
            while (resultSet.next()) {
                int productID = resultSet.getInt("productID");
                int cartQuantity = resultSet.getInt("quantity");

                // SQL query to update product quantity
                String updateSql = "UPDATE Product SET quantity = quantity - ? WHERE productID = ?";
                updateStatement = connection.prepareStatement(updateSql);
                updateStatement.setInt(1, cartQuantity);
                updateStatement.setInt(2, productID);
                updateStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // Close resources
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (selectStatement != null) {
                try {
                    selectStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (updateStatement != null) {
                try {
                    updateStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Product> pagingProduct(int index) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Product order by productID \n"
                + "offset ? row fetch next 8 rows only";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, (index - 1) * 8);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int getTotalProduct() {
        String sql = "select count(*) from Product";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Product> AllProducts() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM Product\n";

        try (
                 PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    // lay 8 san pham de lam feature product tren HomePage
    public List<Product> getAllProducts() {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT TOP 8 * FROM Product\n"
                + "ORDER BY productID ASC;";

        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                list.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)));
            }
        } catch (SQLException e) {
        }
        return list;
    }

    //tim kiem san pham theo productID
    public Product getProductByID(int id) {
        String sql = "SELECT * FROM Product\n"
                + "where productID = ?";

        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10));
            }
        } catch (SQLException e) {
        }
        return null;
    }

    //find product by name
    public List<Product> getProductByName(String name) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM Product\n"
                + "where proName like ?";
        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "%" + name + "%");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int productID = rs.getInt(1);
                String proName = rs.getString(2);
                String authorName = rs.getString(3);
                String maniDate = rs.getString(4);
                double discount = rs.getDouble(5);
                double price = rs.getDouble(6);
                int quantity = rs.getInt(7);
                int categoryID = rs.getInt(8);
                int providerID = rs.getInt(9);
                String image_url = rs.getString(10);
                list.add(new Product(productID, proName, authorName, maniDate, discount, price, quantity, categoryID, providerID, image_url));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<Product> getProductByCateID(String cateID) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM Product\n"
                + "where categoryID = ?";
        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, cateID);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int productID = rs.getInt(1);
                String proName = rs.getString(2);
                String authorName = rs.getString(3);
                String maniDate = rs.getString(4);
                double discount = rs.getDouble(5);
                double price = rs.getDouble(6);
                int quantity = rs.getInt(7);
                int categoryID = rs.getInt(8);
                int providerID = rs.getInt(9);
                String image_url = rs.getString(10);
                list.add(new Product(productID, proName, authorName, maniDate, discount, price, quantity, categoryID, providerID, image_url));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public List<Product> pagingSearchProduct(int index, String name) {
        List<Product> list = new ArrayList<>();
        String sql = "SELECT * FROM Product WHERE proName LIKE ? ORDER BY productID OFFSET ? ROWS FETCH NEXT 8 ROWS ONLY";
        try ( PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, "%" + name + "%");
            ps.setInt(2, (index - 1) * 8);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)));
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Xử lý ngoại lệ hoặc in log nếu cần thiết
        }
        return list;
    }

    public int getTotalProductByName(String name) {
        String sql = "SELECT count(*) FROM Product\n"
                + "where proName like ?";
        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, "%" + name + "%");
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException ex) {
        }
        return 0;
    }

    public List<Product> sortAscPrice() {
        String sql = "SELECT * FROM Product ORDER BY price ASC";
        List<Product> list = new ArrayList<>();
        try ( PreparedStatement ps = connection.prepareStatement(sql);  ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                list.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<Product> sortDescPrice() {
        String sql = "select * from Product order by price desc";
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> sortOldestName() {
        String sql = "select * from Product order by proName asc";
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> sortLatestName() {
        String sql = "select * from Product order by proName desc";
        List<Product> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Product> getProductsByCateAndSort(String category, String sort) {

        List<Product> filteredProducts = new ArrayList<>();

        String sql = "SELECT * FROM Product ";

        if (category != null) {
            if (category.equals("0")) {
                sql += " ";
            } else {
                sql += "WHERE categoryID = ?";
            }
        }

        if (sort != null) {
            switch (sort) {
                case "ascprice":
                    sql += " ORDER BY price ASC";
                    break;
                case "descprice":
                    sql += " ORDER BY price DESC";
                    break;
                case "oldest":
                    sql += " ORDER BY proName ASC";
                    break;
                case "latest":
                    sql += " ORDER BY proName DESC";
                    break;
                default:
                    break;
            }
        }
        try {
            PreparedStatement ps = connection.prepareStatement(sql);

            if (category != null && !category.equals("0")) {
                ps.setInt(1, Integer.parseInt(category));
            }
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                filteredProducts.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return filteredProducts;

    }

    public List<Product> getBestSellProduct() {
        int n = 0;
        List<Product> list = new ArrayList<>();
        try {
            String sql = "SELECT\n"
                    + "    PD.*\n"
                    + "FROM (\n"
                    + "    SELECT TOP 1 OD.productID\n"
                    + "    FROM OrderDetail OD\n"
                    + "    GROUP BY OD.productID\n"
                    + "    ORDER BY SUM(OD.quantity) DESC\n"
                    + ") BestSellingProductID\n"
                    + "INNER JOIN Product PD ON BestSellingProductID.productID = PD.productID;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5), rs.getDouble(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10)));
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static void main(String[] args) {
        DAOProduct dao = new DAOProduct();
        List<Product> list = dao.getProductsByCateAndSort("1", "default");
        System.out.println(list);
    }
}
