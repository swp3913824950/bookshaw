/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Feedback;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author LuuTu
 */
public class DAOFeedback extends DBContext{

  public void createFeedback(Feedback feedback) {
    String sql = "INSERT INTO feedback (content, customerID, orderID, date) VALUES (?,?,?,?)";
    
    try {
      PreparedStatement stm = connection.prepareStatement(sql);
      stm.setString(1, feedback.getContent());
      stm.setInt(2, feedback.getCustomerID());
      stm.setInt(3, feedback.getOrderID());
      stm.setString(4, feedback.getDate());  
      stm.executeUpdate();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
  
  public boolean hasUserFeedbackOnOrder(int orderID, int customerID) {
    String sql = "SELECT COUNT(*) FROM feedback WHERE orderID = ? AND customerID = ?";
    
    try {
      PreparedStatement stm = connection.prepareStatement(sql);
      stm.setInt(1, orderID);
      stm.setInt(2, customerID);
      
      ResultSet rs = stm.executeQuery();
      if(rs.next()) {
        int count = rs.getInt(1);
        return count > 0;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    
    return false;
  }
  
    public static void main(String[] args) {
        DAOFeedback dao = new DAOFeedback();
        System.out.println(dao.hasUserFeedbackOnOrder(4, 1));
    }
}
