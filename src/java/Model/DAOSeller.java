/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Account;
import Entity.Customer;
import Entity.CustomerOrder;
import Entity.Feedback;
import Entity.OrderDetail;
import Entity.Product;
import Entity.Status;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author FPT
 */
public class DAOSeller extends DBContext {

    public ResultSet getAllSellerInfor() {
        String sql = "  select sellerID, fullname from Seller s JOIN Account a on a.accountID = s.accountID";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            return rs;

        } catch (SQLException e) {
        }
        return null;
    }

    public List<Feedback> getAllFeedBack() {
        List<Feedback> list = new ArrayList<>();
        String sql = "select * from Feedback";
        try {
            PreparedStatement ps = connection.prepareCall(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Feedback(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getInt(4), rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int getSellerIDByAccountID(int accountID) {
        String sql = "select * from Seller where accountID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, accountID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);

            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Product> getProductBySell_ID(int id) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Product where sellerID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)
                ));
            }
        } catch (Exception e) {
            e.printStackTrace(); // In thÃ´ng tin lá»—i ra console

        }
        return list;
    }

    public void deleteProduct(String id) {
        String sql = "delete from Product where productID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {

        }
    }

    public void insertProduct(String productName, String auName, String date, String price,
            String quantity, String category, int id, String image) {
        String sql = "INSERT INTO [dbo].[Product]\n"
                + "           ([proName]\n"
                + "           ,[authorName]\n"
                + "           ,[maniDate]\n"
                + "           ,[price]\n"
                + "           ,[quantity]\n"
                + "           ,[categoryID]\n"
                + "           ,[sellerID]\n"
                + "           ,[image_url])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, productName);
            ps.setString(2, auName);
            ps.setString(3, date);
            ps.setString(4, price);
            ps.setString(5, quantity);
            ps.setString(6, category);
            ps.setInt(7, id);
            ps.setString(8, image);
            ps.executeUpdate();
            System.out.println("Product inserted successfully.");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Failed to insert the product.");
        }
    }

    public void updateProduct(String productName, String auName, String date, String price,
            String quantity, String category, String image, String pid) {
        String sql = "UPDATE [dbo].[Product]\n"
                + "   SET [proName] = ?\n"
                + "      ,[authorName] = ?\n"
                + "      ,[maniDate] = ?\n"
                + "      ,[price] = ?\n"
                + "      ,[quantity] = ?\n"
                + "      ,[categoryID] = ?\n"
                + "      ,[image_url] = ?\n"
                + " WHERE productID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, productName);
            ps.setString(2, auName);
            ps.setString(3, date);
            ps.setString(4, price);
            ps.setString(5, quantity);
            ps.setString(6, category);
            ps.setString(7, image);
            ps.setString(8, pid);
            ps.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<CustomerOrder> viewOrderList(int id) {
        String sql = "SELECT *\n"
                + "FROM CustomerOrder where sellerID = ?";
        List<CustomerOrder> orderlist = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Status s = new Status();
                orderlist.add(new CustomerOrder(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getDate(5), rs.getDate(6), rs.getDate(7), rs.getString(8)));
            }
        } catch (Exception e) {
        }
        return orderlist;
    }

    public CustomerOrder getShipDate(int orderID) {
        String sql = "select * from CustomerOrder where orderID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, orderID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Status s = new Status();
                return new CustomerOrder(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getDate(5), rs.getDate(6), rs.getDate(7), rs.getString(8));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<Status> viewStatusList() {
        String sql = "select * from Status";
        List<Status> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Status(rs.getString(1)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public void editDate(String requiredDate, String shippedDate, String status, String id) {
        String sql = "UPDATE [dbo].[CustomerOrder]\n"
                + "   SET \n"
                + "      [requiredDate] = ?\n"
                + "      ,[shippedDate] = ?\n"
                + "      ,[status] = ?\n"
                + " WHERE orderID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, requiredDate);
            ps.setString(2, shippedDate);
            ps.setString(3, status);
            ps.setString(4, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<CustomerOrder> searchByStatus(String status, int id) {
        String sql = "select * from CustomerOrder where status like ? and sellerID = ?";
        List<CustomerOrder> olist = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + status + "%");
            ps.setInt(2, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Status s = new Status();
                olist.add(new CustomerOrder(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getDate(5), rs.getDate(6), rs.getDate(7), rs.getString(8)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return olist;
    }

    public void changeStatus(String status, String id) {
        String sql = "update CustomerOrder set status = ? where orderID=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, status);
            ps.setString(2, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteOrder(int id) {
        String sql = "delete from CustomerOrder where orderID=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Product> searchProductByName(String keyword, int sellerID) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Product where proName like ? and sellerID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, "%" + keyword + "%");
            ps.setInt(2, sellerID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                list.add(new Product(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDouble(5),
                        rs.getDouble(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getString(10)
                ));
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    //cÃ¡i nÃ y cua customer nma toi viet tam vo day 
    public List<CustomerOrder> viewCustomerOrderList(int customerID) {
        String sql = "SELECT *\n"
                + "FROM CustomerOrder WHERE customerID = ?\n";
        List<CustomerOrder> orderlist = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, customerID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Status s = new Status();
                orderlist.add(new CustomerOrder(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getString(4), rs.getDate(5), rs.getDate(6), rs.getDate(7), rs.getString(8)));
            }
        } catch (Exception e) {
        }
        return orderlist;
    }

    public List<OrderDetail> viewCustomerOrderDetailList(int orderID) {
        String sql = "SELECT *\n"
                + "FROM OrderDetail WHERE orderID = ?\n";
        List<OrderDetail> orderlist = new ArrayList<>();
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, orderID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                orderlist.add(new OrderDetail(rs.getInt("productID"), rs.getInt("orderID"), rs.getDouble("price"), rs.getInt("quantity")));
            }
        } catch (Exception e) {
        }
        return orderlist;
    }

    public Account getCustomerbyAccountID(int customerID, int accountID) {
        String sql = "select * from Account where accountID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, accountID);
            ResultSet rs = ps.executeQuery();
            String sql1 = "select accountID from Customer where customerID = ?";
            PreparedStatement ps1 = connection.prepareStatement(sql1);
            ps1.setInt(1, customerID);
            if (rs.next()) {
                return new Account(accountID, rs.getDate(3), rs.getString(2), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getCustomerID(int accountID, int customerID) {
        String sql = "select customerID from CustomerOrder where customerID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, customerID);
            ResultSet rs = ps.executeQuery();
            String sql1 = "select accountID from Customer where accountID = ?";
            PreparedStatement ps1 = connection.prepareStatement(sql1);
            ps1.setInt(1, accountID);
            if (rs.next()) {
                return rs.getInt("customerID");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public double saleByMonth(int month, int id) {
        String sql = "select SUM(price) from OrderDetail od JOIN CustomerOrder cd on cd.orderID = od.orderID  where MONTH(cd.shippedDate) = ? and cd.sellerID = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, month);
            st.setInt(2, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getDouble(1);
            }
        } catch (SQLException e) {

        }
        return 0;
    }

    public static void main(String[] args) {
        DAOSeller dao = new DAOSeller();
        System.out.println(dao.getAllFeedBack());
    }

}
