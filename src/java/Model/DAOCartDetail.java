/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.CartDetail;
import Entity.Product;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class DAOCartDetail extends DBContext{
    public List<CartDetail> findCartDetailByID(int cartID){
        List<CartDetail> list1 = new ArrayList<>();
        int xProductID, xQuantity;
        String xSql = "select * from CartDetail where cartID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, cartID);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                xProductID = rs.getInt("productID");
                xQuantity = rs.getInt("quantity");
                CartDetail cart_detail = new CartDetail(cartID,xProductID,xQuantity);
                list1.add(cart_detail);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list1;
    }
    public void delete(int productID, int cartID) {
        String xSql = "delete from CartDetail where cartID=? and productID=?";
        try {
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, cartID);
            ps.setInt(2, productID);
            ps.executeUpdate();
            //con.commit();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void insert(CartDetail cd){
        String xSql = "insert into CartDetail (cartID, productID, quantity) values (?, ?, ?)";
        try {
        PreparedStatement pre = connection.prepareStatement(xSql);
        pre.setInt(1, cd.getCartID());
        pre.setInt(2, cd.getProductID());
        pre.setInt(3, cd.getQuantity());
        pre.executeUpdate();
        pre.close();
     }     
     catch(Exception e) {
        e.printStackTrace();
     }
    }
    public void deleteAll(int cartID){
        String xSql = "delete from CartDetail where cartID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, cartID);
            ps.executeUpdate();
            //con.commit();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public CartDetail findCartDetailByID2 (int cartID, int productID){
        CartDetail cd = null;
        int xQuantity;
        String xSql = "select*from CartDetail where cartID = ? and productID = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, cartID);
            ps.setInt(2, productID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                xQuantity = rs.getInt("quantity");
                cd = new CartDetail(cartID,productID,xQuantity);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cd;
    }
    public int getTotalCost(List<CartDetail> CartDetailList){
        DAOProduct dao = new DAOProduct();
        int total_cost = 0;
        for (CartDetail c : CartDetailList){
            double cost;
            int productID = c.getProductID();
            int quantity = c.getQuantity();
            Product pro = dao.getProductByID(productID);
            double price = pro.getPrice();
            double discount = pro.getDiscount();
            cost = price * (1 - discount) * (quantity * 1.0);
            total_cost += cost;
        }
        return total_cost;
    }
    public void updateQuantity(CartDetail cd){
        String xSql = "update CartDetail set quantity=? where cartID=? and productID=?";
     try {      
        PreparedStatement pre = connection.prepareStatement(xSql);
        pre.setInt(1, cd.getQuantity());
        pre.setInt(2, cd.getCartID());
        pre.setInt(3, cd.getProductID());
        pre.executeUpdate();
        pre.close();
     }
      catch(Exception e) {
        e.printStackTrace();
      }
    }
}