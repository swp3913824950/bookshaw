/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Customer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author ASUS
 */
public class DAOCustomer extends DBContext {

    public String getFullNameByCustomerID(int customerID) {
        String fullName = null;
        try {
            String sql = "SELECT a.fullname\n"
                    + "  FROM Account a\n"
                    + "  JOIN Customer c on a.accountID = c.accountID\n"
                    + "  WHERE c.customerID = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, customerID);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                fullName = resultSet.getString("fullname");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return fullName;
    }

    public Customer findCustomer(int accountID) {
        Customer x = null;
        int xCustomerID;
        String xSql = "select * from Customer where accountID =? ";
        try {
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, accountID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                xCustomerID = rs.getInt("customerID");
                x = new Customer(xCustomerID, accountID);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return x;
    }

    public int getNumberOfCustomer() throws SQLException {
        String xSql = "select count(*)[number] from Customer";
        PreparedStatement ps = connection.prepareStatement(xSql);
        ResultSet rs = ps.executeQuery();
        int number = rs.getInt("number");
        return number;
    }

    public static void main(String[] args) {

    }
}
