/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Cart;
import Entity.Customer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author ASUS
 */
public class DAOCart extends DBContext{
    public Cart findCartByID(int customerID){
        Cart cart = null;
        int xCartID;
        String xSql = "select * from Cart where customerID =?";
        try {
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, customerID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                xCartID = rs.getInt("cartID");
                cart = new Cart(xCartID, customerID);
            }
            rs.close();
            ps.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cart;
    }
    public void insert(Customer customer){
        String xSql = "insert into Cart (customerID) values (?)";
        try{
            PreparedStatement ps = connection.prepareCall(xSql);
            ps.setInt(1, customer.getCustomerID());
            ps.executeUpdate();
            ps.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}