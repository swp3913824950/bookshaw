/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author LuuTu
 */
public class DAOAccountRole extends DBContext{
    
    
    public int getRoleID(int accountID) {
    String sql = "SELECT [roleID] FROM [AccountRole] WHERE accountID = ?";
    int roleID = 0;

    try (PreparedStatement pre = connection.prepareStatement(sql)) {
        pre.setInt(1, accountID);
        try (ResultSet resultSet = pre.executeQuery()) {
            if (resultSet.next()) {
                roleID = resultSet.getInt("roleID");
            } 
        }
    } catch (SQLException e) {
    }

    return roleID;
}

public void changeAccountRole(int accountID, int newRoleId) {
    PreparedStatement deleteStatement = null;
    PreparedStatement insertStatement = null;
    PreparedStatement updateStatement = null;
    
    if (doesAccountExistWithRole(accountID, newRoleId)) return;
    try {
        // Determine the current role of the user
        int currentRoleId = getRoleID(accountID);

        // Define DELETE and INSERT queries based on the old and new roles
        String deleteQuery = "";
        String insertQuery = "";

        switch (currentRoleId) {
            case 1: // Customer
                deleteQuery = "DELETE FROM Customer WHERE accountID = ?";
                break;
            case 2: // Seller
                deleteQuery = "DELETE FROM Seller WHERE accountID = ?";
                break;
            case 3: // Manager
                deleteQuery = "DELETE FROM Manager WHERE accountID = ?";
                break;
            case 4: // Admin
                deleteQuery = "DELETE FROM Admin WHERE accountID = ?";
                break;
            default:
                // Handle unknown roles or exceptions as needed
                break;
        }

        switch (newRoleId) {
            case 1: // Customer
                insertQuery = "INSERT INTO Customer (accountID) VALUES (?)";
                break;
            case 2: // Seller
                insertQuery = "INSERT INTO Seller (accountID) VALUES (?)";
                break;
            case 3: // Manager
                insertQuery = "INSERT INTO Manager (accountID) VALUES (?)";
                break;
            case 4: // Admin
                insertQuery = "INSERT INTO Admin (accountID) VALUES (?)";
                break;
            default:
                break;
        }

        if (!deleteQuery.isEmpty()) {
            deleteStatement = connection.prepareStatement(deleteQuery);
            deleteStatement.setInt(1, accountID);
            deleteStatement.executeUpdate();
        }

        if (!insertQuery.isEmpty()) {
            insertStatement = connection.prepareStatement(insertQuery);
            insertStatement.setInt(1, accountID);
            insertStatement.executeUpdate();
        }

        // Update the AccountRole table
        String updateQuery = "UPDATE AccountRole SET roleID = ? WHERE accountID = ?";
        updateStatement = connection.prepareStatement(updateQuery);
        updateStatement.setInt(1, newRoleId);
        updateStatement.setInt(2, accountID);
        updateStatement.executeUpdate();
    } catch (SQLException e) {
    } finally {
        try {
            if (updateStatement != null) updateStatement.close();
            if (insertStatement != null) insertStatement.close();
            if (deleteStatement != null) deleteStatement.close();
        } catch (SQLException e) {
        }
    }
}

public boolean doesAccountExistWithRole(int accountID, int newRoleId) {
    PreparedStatement checkStatement = null;
    ResultSet resultSet = null;
    boolean accountExists = false;

    try {
        // Define a SELECT query to check if an account with the given ID and role exists
        String checkQuery = "SELECT COUNT(*) FROM AccountRole WHERE accountID = ? AND roleID = ?";
        checkStatement = connection.prepareStatement(checkQuery);
        checkStatement.setInt(1, accountID);
        checkStatement.setInt(2, newRoleId);
        resultSet = checkStatement.executeQuery();

        if (resultSet.next()) {
            int count = resultSet.getInt(1);
            if (count > 0) {
                accountExists = true; // An account with the given ID and role exists
            }
        }
    } catch (SQLException e) {
        e.printStackTrace(); // Handle exceptions as needed
    } finally {
        try {
            if (resultSet != null) resultSet.close();
            if (checkStatement != null) checkStatement.close();
        } catch (SQLException e) {
            e.printStackTrace(); // Handle exceptions as needed
        }
    }

    return accountExists;
}



    public static void main(String[] args) {
        DAOAccountRole roleChanger = new DAOAccountRole();
        int accountID = 9;
        int newRoleId = 3;
        roleChanger.changeAccountRole(accountID, newRoleId);
    }
}
