/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Account;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

/**
 *
 * @author admin
 */
public class DAOAccount extends DBContext {

    
    public void sendMail(String to, String subject, String text) {
        Properties props = new Properties();
        props.put("mail.smtp.charset", "UTF-8");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("booksaw.shop@gmail.com", "kjwy xfpu jjzq woom");
            }
        });
        try {
            Message message = new MimeMessage(session);
            message.setHeader("Content-Type", "text/plain; charset=UTF-8");
            message.setFrom(new InternetAddress("booksaw.shop@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(MimeUtility.encodeText(subject, "UTF-8", "B"));
             message.setContent(text, "text/plain; charset=UTF-8");
            Transport.send(message);
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(DAOAccount.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void deleteAccount(int accountID) {
        String sql = "DELETE FROM Account WHERE accountID = ?";
        try (
                 PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            preparedStatement.setInt(1, accountID);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * Checks if there is an account in the database based on the provided
     * username and password.
     *
     * @param username The username of the account.
     * @param password The password of the account.
     * @return True if an account with the provided username and password exists
     * in the database, false otherwise.
     */
//    public boolean checkAccount(String username, String password) {
//        String sql = "SELECT accountID FROM account "
//                + "WHERE username = ? AND password = ?";
//        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
//
//            statement.setString(1, username);
//            statement.setString(2, password);
//
//            ResultSet resultSet = statement.executeQuery();
//            return resultSet.next();
//
//        } catch (SQLException e) {
//        }
//        return false;
//    }
    public boolean checkAccount(String username, String password) {
        String sql = "SELECT accountID FROM account "
                + "WHERE (username = ? OR email = ?) AND password = ?";
        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, username);
            statement.setString(2, username);
            statement.setString(3, password);

            ResultSet resultSet = statement.executeQuery();
            return resultSet.next();

        } catch (SQLException e) {
            // Xử lý ngoại lệ (ví dụ: ghi log hoặc báo lỗi)
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Checks the existence of an account based on the provided username and
     * email.
     *
     * @param username The username to check.
     * @param email The email to check.
     * @return 2 if both exist, 1 if username exist, -1 if email exist, 0 if
     * none
     */
    public int checkAccountByNameAndEmail(String username, String email) {
        String sql = "SELECT * FROM account "
                + "WHERE username = ? OR email = ?";

        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, username);
            statement.setString(2, email);
            ResultSet resultSet = statement.executeQuery();

            int rowCount = 0;
            boolean usernameExists = false;
            boolean emailExists = false;

            while (resultSet.next()) {
                rowCount++;
                String dbUsername = resultSet.getString("username");
                String dbEmail = resultSet.getString("email");

                if (dbUsername != null && dbUsername.equals(username)) {
                    usernameExists = true;
                }

                if (dbEmail != null && dbEmail.equals(email)) {
                    emailExists = true;
                }
            }

            if (rowCount == 0) {
                return 0;
            } else if (rowCount == 1) {
                if (usernameExists && emailExists) {
                    return 2;
                } else if (usernameExists) {
                    return 1;
                } else if (emailExists) {
                    return -1;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOAccount.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public boolean changeProfile(Account a) {
        String sql = "UPDATE Account\n"
                + "   SET [dob] = ?\n"
                + "      ,[fullname] = ?\n"
                + "      ,[phone] = ?\n"
                + "      ,[address] = ?\n"
                + "      ,[password] = ?\n"
                + "      ,[username] = ?\n"
                + "      ,[email] = ?\n"
                + " WHERE accountID = ?";
        try ( PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, a.getDob().toString());
            ps.setString(2, a.getFullname());
            ps.setString(3, a.getPhone());
            ps.setString(4, a.getAddress());
            ps.setString(5, a.getPassword());
            ps.setString(6, a.getUsername());
            ps.setString(7, a.getEmail());
            ps.setInt(8, a.getAccountID());
            int rowsUpdated = ps.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException e) {
        }
        return false;
    }

    public void changePassword(String password, String username) {
        String sql = "update Account set password=? where username=?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, password);
            ps.setString(2, username);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves an account from the database based on the provided accountID.
     *
     * @param accountID The id of the account to retrieve.
     * @return An Account object if an account with the given ID is found in the
     * database, or null if no such account exists.
     */
    public Account getAccountById(int accountID) { // Change the parameter type to int
        String sql = "SELECT * FROM Account WHERE accountID = ?";
        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, accountID);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return extractAccountFromResultSet(resultSet);
            }
        } catch (SQLException e) {
        }
        return null;
    }

    /**
     * Extracts an Account object from the provided ResultSet.
     *
     * @param resultSet The ResultSet containing account information.
     * @return An Account object populated with data from the ResultSet.
     */
    Account extractAccountFromResultSet(ResultSet resultSet) {
        try {
            Account account = new Account();
            account.setAccountID(resultSet.getInt("accountID"));
            account.setDob(resultSet.getDate("dob"));
            account.setFullname(resultSet.getString("fullname"));
            account.setPhone(resultSet.getString("phone"));
            account.setAddress(resultSet.getString("address"));
            account.setPassword(resultSet.getString("password"));
            account.setUsername(resultSet.getString("username"));
            account.setEmail(resultSet.getString("email"));
            return account;
        } catch (SQLException ex) {
            Logger.getLogger(DAOAccount.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Retrieves an Account from the database based on the provided username.
     *
     * @param username The username associated with the Account to retrieve.
     * @return An Account object if an account with the given username is found
     * in the database, or null if no such account exists.
     */
    public Account getAccount(String username) {
        String sql = "SELECT * FROM Account WHERE (username = ? OR email =?)";
        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, username);
            statement.setString(2, username);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return extractAccountFromResultSet(resultSet);
            }
        } catch (SQLException e) {
        }
        return null;
    }

    public ResultSet getAllNonAdminAccount() {
        String sql = "SELECT a.*, r.roleName\n"
                + "FROM [Account] a\n"
                + "INNER JOIN AccountRole ar ON ar.accountID = a.accountID\n"
                + "INNER JOIN Role r ON ar.roleID =  r.roleID\n"
                + "where roleName not like 'Admin'";
        ResultSet rs = getData(sql);
        return rs;
    }

    public boolean addAccount(Account account, String roleName) {
        String insertAccountSql = "INSERT INTO Account (dob, fullname, phone, address, password, username, email) VALUES (?, ?, ?, ?, ?, ?, ?)";
        String insertAccountRoleSql = "INSERT INTO AccountRole (roleID, accountID) VALUES (?, ?)";
        String insertRoleSpecificSql = "";
        String roleColumnName = "";

        // Determine the role-specific SQL query and role ID based on the roleName
        int roleId;
        switch (roleName.toLowerCase()) {
            case "khách hàng":
                roleId = 1;
                insertRoleSpecificSql = "INSERT INTO Customer (accountID) VALUES (?)";
                roleColumnName = "customerID";
                break;
            case "nhân viên bán hàng":
                roleId = 2;
                insertRoleSpecificSql = "INSERT INTO Seller (accountID) VALUES (?)";
                roleColumnName = "sellerID";
                break;
            case "quản lý":
                roleId = 3;
                insertRoleSpecificSql = "INSERT INTO Manager (accountID) VALUES (?)";
                roleColumnName = "managerID";
                break;
            case "admin":
                roleId = 4;
                insertRoleSpecificSql = "INSERT INTO Admin (accountID) VALUES (?)";
                roleColumnName = "adminID";
                break;
            default:
                return false;
        }

        try ( PreparedStatement insertAccountStatement = connection.prepareStatement(insertAccountSql, Statement.RETURN_GENERATED_KEYS);  PreparedStatement insertAccountRoleStatement = connection.prepareStatement(insertAccountRoleSql);  PreparedStatement insertRoleSpecificStatement = connection.prepareStatement(insertRoleSpecificSql)) {

            insertAccountStatement.setDate(1, (Date) account.getDob());
            insertAccountStatement.setString(2, account.getFullname());
            insertAccountStatement.setString(3, account.getPhone());
            insertAccountStatement.setString(4, account.getAddress());
            insertAccountStatement.setString(5, account.getPassword());
            insertAccountStatement.setString(6, account.getUsername());
            insertAccountStatement.setString(7, account.getEmail());

            int rowsInserted = insertAccountStatement.executeUpdate();

            if (rowsInserted == 0) {
                return false;
            }

            ResultSet generatedKeys = insertAccountStatement.getGeneratedKeys();
            int accountID = -1;

            if (generatedKeys.next()) {
                accountID = generatedKeys.getInt(1);
            } else {
                return false;
            }

            insertAccountRoleStatement.setInt(1, roleId);
            insertAccountRoleStatement.setInt(2, accountID);

            insertRoleSpecificStatement.setInt(1, accountID);

            int accountRoleRowsInserted = insertAccountRoleStatement.executeUpdate();
            int roleSpecificRowsInserted = insertRoleSpecificStatement.executeUpdate();

            return (accountRoleRowsInserted > 0 && roleSpecificRowsInserted > 0);
        } catch (SQLException e) {
            return false;
        }
    }

    public boolean getUserEmail(String user, String email) {
        try {
            String sql = "select [Email] from Account where [username] = ? ";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, user);
            ResultSet rs = ps.executeQuery();
            rs.next();
            if (email.equals(rs.getString(1))) {
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAOAccount.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean existedEmail(String email) {
        try {
            String sql = "select * from Account where email = ?";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public class RandomPasswordGenerator {

        private static final String CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_+=<>?";

        public String generateRandomPassword(int length) {
            StringBuilder password = new StringBuilder();
            SecureRandom random = new SecureRandom();

            for (int i = 0; i < length; i++) {
                int randomIndex = random.nextInt(CHARACTERS.length());
                char randomChar = CHARACTERS.charAt(randomIndex);
                password.append(randomChar);
            }

            return password.toString();
        }
    }

    public void updatePassWord(String user, String pass) {
        try {
            String sql = " update Account set password = ?\n"
                    + "  where username = N'" + user + "'";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, pass);
            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(DAOAccount.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void main(String[] args) {
        DAOAccount dao = new DAOAccount();
        System.out.println(dao.addAccount(new Account(null, null, null, null, "15515115Tt", "tuanzzz", "tuan"), "khách hàng"));
    }
}
