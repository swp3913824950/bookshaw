/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import Entity.Comment;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LuuTu
 */
public class DAOComment extends DBContext {

    // Check if the user can comment
    public boolean hasUserCommentedOnProductInOrders(int productID, int customerID) {
        String sql = "SELECT COUNT(*) FROM Comment AS c "
                + "INNER JOIN OrderDetail AS od ON c.productID = od.productID "
                + "WHERE c.productID = ? AND c.customerID = ?";
        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, productID);
            statement.setInt(2, customerID);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                int count = resultSet.getInt(1);
                return count > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean checkCommentPurchaseMatch(int customerId, int productId) {

        int purchaseCount = 0;
        int commentCount = 0;

        // Get purchase count
       String sql1 = "SELECT SUM(Quantity) AS PurchaseCount " +  
               "FROM OrderDetail OD " +
               "JOIN CustomerOrder CO ON OD.OrderID = CO.OrderID " +
               "WHERE CO.CustomerID = ? AND OD.ProductID = ? " +
               "AND CO.Status IN ('Received', 'Rejected')";
        // Get comment count
        String sql2 = "SELECT COUNT(CommentID) AS CommentCount "
                + "FROM Comment "
                + "WHERE CustomerID = ? AND ProductID = ?";

        try {

            PreparedStatement stmt1 = connection.prepareStatement(sql1);
            stmt1.setInt(1, customerId);
            stmt1.setInt(2, productId);
            ResultSet rs1 = stmt1.executeQuery();
            if (rs1.next()) {
                purchaseCount = rs1.getInt("PurchaseCount");
            }

            PreparedStatement stmt2 = connection.prepareStatement(sql2);
            stmt2.setInt(1, customerId);
            stmt2.setInt(2, productId);
            ResultSet rs2 = stmt2.executeQuery();
            if (rs2.next()) {
                commentCount = rs2.getInt("CommentCount");
            }

        } catch (SQLException e) {
            // Handle errors
        }

        return purchaseCount > commentCount;

    }

    // Create a new comment
    public void createComment(Comment comment) {
        String sql = "INSERT INTO comment (content, productID, date, star, customerID) VALUES (?, ?, ?, ?, ?)";
        try ( PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, comment.getContent());
            statement.setInt(2, comment.getProductID());
            statement.setString(3, comment.getDate());
            statement.setInt(4, comment.getStar());
            statement.setInt(5, comment.getCustomerID());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Retrieve a comment by its ID
    public Comment getCommentById(int commentID) {
        Comment comment = null;
        try {
            String sql = "SELECT * FROM comment WHERE commentID = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, commentID);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                comment = new Comment();
                comment.setCommentID(resultSet.getInt("commentID"));
                comment.setContent(resultSet.getString("content"));
                comment.setProductID(resultSet.getInt("productID"));
                comment.setDate(resultSet.getString("date"));
                comment.setStar(resultSet.getInt("star"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return comment;
    }

    // Update a comment in the database
    public void updateComment(Comment comment) {
        try {
            String sql = "UPDATE comment SET content = ?, productID = ?, date = ?, star = ? WHERE commentID = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, comment.getContent());
            statement.setInt(2, comment.getProductID());
            statement.setString(3, comment.getDate());
            statement.setInt(4, comment.getStar());
            statement.setInt(5, comment.getCommentID());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Delete a comment from the database
    public void deleteComment(int commentID) {
        try {
            String sql = "DELETE FROM comment WHERE commentID = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, commentID);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // Retrieve all comment for a specific product
    public List<Comment> getCommentsForProduct(int productID) {
        List<Comment> comments = new ArrayList<>();
        try {
            String sql = "SELECT * FROM comment WHERE productID = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, productID);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Comment comment = new Comment();
                comment.setCommentID(resultSet.getInt("commentID"));
                comment.setContent(resultSet.getString("content"));
                comment.setProductID(resultSet.getInt("productID"));
                comment.setCustomerID(resultSet.getInt("customerID"));
                comment.setDate(resultSet.getString("date"));
                comment.setStar(resultSet.getInt("star"));
                comments.add(comment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return comments;
    }

    public static void main(String[] args) {
        Comment cmt = new Comment("hay", 319, 4, "", 2);
        DAOComment dao = new DAOComment();
        System.out.println(dao.checkCommentPurchaseMatch(1, 318));
    }
}
