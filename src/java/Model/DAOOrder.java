/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author LuuTu
 */
public class DAOOrder extends DBContext {

    public void checkout(int customerId, int sellerId, int cartId, String shipAddress, Date requiredDate) {
        DAOOrderDetail daoOD = new DAOOrderDetail();
        DAOCartDetail daoCD = new DAOCartDetail();
        DAOProduct daoP = new DAOProduct();
        try {

            // Create order  
            int orderId = createOrder(customerId, shipAddress, sellerId, requiredDate);
            // Create order details
            daoOD.createOrderDetails(orderId, cartId);
            daoP.reduceProductQuantity(cartId);
            // Clear cart
            daoCD.deleteAll(cartId);

            // Commit transaction  
            connection.commit();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void buyNow(int customerId, int sellerId, int productID,
            int quantity, double price, String shipAddress, Date requiredDate) {
        DAOOrderDetail daoOD = new DAOOrderDetail();
        DAOProduct daoP = new DAOProduct();
        try {

            // Create order  
            int orderId = createOrder(customerId, shipAddress, sellerId, requiredDate);
            // Create order details
            daoOD.createOrderDetailsBuyNow(orderId, productID, quantity, price);
            daoP.reduceProductQuantityBuyNow(productID, quantity);
            // Clear cart

            // Commit transaction  
            connection.commit();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public int createOrder(int customerId, String shipAddress, int sellerID, Date requiredDate) {

        // Insert into CustomerOrder table
        String sql = "INSERT INTO [CustomerOrder] (customerId, sellerId, shipAddress, orderDate, requiredDate, status) VALUES (?,?,?,GETDATE(),?,'Processing')";

        try {

            PreparedStatement stmt = connection.prepareStatement(sql);

            stmt.setInt(1, customerId);
            stmt.setInt(2, sellerID);
            stmt.setString(3, shipAddress);
            stmt.setDate(4, requiredDate);

            stmt.executeUpdate();

            // Get new order ID
            return getLastOrderId();

        } catch (SQLException e) {

            throw new RuntimeException(e);

        }

    }

    private int getLastOrderId() throws SQLException {
        String sql = "SELECT TOP 1 OrderID FROM CustomerOrder c ORDER BY c.OrderID DESC";

        try ( Statement stmt = connection.createStatement();  ResultSet rs = stmt.executeQuery(sql)) {
            if (rs.next()) {
                return rs.getInt("OrderID");
            } else {
                throw new RuntimeException("Error getting last OrderID");
            }
        }
    }

    public static void main(String[] args) {
        int customerId = 1;
        int cartId = 2;
        DAOAccount daoC = new DAOAccount();
        String shipAddress = daoC.getAccountById(13).getAddress();
        DAOOrder dao = new DAOOrder();
        // Perform checkout
        dao.checkout(customerId, 2, cartId, shipAddress, Date.valueOf("2023-10-26"));

    }

}
