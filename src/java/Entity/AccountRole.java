/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

/**
 *
 * @author LuuTu
 */
public class AccountRole {
    private int roleID;
    private int accountID;

    public AccountRole() {
    }

    public AccountRole(int roleID, int accountID) {
        this.roleID = roleID;
        this.accountID = accountID;
    }
    
    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    @Override
    public String toString() {
        return "AccountRole{" + "roleID=" + roleID + ", accountID=" + accountID + '}';
    }
    
    
}
