/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

/**
 *
 * @author LuuTu
 */
public class Comment {

    private int commentID;
    private String content;
    private int productID;
    private int customerID;
    private String date;
    private int star;

    public Comment() {
    }

    public Comment(int commentID, String content, int productID, int customerID, String date, int star) {
        this.commentID = commentID;
        this.content = content;
        this.productID = productID;
        this.customerID = customerID;
        this.date = date;
        this.star = star;
    }

    public Comment(String content, int productID, int customerID, String date, int star) {
        this.content = content;
        this.productID = productID;
        this.customerID = customerID;
        this.date = date;
        this.star = star;
    }

    public int getCommentID() {
        return commentID;
    }

    public void setCommentID(int commentID) {
        this.commentID = commentID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getStar() {
        return star;
    }

    public void setStar(int star) {
        this.star = star;
    }

}
