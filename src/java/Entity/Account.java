/*
     * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
     * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

import java.util.Date; // Import the Date class

/**
 *
 * @author admin
 */
public class Account {

    private int accountID;
    private Date dob;
    private String fullname;
    private String phone;
    private String address;
    private String password;
    private String username;
    private String email;

    public Account() {
    }

    public Account(int accountID, Date dob, String fullname, String phone, String address, String password, String username, String email) {
        this.accountID = accountID;
        this.dob = dob;
        this.fullname = fullname;
        this.phone = phone;
        this.address = address;
        this.password = password;
        this.username = username;
        this.email = email;
    }

    public Account(Date dob, String fullname, String phone, String address, String password, String username, String email) {
        this.dob = dob;
        this.fullname = fullname;
        this.phone = phone;
        this.address = address;
        this.password = password;
        this.username = username;
        this.email = email;
    }

    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return super.toString(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    
}
