/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

/**
 *
 * @author FPT
 */
public class UserGoogleDto {
    private String id;
    private String email;
    private String name;
    private String given_name;
    private String family_name;
    private String pictrue;

    public UserGoogleDto() {
    }

    public UserGoogleDto(String id, String email, String name, String given_name, String family_name, String pictrue) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.given_name = given_name;
        this.family_name = family_name;
        this.pictrue = pictrue;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGiven_name() {
        return given_name;
    }

    public void setGiven_name(String given_name) {
        this.given_name = given_name;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getPictrue() {
        return pictrue;
    }

    public void setPictrue(String pictrue) {
        this.pictrue = pictrue;
    }

    @Override
    public String toString() {
        return "UserGoogleDto{" + "id=" + id + ", email=" + email + ", name=" + name + ", given_name=" + given_name + ", family_name=" + family_name + ", pictrue=" + pictrue + '}';
    }
    
}
