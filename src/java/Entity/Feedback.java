/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Entity;

import java.sql.Date;

/**
 *
 * @author staytogether
 */
public class Feedback {
    private int feedbackID;
    private String content;
    private int customerID;
    private int orderID;
    private String date;

    public Feedback() {
    }

    public Feedback(int feedbackID, String conent, int customerID, int orderID, String date) {
        this.feedbackID = feedbackID;
        this.content = conent;
        this.customerID = customerID;
        this.orderID = orderID;
        this.date = date;
    }

    public Feedback(String conent, int customerID, int orderID, String date) {
        this.content = conent;
        this.customerID = customerID;
        this.orderID = orderID;
        this.date = date;
    }
    
    public int getFeedbackID() {
        return feedbackID;
    }

    public void setFeedbackID(int feedbackID) {
        this.feedbackID = feedbackID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    
    
    
}
