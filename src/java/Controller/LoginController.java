/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Entity.*;
import Model.*;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author admin
 */
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            //
            if (username == null) {
                username = session.getAttribute("username").toString();
                password = session.getAttribute("password").toString();
            }
            DAOAccount daoA = new DAOAccount();
            Account a = daoA.getAccount(username);
            if (daoA.checkAccount(username, password) && a != null) { 
                //
                //
                DAORole daoR = new DAORole();
                DAOCustomer daoB = new DAOCustomer();
                DAOCart daoC = new DAOCart();
                //
                int accountID = daoA.getAccount(username).getAccountID(); 
                String role = daoR.getRoleByAccountID(accountID); 
                //
                session.setAttribute("role", role);
                session.setAttribute("account", a);
                session.setAttribute("username", a.getUsername());
                //

                if (role.equals("Khách hàng")) {
                    Customer cus = daoB.findCustomer(accountID);
                    //
                    int customerID = cus.getCustomerID();
                    boolean checkCart = (daoC.findCartByID(customerID) != null);
                    if (checkCart) {
                        Cart c = daoC.findCartByID(customerID);
                        int cartID = c.getCartID();
                        session.setAttribute("cartID", cartID);
                    } else {
                        daoC.insert(cus);
                        Cart c = daoC.findCartByID(cus.getCustomerID());
                        session.setAttribute("cartID", c.getCartID());
                    }
                }

                sendRedirectByRole(response, role);
            } else if (!daoA.checkAccount(username, password)) {
                request.setAttribute("error", "Username or Password is incorrect");
                request.getRequestDispatcher("Login.jsp").forward(request, response);
            }
        }
    }

    private void sendRedirectByRole(HttpServletResponse response, String role) throws IOException {
        switch (role) {
            case "Admin":
                response.sendRedirect("dashboard");
                break;
            case "Khách hàng":
                response.sendRedirect("home");
                break;
            case "Nhân viên bán hàng":
                response.sendRedirect("listProduct");
                break;
            default:
                response.sendRedirect("home");
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
