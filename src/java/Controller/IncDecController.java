/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Entity.CartDetail;
import Entity.Product;
import Model.DAOCartDetail;
import Model.DAOProduct;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class IncDecController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DAOCartDetail dao = new DAOCartDetail();
        String action = request.getParameter("action");

        int id = Integer.parseInt(request.getParameter("id"));
        DAOProduct dao1 = new DAOProduct();
        Product pro = dao1.getProductByID(id);
        int totalQuantity = pro.getQuantity();
        int cart_id = (int) request.getSession().getAttribute("cartID");
        List<CartDetail> cart_detail = dao.findCartDetailByID(cart_id);

        if (action != null && id >= 1) {
            if (action.equals("increase")) {
                for (CartDetail c : cart_detail) {
                    if (c.getProductID() == id) {
                        int quantity = c.getQuantity();
                        quantity++;

                        if (pro.getQuantity() < quantity) {
                            String message = "Số lượng sản phẩm không đủ";
                            request.setAttribute("message", message);
                            request.getRequestDispatcher("Cart.jsp").forward(request, response);
                            return;
                        }
                        c.setQuantity(quantity);
                        dao.updateQuantity(c);
                        response.sendRedirect("Cart.jsp");
                    }
                }
            }
            if (action.equals("decrease")) {
                for (CartDetail c : cart_detail) {
                    if (c.getProductID() == id) {
                        if (c.getQuantity() > 1) {
                            int quantity = c.getQuantity();
                            quantity--;
                            c.setQuantity(quantity);
                            dao.updateQuantity(c);
                            break;
                        } else {
                            cart_detail.remove(c);
                            dao.delete(c.getProductID(), cart_id);
                        }
                    }
                }
                response.sendRedirect("Cart.jsp");
            }
        } else {
            response.sendRedirect("Cart.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();

    }
}
