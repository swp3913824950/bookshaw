/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Entity.Comment;
import Entity.Product;
import Model.DAOComment;
import Model.DAOCustomer;
import Model.DAOProduct;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author FPT
 */
@WebServlet(name = "DetailProductController", urlPatterns = {"/detail"})
public class DetailProductController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        //chi khach hang duoc checkout
        String checkacc = (String) session.getAttribute("role");
        if (checkacc != null && (checkacc.equals("Admin") || checkacc.equals("Nhân viên bán hàng"))) {
                response.sendRedirect("NotPermit.jsp");
                return;
            }
        //lay productID cua sp roi day len Detail.jsp
        String productID = request.getParameter("productID");
        DAOProduct dao = new DAOProduct();
        Product pro = dao.getProductByID(Integer.parseInt(productID));
        DAOComment daoCmt = new DAOComment();
        DAOCustomer daoC = new DAOCustomer();
        List<Comment> listComment = daoCmt.getCommentsForProduct(Integer.parseInt(productID));

        Map<String, List<Comment>> commentsByCustomer = new HashMap<>();

        for (Comment comment : listComment) {
            String customerName = daoC.getFullNameByCustomerID(comment.getCustomerID());

            if (commentsByCustomer.containsKey(customerName)) {
                commentsByCustomer.get(customerName).add(comment);
            } else {
                List<Comment> comments = new ArrayList<>();
                comments.add(comment);
                commentsByCustomer.put(customerName, comments);
            }
        }

        request.setAttribute("commentsByCustomer", commentsByCustomer);
        request.setAttribute("detailProduct", pro);
        request.setAttribute("listComment", commentsByCustomer);
        request.getRequestDispatcher("Detail.jsp").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
