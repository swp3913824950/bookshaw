/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Entity.CartDetail;
import Model.DAOCartDetail;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 *
 * @author ASUS
 */
public class DeleteController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        DAOCartDetail dao = new DAOCartDetail();
        String productID = request.getParameter("id");
        if(productID != null){
            int cart_id = (int) request.getSession().getAttribute("cartID");
            List<CartDetail> cart_detail = dao.findCartDetailByID(cart_id);
            if (cart_detail != null){
                for (int i = 0; i < cart_detail.size(); i++){
                    if (cart_detail.get(i).getProductID() == Integer.parseInt(productID)){
                        cart_detail.remove(cart_detail.get(i));
                        dao.delete(Integer.parseInt(productID), cart_id);
                    }
                }
            }
            response.sendRedirect("Cart.jsp");
        }
        else{
            response.sendRedirect("Cart.jsp");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
    }
}
