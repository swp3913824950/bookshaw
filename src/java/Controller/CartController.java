/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Entity.CartDetail;
import Entity.Product;
import Model.DAOCartDetail;
import Model.DAOProduct;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author ASUS
 */
public class CartController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        //chi khach hang duoc checkout
        String checkacc = (String) session.getAttribute("role");
        if (checkacc != null && (checkacc.equals("Admin") || checkacc.equals("Nhân viên bán hàng"))) {
            response.sendRedirect("NotPermit.jsp");
            return;
        }
        if (session.getAttribute("account") == null) {
            response.sendRedirect("Login.jsp");
            return;
        }
        DAOCartDetail daoB = new DAOCartDetail();
        int id = Integer.parseInt(request.getParameter("id"));
        int cart_id = (int) request.getSession().getAttribute("cartID");
        CartDetail cd = daoB.findCartDetailByID2(cart_id, id);
        int quantity = 1; // default value
        String quantityParam = request.getParameter("quantity");

        //check quantity
        DAOProduct daoP = new DAOProduct();
        Product product = daoP.getProductByID(id);
        if (product.getQuantity() < quantity) {
            String message = "Số lượng sản phẩm không đủ";
            request.setAttribute("message", message);
            request.getRequestDispatcher("Cart.jsp").forward(request, response);
            return;
        }
        if (quantityParam != null && !quantityParam.isEmpty()) {
            quantity = Integer.parseInt(quantityParam);
            if (product.getQuantity() < quantity) {
                String message = "Số lượng sản phẩm không đủ";
                request.setAttribute("message", message);
                request.getRequestDispatcher("Cart.jsp").forward(request, response);
                return;
            }
        }
        if (cd != null) {
            int prevQuantity = cd.getQuantity();
            cd.setQuantity(prevQuantity + quantity);
            daoB.updateQuantity(cd);
        } else {
            CartDetail cartDetail = new CartDetail(cart_id, id, quantity);
            daoB.insert(cartDetail);
        }
        response.sendRedirect("Cart.jsp");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
    }

    public void disp(HttpServletRequest request, HttpServletResponse response,
            String url) throws ServletException, IOException {
        RequestDispatcher disp
                = request.getRequestDispatcher(url);
        disp.forward(request, response);
    }

}
