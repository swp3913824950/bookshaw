/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Entity.Category;
import Entity.Product;
import Model.DAOCategory;
import Model.DAOProduct;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author FPT
 */
@WebServlet(name = "sortPro", urlPatterns = {"/sortPro"})
public class sortPro extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            //chi khach hang duoc checkout
            String checkacc = (String) session.getAttribute("role");
            if (checkacc == null || checkacc.trim().equals("") || !checkacc.equals("Khách hàng")) {
                response.sendRedirect("NotPermit.jsp");
                return;
            }
            DAOProduct dao = new DAOProduct();
            List<Product> searchPro = null; // Khởi tạo searchPro với giá trị mặc định là null
            String sortProduct = request.getParameter("sort");
            if (sortProduct != null && sortProduct.equals("Default")) {
                searchPro = dao.AllProducts();
            } else if (sortProduct != null && !sortProduct.isEmpty()) {
                switch (sortProduct) {
                    case "ascprice":
                        searchPro = dao.sortAscPrice();
                        break;
                    case "descprice":
                        searchPro = dao.sortDescPrice();
                        break;
                    case "latest":
                        searchPro = dao.sortLatestName();
                        break;
                    case "oldest":
                        searchPro = dao.sortOldestName();
                        break;
                    default:
                        break;
                }
            }
            DAOCategory daoC = new DAOCategory();
            List<Category> listC = daoC.getAllCategory();
            if (request.getParameter("cateID") != null) {
                request.setAttribute("proListByCate",
                        dao.getProductByCateID(request.getParameter("cateID")));
            }
            request.setAttribute("listCate", listC);
            request.setAttribute("listFeature", searchPro);
            request.getRequestDispatcher("Allproduct.jsp").forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
