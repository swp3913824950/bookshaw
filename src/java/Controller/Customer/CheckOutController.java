/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Customer;

import Entity.Account;
import Entity.CartDetail;
import Model.DAOAccount;
import Model.DAOCartDetail;
import Model.DAOCustomer;
import Model.DAOOrder;
import Model.DAOSeller;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LuuTu
 */
@WebServlet(name = "CheckOutController", urlPatterns = {"/checkout"})
public class CheckOutController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            doPost(request, response);
        }
    }

    //xu ly va them vao db
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        //chi khach hang duoc checkout
        String checkacc = (String) session.getAttribute("role");
        if (checkacc == null || checkacc.trim().equals("") || !checkacc.equals("Khách hàng")) {
            response.sendRedirect("NotPermit.jsp");
            return;
        }
        Account acc = (Account) session.getAttribute("account");
        DAOOrder daoO = new DAOOrder();
        DAOCustomer daoC = new DAOCustomer();
        if (acc.getFullname() == null || acc.getPhone() == null || acc.getAddress() == null) {
            String message = "Hãy hoàn thiện thông tin của bạn";
            request.setAttribute("message", message);
            request.getRequestDispatcher("navbar?go=profile").forward(request, response);
            return;
        }
        LocalDate today = LocalDate.now();
        LocalDate threeDaysLater = today.plusDays(3);

        Date requiredDate = Date.valueOf(threeDaysLater);

        int customerID = daoC.findCustomer(acc.getAccountID()).getCustomerID();
        int sellerID = Integer.parseInt(request.getParameter("sellerID"));
        String address = request.getParameter("address");

        int cartID = (int) session.getAttribute("cartID");

        //checkout
        daoO.checkout(customerID, sellerID, cartID, address, requiredDate);
        DAOAccount daoa = new DAOAccount();

        //gui mail cam on
        daoa.sendMail(acc.getEmail(), "Đơn hàng của quý khách đang được xử lý",
                "Kính gửi " + acc.getFullname() + ",\n"
                + "\n"
                + "Cảm ơn bạn đã mua hàng tại cửa hàng của chúng tôi. Chúng tôi rất trân trọng sự ủng hộ của bạn và hy vọng bạn đã có một trải nghiệm mua sắm thú vị tại cửa hàng của chúng tôi.\n"
                + "\n"
                + "Dưới đây là một số thông tin quan trọng mà chúng tôi đề nghị bạn kiểm tra:\n"
                + "\n"
                + "Tên người nhận: " + request.getParameter("name") + ".\n"
                + "\n"
                + "Số điện thoại người nhận: " + request.getParameter("phone") + ".\n"
                + "\n"
                + "Địa chỉ giao hàng: " + request.getParameter("address") + ".\n"
                + "\n"
                + "Nếu bạn có bất kỳ câu hỏi hoặc cần hỗ trợ bổ sung, đừng ngần ngại liên hệ với chúng tôi. Chúng tôi luôn sẵn sàng để giúp đỡ.\n"
                + "\n"
                + "Một lần nữa, chúng tôi xin chân thành cảm ơn bạn đã mua hàng tại cửa hàng của chúng tôi và hy vọng bạn sẽ tiếp tục ủng hộ chúng tôi trong tương lai.\n"
                + "\n"
                + "Trân trọng,\n"
                + "\n"
                + "BookSaw");
        String message = "Đặt hàng thành công";
        request.setAttribute("message", message);
        request.getRequestDispatcher("home?go=order&id=" + acc.getAccountID()).forward(request, response);

    }

    //ra form check out
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String checkacc = (String) session.getAttribute("role");
        if (checkacc == null || checkacc.trim().equals("") || checkacc.equals("Admin")) {
            response.sendRedirect("NotPermit.jsp");
        }
        DAOCartDetail daoCD = new DAOCartDetail();
        int cartID = (int) session.getAttribute("cartID");
        List<CartDetail> cartDetails = daoCD.findCartDetailByID(cartID);

        //check cart empty
        if (cartDetails.isEmpty()) {
            String message = "Giỏ hàng của bạn đang trống";
            request.setAttribute("message", message);
            request.getRequestDispatcher("Cart.jsp").forward(request, response);

        } else {

            //khong empty chuyen sang checkout
            String service = request.getParameter("go");
            if (service.equals("start")) {
                try {
                    DAOSeller dao = new DAOSeller();
                    ResultSet resultSet = dao.getAllSellerInfor();

                    List<Map<String, Object>> sellerList = new ArrayList<>();
                    while (resultSet.next()) {
                        Map<String, Object> seller = new HashMap<>();
                        seller.put("sellerID", resultSet.getInt("sellerID"));
                        seller.put("fullname", resultSet.getString("fullname"));
                        sellerList.add(seller);
                    }

                    request.setAttribute("sellerList", sellerList);
                    request.setAttribute("cartDetails", cartDetails);
                    RequestDispatcher dispatcher = request.getRequestDispatcher("/CheckOut.jsp");

                    dispatcher.forward(request, response);

                } catch (SQLException ex) {
                    Logger.getLogger(CheckOutController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                String contextPath = request.getContextPath();
                response.sendRedirect(contextPath + "/Cart.jsp");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
