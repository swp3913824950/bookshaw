/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Customer;

import Entity.Account;
import Entity.OrderDetail;
import Model.DAOProduct;
import Model.DAOSeller;
import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author LuuTu
 */
@WebServlet(name = "AcceptOrderController", urlPatterns = {"/acceptOrder"})
public class AcceptOrderController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String orderID = request.getParameter("orderid");
            String status = request.getParameter("status");
            DAOSeller dao = new DAOSeller();
            if (status.equals("accept")) {
                status = "Received";
            } else {
                status = "Rejected";
                DAOProduct daoP = new DAOProduct();
                List<OrderDetail> orderDetail = dao.viewCustomerOrderDetailList(Integer.parseInt(orderID));
                for (OrderDetail od: orderDetail) {
                    daoP.updateQuantityRejected(od.getProductID(), od.getQuantity());
                }
            }
            dao.changeStatus(status, orderID);
            HttpSession session = request.getSession();
            Account a = (Account) session.getAttribute("account");
            int accountId = 0;
            if (a != null) {
                accountId = a.getAccountID();
            }
            //out.print(accountId +"  " +a);
            request.getRequestDispatcher("home?go=order&id=" + accountId).forward(request, response);

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
