/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Customer;

import Entity.Account;
import Entity.Feedback;
import Model.DAOCustomer;
import Model.DAOFeedback;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author LuuTu
 */
@WebServlet(name = "FeedbackController", urlPatterns = {"/addFeedback"})
public class FeedbackController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FeedbackController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FeedbackController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Get form data
        String content = request.getParameter("content");
        int orderID = Integer.parseInt(request.getParameter("orderID"));

        // Validation
        if (content == null || content.trim().isEmpty()) {
            HttpSession session = request.getSession();
            DAOCustomer daoA = new DAOCustomer();

            Account account = (Account) session.getAttribute("account");
            String message = "Hãy điền phản hồi";
            request.setAttribute("message", message);
            request.getRequestDispatcher("home?go=order&id=" + account.getAccountID()).forward(request, response);
            return;
        }

        // Create feedback object
        Feedback feedback = new Feedback();
        feedback.setContent(content);
        feedback.setOrderID(orderID);

        // Get current logged in user
        HttpSession session = request.getSession();
        DAOCustomer daoA = new DAOCustomer();

        Account account = (Account) session.getAttribute("account");
        int customerID = daoA.findCustomer(account.getAccountID()).getCustomerID();

        feedback.setCustomerID(customerID);

        // Get current date
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(new java.util.Date());

        feedback.setDate(date);

        // Save feedback
        DAOFeedback dao = new DAOFeedback();
        dao.createFeedback(feedback);

        // Redirect back to same page
        String prevPage = request.getHeader("Referer");
        response.sendRedirect(prevPage);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
