/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Seller;

import Entity.Account;
import Entity.CustomerOrder;
import Entity.OrderDetail;
import Model.DAOSeller;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author staytogether
 */
@WebServlet(name = "myorder", urlPatterns = {"/order"})
public class myorder extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet myorder</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet myorder at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        //chi khach hang duoc checkout
        String checkacc = (String) session.getAttribute("role");
        if (checkacc == null || checkacc.trim().equals("") || !checkacc.equals("Nhân viên bán hàng")) {
            response.sendRedirect("NotPermit.jsp");
            return;
        }
        DAOSeller ordao = new DAOSeller();
        List<CustomerOrder> list1;
        String selectedStatus = request.getParameter("viewstatus");
        Account a = (Account) session.getAttribute("account");
        int id = a.getAccountID();
        int sellerID = ordao.getSellerIDByAccountID(id);
        if (a != null) {
            if (selectedStatus != null && selectedStatus.equals("All")) {
                list1 = ordao.viewOrderList(sellerID);
            } else if (selectedStatus != null && !selectedStatus.isEmpty()) {
                list1 = ordao.searchByStatus(selectedStatus, sellerID);
            } else {
                list1 = ordao.viewOrderList(sellerID);
            }
            request.setAttribute("olist", list1);
        }
        request.getRequestDispatcher("order.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
