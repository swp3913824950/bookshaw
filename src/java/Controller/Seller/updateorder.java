/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller.Seller;

import Entity.OrderDetail;
import Model.DAOProduct;
import Model.DAOSeller;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author staytogether
 */
@WebServlet(name="updateorder", urlPatterns={"/updateorder"})
public class updateorder extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String orderID = request.getParameter("orderid");
        String orderDate = request.getParameter("orderDate");
        String shippedDate = request.getParameter("shippedDate");
        String requiredDate = request.getParameter("requiredDate");
        String status = request.getParameter("status");
        DAOSeller dao = new DAOSeller();
        if (status != null && (shippedDate.compareTo(requiredDate) >= 0 && (shippedDate.compareTo(orderDate) >= 0) && (requiredDate.compareTo(orderDate) >= 0))) {
            dao.changeStatus(status, orderID);
            dao.editDate(requiredDate, shippedDate, status, orderID);
        }else if((shippedDate.compareTo(requiredDate) < 0) || (shippedDate.compareTo(orderDate) < 0) || (requiredDate.compareTo(orderDate) < 0)){
            request.setAttribute("msg", "Please input date again.");
            request.getRequestDispatcher("getorder").forward(request, response);
        }
        if(status.equals("Accepted")){
            status = "Accepted";
        }else if(status.equals("Rejected")){
            status = "Rejected";
            DAOProduct daoP = new DAOProduct();
                List<OrderDetail> orderDetail = dao.viewCustomerOrderDetailList(Integer.parseInt(orderID));
                for (OrderDetail od: orderDetail) {
                    daoP.updateQuantityRejected(od.getProductID(), od.getQuantity());
                }
        }
        dao.changeStatus(status, orderID);
        response.sendRedirect("order");


    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}