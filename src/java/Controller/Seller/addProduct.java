/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Seller;

import Entity.Account;
import Model.DAOSeller;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.nio.file.Paths;

/**
 *
 * @author FPT
 */
@MultipartConfig()
@WebServlet(name = "addProduct", urlPatterns = {"/add"})
public class addProduct extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String productName = request.getParameter("productName");
        String auName = request.getParameter("auName");
        String date = request.getParameter("date");
        String price = request.getParameter("price");
        String quantity = request.getParameter("quantity");
        String category = request.getParameter("category");
        Part image = request.getPart("image");
        if (quantity == null || quantity.trim().isEmpty() || Integer.parseInt(quantity) <= 0 || price == null || price.trim().isEmpty() || Double.parseDouble(price) < 1000) {
            request.setAttribute("error", "Thêm sản phẩm thất bại, số lượng > 0 và giá phải >= 1000 đồng");
            request.getRequestDispatcher("listProduct").forward(request, response);
            return;
        }
        if (image == null || image.getSize() == 0) {
            request.setAttribute("error", "Thêm sản phẩm thất bại,Vui lòng chọn một file ảnh");
            request.getRequestDispatcher("listProduct").forward(request, response);
            return;
        }

        String filename = Paths.get(image.getSubmittedFileName()).getFileName().toString();
        String path = request.getServletContext().getRealPath("/images");
        image.write(path + "/" + filename);
        String pathStore = "images" + File.separator + filename;
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("account");
        int id = a.getAccountID();
        DAOSeller dao = new DAOSeller();
        int sellerID = dao.getSellerIDByAccountID(id);

        if (productName.trim().isEmpty() || productName == null || auName.trim().isEmpty() || auName == null || price.trim().isEmpty() || price == null || quantity.trim().isEmpty() || quantity == null) {
            request.setAttribute("error", "Thêm sản phẩm thất bại, phải nhập đủ thông tin");
            request.getRequestDispatcher("listProduct").forward(request, response);
            return;
        }

        dao.insertProduct(productName, auName, date, price, quantity, category, sellerID, pathStore);
        response.sendRedirect("listProduct");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
