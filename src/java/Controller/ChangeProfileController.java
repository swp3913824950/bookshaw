/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller;

import Entity.Account;
import Model.DAOAccount;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author staytogether
 */
@WebServlet(name="ChangeProfileController", urlPatterns={"/profile"})
public class ChangeProfileController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangeProfileController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangeProfileController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        request.getRequestDispatcher("Profile.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String fullname = request.getParameter("fullname");
        String pass = request.getParameter("pass");
        String email = request.getParameter("email");
        String address = request.getParameter("address");
        String phone = request.getParameter("phone");
        String dobStr = request.getParameter("dob");
        java.util.Date dobUtilDate = null;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                dobUtilDate = sdf.parse(dobStr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            java.sql.Date dob = null;
            if (dobUtilDate != null) {
                dob = new java.sql.Date(dobUtilDate.getTime());
            }
             HttpSession session = request.getSession();
        Account a = (Account)session.getAttribute("account");
        // Get the current date
            java.util.Date currentDate = new java.util.Date();

            if (dob.after(currentDate)) {
                request.setAttribute("error", "Ngày sinh không hợp lệ");
                request.getRequestDispatcher("/Profile.jsp").forward(request, response);
            }
        // Validation for Full Name
            if (fullname == null || fullname.isEmpty() || !fullname.matches("^[a-zA-Z-'\\s ]+$")) {
                request.setAttribute("error", "Tên không hợp lệ");
        request.getRequestDispatcher("/Profile.jsp").forward(request, response);
                return;
            }

// Validation for Phone Number
            if (phone == null || phone.isEmpty() || !phone.matches("0[0-9]{9}")) {
                request.setAttribute("error", "Số điện thoại không hợp lệ");
        request.getRequestDispatcher("/Profile.jsp").forward(request, response);
                return;
            }


// Validation for Email
            if (email == null || email.isEmpty() || !email.matches(".*@.*\\..*")) {
                request.setAttribute("error", "Email không hợp lệ, phải có @ và .");
        request.getRequestDispatcher("/Profile.jsp").forward(request, response);
                return;
            }
            DAOAccount daoA = new DAOAccount();
            if (email.equals(a.getEmail())){} else {
            int existAccount = daoA.checkAccountByNameAndEmail(null, email);
            if (existAccount >= 1) {
                request.setAttribute("error", "Tên đăng nhập đã được dùng");
        request.getRequestDispatcher("/Profile.jsp").forward(request, response);
                return;
            } else if (existAccount == -1) {
                request.setAttribute("error", "Email đã được dùng");
        request.getRequestDispatcher("/Profile.jsp").forward(request, response);
                return;
            }
            }
   
       
        a.setEmail(email);
        a.setFullname(fullname);
        a.setPassword(pass);   
        a.setPhone(phone);
        a.setAddress(address);
        a.setDob(dob);
        DAOAccount adao = new DAOAccount();
        if(adao.changeProfile(a)){
            request.setAttribute("success", "Cập nhật thành công.");
            
        }
        request.getRequestDispatcher("/Profile.jsp").forward(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
