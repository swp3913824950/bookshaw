/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Entity.Account;
import Model.DAOAccount;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author LuuTu
 */
@WebServlet(name = "RegisterController", urlPatterns = {"/RegisterController"})
public class RegisterController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            request.setAttribute("dob", request.getParameter("dob"));
            request.setAttribute("fullname", request.getParameter("fullname"));
            request.setAttribute("phone", request.getParameter("phone"));
            request.setAttribute("password", request.getParameter("password"));
            request.setAttribute("reEnterPassword", request.getParameter("re-enterPassword"));
            request.setAttribute("address", request.getParameter("address"));
            request.setAttribute("username", request.getParameter("username"));
            request.setAttribute("email", request.getParameter("email"));

            String dobStr = request.getParameter("dob");
            java.util.Date dobUtilDate = null;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                dobUtilDate = sdf.parse(dobStr);
            } catch (ParseException e) {
                request.setAttribute("error", "Invalid date format");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
                return;
            }

            java.sql.Date dob = new java.sql.Date(dobUtilDate.getTime());

// Get the current date
            java.util.Date currentDate = new java.util.Date();

            if (dob.after(currentDate)) {
                request.setAttribute("error", "Ngày sinh không hợp lệ");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
            }
            String fullname = request.getParameter("fullname");
            String phone = request.getParameter("phone");
            String address = request.getParameter("address");
            String password = request.getParameter("password");
            String reEnterPassword = request.getParameter("re-enterPassword");
            String username = request.getParameter("username");
            String email = request.getParameter("email");
            DAOAccount daoA = new DAOAccount();

// Validation for Full Name
            if (fullname == null || fullname.isEmpty() || !fullname.matches("^[a-zA-Z-'\\s ]+$")) {
                request.setAttribute("error", "Tên không hợp lệ");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
                return;
            }

// Validation for Phone Number
            if (phone == null || phone.isEmpty() || !phone.matches("0[0-9]{9}")) {
                request.setAttribute("error", "Số điện thoại không hợp lệ");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
                return;
            }

// Validation for Password
            if (password.length() < 8) {
                request.setAttribute("error", "Mật khẩu phải có hơn 8 ký tự");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
                return;
            }
// Validation for Username
            if (username == null || username.isEmpty() || !username.matches("^[a-zA-Z0-9]+$") || username.length() < 6) {
                request.setAttribute("error", "Tên đăng nhập chỉ có thể là chữ và số, phải có hơn 6 ký tự");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
                return;
            }
// Validation for Email
            if (email == null || email.isEmpty() || !email.matches(".*@.*\\..*")) {
                request.setAttribute("error", "Email không hợp lệ, phải có @ và .");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
                return;
            }

            int existAccount = daoA.checkAccountByNameAndEmail(username, email);
            if (existAccount >= 1) {
                request.setAttribute("error", "Tên đăng nhập đã được dùng");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
                return;
            } else if (existAccount == -1) {
                request.setAttribute("error", "Email đã được dùng");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
                return;
            }

// Validation for Password and Re-entered Password
            if (!password.equals(reEnterPassword)) {
                request.setAttribute("error", "Hai mật khẩu phải khớp với nhau");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
                return;
            }

// If all validations pass, proceed with account creation
            Account acc = new Account(dob, fullname, phone, address, password, username, email);
            boolean isInserted = daoA.addAccount(acc, "khách hàng");

            if (isInserted) {
                request.setAttribute("success", "Đăng kí thành công!");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
            } else {
                request.setAttribute("error", "Đăng kí thất bại!");
                request.getRequestDispatcher("/Register.jsp").forward(request, response);
            }

        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
