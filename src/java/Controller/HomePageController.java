/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Entity.Account;
import Entity.Category;
import Entity.Customer;
import Entity.CustomerOrder;
import Entity.OrderDetail;
import Entity.Product;
import Model.DAOCategory;
import Model.DAOComment;
import Model.DAOCustomer;
import Model.DAOProduct;
import Model.DAOSeller;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author FPT
 */
public class HomePageController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            //chi khach hang duoc checkout
            String checkacc = (String) session.getAttribute("role");
            if (checkacc != null && (checkacc.equals("Admin") || checkacc.equals("Nhân viên bán hàng"))) {
                response.sendRedirect("NotPermit.jsp");
                return;
            }
            String service = request.getParameter("go");
            if (service == null) {
                service = "displayAll";
            }
            if (service.equals("displayAll")) {
                DAOProduct dao = new DAOProduct();
                List<Product> list = dao.getAllProducts();
                List<Product> bestSeller = dao.getBestSellProduct();
                DAOCategory daoC = new DAOCategory();
                List<Category> listC = daoC.getAllCategory();
                if (request.getParameter("cateID") != null) {
                    request.setAttribute("proListByCate",
                            dao.getProductByCateID(request.getParameter("cateID")));
                }
                request.setAttribute("bestSeller", bestSeller);
                request.setAttribute("listCate", listC);
                request.setAttribute("listFeature", list);
                request.getRequestDispatcher("Home.jsp").forward(request, response);
            }
//            if (service.equals("search")) {
//                DAOProduct dao = new DAOProduct();
//                String search = request.getParameter("search-box");
//                List<Product> listSearch = dao.getProductByName(search);
//                request.setAttribute("listSearch", listSearch);
//                List<Product> list = dao.getAllProducts();
//                DAOCategory daoC = new DAOCategory();
//                List<Category> listC = daoC.getAllCategory();
//                if (request.getParameter("cateID") != null) {
//                    request.setAttribute("proListByCate",
//                            dao.getProductByCateID(request.getParameter("cateID")));
//                }
//                request.setAttribute("listCate", listC);
//                request.setAttribute("listFeature", list);
//                request.getRequestDispatcher("Search.jsp").forward(request, response);
//            }
            if (service.equals("order")) {
                DAOSeller dao = new DAOSeller();
                DAOCustomer daoB = new DAOCustomer();
                int accountID = Integer.parseInt(request.getParameter("id"));
                Customer cus = daoB.findCustomer(accountID);
                int customerID = cus.getCustomerID();
                List<CustomerOrder> list = dao.viewCustomerOrderList(customerID);
                request.setAttribute("olist", list);
                request.getRequestDispatcher("CustomerOrder.jsp").forward(request, response);
            }
            if (service.equals("orderDetail")) {
                DAOSeller dao = new DAOSeller();
                int orderID = Integer.parseInt(request.getParameter("orderID"));
                List<OrderDetail> orderDetails = dao.viewCustomerOrderDetailList(orderID);

                //List<Boolean> hasCommentedOnProducts = new ArrayList<>();
                DAOComment daoCmt = new DAOComment();
//                HttpSession session = request.getSession();
                Account account = (Account) session.getAttribute("account");
                DAOCustomer daoC = new DAOCustomer();
                Customer customer = daoC.findCustomer(account.getAccountID());
                String status = request.getParameter("status");

//                for (OrderDetail detail : orderDetails) {
//                    // Check if the customer has commented on each product on order
//                    boolean hasCommented = daoCmt.checkCommentPurchaseMatch(customer.getCustomerID(),detail.getProductID());
//                    hasCommentedOnProducts.add(hasCommented);
//                }
                request.setAttribute("customerID", customer.getCustomerID());
                request.setAttribute("status", status);
                // request.setAttribute("hasUserCommentedOnProducts", hasCommentedOnProducts);
                request.setAttribute("olist", orderDetails);
                request.getRequestDispatcher("CustomerOrderDetail.jsp").forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
;
