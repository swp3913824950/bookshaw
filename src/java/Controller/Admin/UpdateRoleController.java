/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Admin;

import Model.DAOAccountRole;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author LuuTu
 */
@WebServlet(name = "UpdateRoleController", urlPatterns = {"/updateRole"})
public class UpdateRoleController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            DAOAccountRole dao = new DAOAccountRole();
            int accountID = Integer.parseInt(request.getParameter("accountId"));
            String currentFullname = request.getParameter("currentFullname");
            int newRoleId = Integer.parseInt(request.getParameter("newRole"));

            // Lấy roleName tương ứng với newRoleId
            String newRole = getRoleName(newRoleId);

            String currentRole = request.getParameter("currentRole");

            if (currentRole.equals(newRole)) {
                out.println("<script>alert('Vai trò mới không được trùng với vai trò hiện tại');"
                        + "window.location.href='dashboard';</script>");
            } else {
                dao.changeAccountRole(accountID, newRoleId);
                out.println("<script>alert('Thành công');"
                        + "window.location.href='dashboard';</script>");
            }

        }
    }

    private String getRoleName(int roleId) {
        switch (roleId) {
            case 1:
                return "Khách hàng";
            case 2:
                return "Nhân viên bán hàng";
            case 3:
                return "Quản lý";
            case 4:
                return "Admin";
            default:
                return "Unknown";
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
