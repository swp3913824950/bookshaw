package Controller.Admin;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
import Entity.Account;
import Model.DAOAccount;
import Model.DAORole;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.ResultSet;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Vector;

/**
 *
 * @author admin
 */
@WebServlet(urlPatterns = {"/dashboard"})
public class DashboardController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            //chi khach hang duoc checkout
            String checkacc = (String) session.getAttribute("role");
            if (checkacc == null || checkacc.trim().equals("") || !checkacc.equals("Admin")) {
                response.sendRedirect("NotPermit.jsp");
                return;
            }
            String service = request.getParameter("go");
            if (service == null || service.equals("")) {
                service = "displayAll";
            }
            if (service.equals("displayAll")) {
                DAOAccount daoAccount = new DAOAccount();
                ResultSet account = daoAccount.getAllNonAdminAccount();
                request.setAttribute("account", account);
                request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
            }
            if (service.equals("add")) {
                DAOAccount daoA = new DAOAccount();
                //Luu lai thong tin neu co nhap sai
                request.setAttribute("dob", request.getParameter("birthDate"));
                request.setAttribute("fullname", request.getParameter("fullname"));
                request.setAttribute("phone", request.getParameter("phone"));
                request.setAttribute("password", request.getParameter("password"));
                request.setAttribute("reEnterPassword", request.getParameter("re-enterPassword"));
                request.setAttribute("address", request.getParameter("address"));
                request.setAttribute("username", request.getParameter("username"));
                request.setAttribute("email", request.getParameter("email"));
                request.setAttribute("role", request.getParameter("role"));
                String dobStr = request.getParameter("birthDate");
                java.util.Date dobUtilDate = null;
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    dobUtilDate = sdf.parse(dobStr);
                } catch (ParseException e) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Ngày sinh không hợp lệ");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                    return;
                }

                java.sql.Date dob = new java.sql.Date(dobUtilDate.getTime());

// Get the current date
                java.util.Date currentDate = new java.util.Date();

                if (dob.after(currentDate)) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Ngày sinh không hợp lệ");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                }
                String fullname = request.getParameter("fullname");
                String phone = request.getParameter("phone");
                String address = request.getParameter("address");
                String password = request.getParameter("password");
                String reEnterPassword = request.getParameter("re-enterPassword");
                String username = request.getParameter("username");
                String email = request.getParameter("email");
                String role = request.getParameter("role");

// Validation for Full Name
                if (fullname == null || fullname.isEmpty() || !fullname.matches("^[a-zA-Z-'\\s ]+$")) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Tên không hợp lệ");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                    return;
                }

// Validation for Phone Number
                if (phone == null || phone.isEmpty() || !phone.matches("0[0-9]{9}")) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Số điện thoại không hợp lệ");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                    return;
                }

// Validation for Password
                if (password.length() < 8) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Mật khẩu phải có hơn 8 ký tự");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                    return;
                }
// Validation for Username
                if (username == null || username.isEmpty() || !username.matches("^[a-zA-Z0-9]+$") || username.length() < 6) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Tên đăng nhập chỉ có thể là chữ và số, phải có hơn 6 ký tự");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                    return;
                }
// Validation for Email
                if (email == null || email.isEmpty() || !email.matches(".*@.*\\..*")) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Email không hợp lệ, phải có @ và .");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                    return;
                }

                int existAccount = daoA.checkAccountByNameAndEmail(username, email);
                if (existAccount >= 1) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Tên đăng nhập đã được dùng");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                    return;
                } else if (existAccount == -1) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Email đã được dùng");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                    return;
                }

// Validation for Password and Re-entered Password
                if (!password.equals(reEnterPassword)) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Hai mật khẩu phải khớp với nhau");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                    return;
                }

// If all validations pass, proceed with account creation
                Account acc = new Account(dob, fullname, phone, address, password, username, email);
                boolean isInserted = daoA.addAccount(acc, role);

                if (isInserted) {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("success", "Register successfully!");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                } else {
                    ResultSet account = daoA.getAllNonAdminAccount();
                    request.setAttribute("account", account);
                    request.setAttribute("error", "Register fail!");
                    request.getRequestDispatcher("/Dashboard.jsp").forward(request, response);
                }

            }

            if (service.equals("delete")) {
                int id = Integer.parseInt(request.getParameter("id"));
                DAOAccount daoA = new DAOAccount();
                daoA.deleteAccount(id);
                if (daoA.getAccountById(id) == null) {
                    out.println("<script>alert('Thành công');"
                            + "window.location.href='dashboard';</script>");
                } else {
                    out.println("<script>alert('Không thành công');"
                            + "window.location.href='dashboard';</script>");
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
