package Controller;

import jakarta.servlet.RequestDispatcher;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = {"/ChatServlet"})
public class ChatServlet extends HttpServlet {

    private static final String API_KEY = "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";
    private static final String HISTORY_ATTRIBUTE = "chatHistory";

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession(true);
            //
            String checkacc = (String) session.getAttribute("role");
            if (checkacc != null && (checkacc.equals("Admin") || checkacc.equals("Nhân viên bán hàng"))) {
                response.sendRedirect("NotPermit.jsp");
                return;
            }

            // Retrieve or create the chat history list in the session
            List<String> chatHistory = (List<String>) session.getAttribute(HISTORY_ATTRIBUTE);
            if (chatHistory == null) {
                chatHistory = new ArrayList<>();
                session.setAttribute(HISTORY_ATTRIBUTE, chatHistory);
            }
            String clearChat = request.getParameter("clearChat");
            if (clearChat != null && !clearChat.isEmpty()) {
                // Xóa toàn bộ lịch sử chat
                chatHistory.clear();
                // Cập nhật lại session và request attribute
                session.setAttribute(HISTORY_ATTRIBUTE, chatHistory);
                request.setAttribute("chatHistory", chatHistory);

                // Kiểm tra nếu có tham số "continueChat", chuyển hướng quay lại trang hỏi tiếp
                String continueChatParam = request.getParameter("continueChat");
                if (continueChatParam != null && continueChatParam.equals("true")) {
                    // Forward to the JSP page for further input
                    RequestDispatcher dispatcher = request.getRequestDispatcher("TestGPT.jsp");
                    dispatcher.forward(request, response);
                    return;
                }
            }

            String message = request.getParameter("message");

            // Check if the user entered a message
            if (message != null && !message.isEmpty()) {
                String chatGPTResponse = chatGPT(message);

                // Add the user's input and ChatGPT's response to the chat history
                chatHistory.add("User: " + message);
                chatHistory.add("ChatGPT Response: " + chatGPTResponse);

                // Set the chat history as a request attribute
                request.setAttribute("chatHistory", chatHistory);

                // Forward to the JSP page
                RequestDispatcher dispatcher = request.getRequestDispatcher("ChatBox.jsp");
                dispatcher.forward(request, response);
            } else {
                // Handle the case where no message is entered
                response.setContentType("text/html");
                response.getWriter().write("<html><body>Please enter a message.</body></html>");
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    private String chatGPT(String message) {
        String url = "https://api.openai.com/v1/chat/completions";
        String model = "gpt-3.5-turbo";

        try {
            // Encode the message using UTF-8
            String encodedMessage = URLEncoder.encode(message, StandardCharsets.UTF_8.toString()).replace("+", "%20");

            // Create the HTTP POST request
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Authorization", "Bearer " + API_KEY);
            con.setRequestProperty("Content-Type", "application/json");

            // Build the request body
            String body = "{\"model\": \"" + model + "\", \"messages\": [{\"role\": \"user\", \"content\": \"" + encodedMessage + "\"}]}";
            con.setDoOutput(true);
            OutputStreamWriter writer = new OutputStreamWriter(con.getOutputStream());
            writer.write(body);
            writer.flush();
            writer.close();

            // Get the response
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // Return the extracted contents of the response
            return extractContentFromResponse(response.toString());

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String extractContentFromResponse(String response) {
        int startMarker = response.indexOf("content") + 11;
        int endMarker = response.indexOf("\"", startMarker);
        int maxLength = 8000;
        int actualLength = Math.min(endMarker - startMarker, maxLength);
        return response.substring(startMarker, startMarker + actualLength);
    }

}
