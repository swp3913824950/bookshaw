<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.Vector, Entity.Account, java.sql.ResultSet" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
            body{
                background-color: #f3f2ec;
                font-family: 'Varela Round', sans-serif;
                font-size: 13px;
                margin: 0;
                padding: 0;
            }
            .container {
                max-width: 1200px;
                margin: 0 auto;
                padding: 30px 20px;

            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0, 0, 0, .05);

            }
            .table-title {
                padding-bottom: 15px;
                background: #299be4;
                color: #fff;
                padding: 16px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }

            .table-title .btn {
                color: #566787;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }

            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #f2f2f2;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }

            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 12px 15px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 22px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }
            table{
                width: 100%;

                text-align: center;
            }
            table thead th{
                text-align: center;

            }

        </style>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>
    <body>
        <%@include file="MenuAdmin.jsp" %>


        <!-- form update -->
        <div class="modal fade" id="updateRoleModal" tabindex="-1" role="dialog" aria-labelledby="updateRoleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="updateRole" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="updateRoleModalLabel" style="margin-left: 30%;">Thông tin tài khoản</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" id="accountId" name="accountId">
                            <!-- Display the current account information here -->
                            <div class="form-group">
                                <label for="currentFullname">Họ và tên</label>
                                <input type="text" class="form-control" id="currentFullname" name="currentFullname" readonly>
                            </div>
                            <div class="form-group">
                                <label for="currentRole">Vai trò cũ</label>
                                <input type="text" class="form-control" id="currentRole" name="currentRole" readonly>
                            </div>

                            <div class="form-group">
                                <label for="newRole">Chọn vai trò mới</label>
                                <select name="newRole" id="newRole" class="form-select" aria-label="Default select example">
                                    <option value="1">Khách hàng</option>
                                    <option value="2">Nhân viên bán hàng</option>
                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Ðóng</button>
                            <button type="submit" class="btn btn-primary">Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- end form update -->
        <!-- Modal -->
        <div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="dashboard?go=add" method="post">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="exampleModalLabel" style="margin-left: 30%; ">Thêm tài khoản mới</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <!-- Thêm form nhập liệu cho sản phẩm ở đây -->
                            <div class="error" style="color:red"> ${error} </div>
                            <div class="form-group">
                                <label for="productName">Họ và tên</label>
                                <input type="text" class="form-control" id="productName" name="fullname" placeholder="họ và tên" required>
                            </div>
                            <div class="form-group">
                                <label for="productName">Ngày sinh</label>
                                <input type="date" class="form-control" id="birthDate" name="birthDate" placeholder="ngày sinh" required>
                            </div>
                            <div class="form-group">
                                <label for="productName">Số điện thoại</label>
                                <input type="tel" class="form-control" id="phoneNumber" name="phone" placeholder="số điện thoại" required>
                            </div>
                            <div class="form-group">
                                <label for="productName">Địa chỉ</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="địa chỉ" required>
                            </div>
                            <div class="form-group">
                                <label for="productName">Tên đăng nhập</label>
                                <input type="text" class="form-control" id="username" name="username" placeholder="tên đăng nhập" required>
                            </div>
                            <div class="form-group">
                                <label for="productName">Mật khẩu</label>
                                <input type="password" class="form-control" id="password" name="password" placeholder="mật khẩu" required>
                            </div>
                            <div class="form-group">
                                <label for="productName">Nhập lại mật khẩu</label>
                                <input type="password" class="form-control" id="re-enterPassword" name="re-enterPassword" placeholder="nhập lại mật khẩu" required>
                            </div>
                            <div class="form-group">
                                <label for="productName">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="email" required>
                            </div>
                            <div class="form-group">
                                <label for="productName">Vai trò</label>
                                <select name="role" class="form-select" aria-label="Default select example">
                                    <option value="khách hàng">Khách hàng</option>
                                    <option value="nhân viên bán hàng">Nhân viên Bán hàng</option>

                                </select>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Ðóng</button>
                            <button type="submit" class="btn btn-primary">Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container">
            <div class="table-responsive">
                <div class="table-wrapper">

                    <div class="table-title">
                        <div class="row">
                            <div class="col-xs-5">
                                <h2>Quản lý tài khoản</h2>
                            </div>
                            <div id="productForm" style="display: none;">
                            </div>
                            <div class="col-xs-7">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addProductModal"><i class="material-icons">&#xE147;</i> <span>Thêm tài khoản mới</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="error" style="color:red"> ${error} </div>
                    <br>
                    <table border="3">
                        <thead>
                            <tr>
                                <th>Họ và tên</th>
                                <th>Ngày sinh</th>
                                <th>Số điện thoại</th>
                                <th>Ðịa chỉ</th>
                                <th>Tên đăng nhập</th>
                                <th>Mật khẩu</th>
                                <th>Email</th>
                                <th>Vai trò</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <% 
                            ResultSet account = (ResultSet)request.getAttribute("account");
                            while(account.next()){ 
                            %>
                            <tr>
                                <td><%= account.getString(2)%></td>
                                <td><%= account.getString(3)%></td>
                                <td><%= account.getString(4)%></td>
                                <td><%= account.getString(5)%></td>
                                <td><%= account.getString(7)%></td>
                                <td><%= account.getString(6)%></td>
                                <td><%= account.getString(8)%></td>
                                <td><%= account.getString(9)%></td>
                                <td>                                    
                                    <a href="#" class="settings" title="Đổi vai trò" data-toggle="tooltip" onclick="showUpdateRoleForm(
                                                    '<%= account.getString(1)%>',
                                                    '<%= account.getString(2)%>',
                                                    '<%= account.getString(9)%>'
                                                    )"><i class="material-icons">&#xE8B8;</i></a>

                                    <a href="#" onclick="confirmDelete('<%= account.getString(1)%>')" class="delete" title="Delete" data-toggle="tooltip">
                                        <i class="material-icons">&#xE5C9;</i>
                                    </a>                                </td>
                            </tr>
                            <% } %>
                        </tbody>
                    </table>
                </div>
            </div>        
        </div>        

    </body>

</html>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        const addButton = document.querySelector(".add-new-product-button");
        const productForm = document.getElementById("productForm");

        addButton.addEventListener("click", function () {
            productForm.style.display = "block";
        });
    });
</script>
<script>
    function showUpdateRoleForm(accountID, fullname, currentRole) {
        const form = document.getElementById("updateRoleModal");
        const accountIdInput = document.getElementById("accountId");
        const currentFullname = document.getElementById("currentFullname");
        const currentRoleInput = document.getElementById("currentRole");
        const newRole = document.getElementById("newRole");

        accountIdInput.value = accountID;
        currentFullname.value = fullname;
        currentRoleInput.value = currentRole;
        const roleMap = {
            "Khách hàng": "1",
            "Nhân viên bán hàng": "2",
            "Quản lý": "3"
        };

        // Set the default value of newRole based on the currentRole string
        if (roleMap.hasOwnProperty(currentRole)) {
            newRole.value = roleMap[currentRole];
        }
        $(form).modal("show");
    }
    function confirmDelete(accountId) {
        if (confirm("Xác nhận xóa tài khoản này?")) {
            window.location.href = "dashboard?go=delete&id=" + accountId;
        }
    }
</script>

