<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "Entity.*" %>
<%@page import = "Model.*" %>
<%@page import = "java.util.*" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>BookSaw - Free Book Store HTML CSS Template</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta name="description" content="">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">

        <link rel="stylesheet" type="text/css" href="css/normalize.css">
        <link rel="stylesheet" type="text/css" href="icomoon/icomoon.css">
        <link rel="stylesheet" type="text/css" href="css/vendor.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <style>
            .product-item {
                width: 300px;
                height: 400px;
            }
        </style>
    </head>

    <body data-bs-spy="scroll" data-bs-target="#header" tabindex="0">

        <%@include file="Header.jsp" %>

        <section id="billboard">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <button class="prev slick-arrow">
                            <i class="icon icon-arrow-left"></i>
                        </button>

                        <div class="main-slider pattern-overlay">
                            <div class="slider-item">
                                <div class="banner-content">
                                    <h2 class="banner-title">Life of the Wild</h2>
                                    <p>"Life of the Wild" là một cuốn sách phiêu lưu và hấp dẫn nói về cuộc hành trình hoang dã của Christopher McCandless, một thanh niên trẻ người Mỹ. McCandless bỏ lại cuộc sống xa hoa và sự ổn định của xã hội, quyết định bắt đầu một cuộc hành trình tự do đến vùng hoang dã của Alaska. Trong suốt hành trình, anh sống bằng cách săn bắn, thu thập thực phẩm từ thiên nhiên và sống trong một chiếc xe van cũ.</p>
                                    <div class="btn-wrap">
                                        <a href="detail?productID=459" class="btn btn-outline-accent btn-accent-arrow">Read More<i
                                                class="icon icon-ns-arrow-right"></i></a>
                                    </div>
                                </div><!--banner-content-->
                                <img src="images/main-banner1.jpg" alt="banner" class="banner-image">
                            </div><!--slider-item-->

                            <div class="slider-item">
                                <div class="banner-content">
                                    <h2 class="banner-title">Birds gonna be Happy</h2>
                                    <p>Cuốn sách "Birds gonna be Happy" là một tác phẩm đầy cảm hứng về cuộc hành trình của một người nghiên cứu thiên nhiên và yêu thiên nhiên sâu sắc. Tác giả, một nhà điều tra thiên nhiên tên là Anna, dành suốt đời mình để nghiên cứu và theo đuổi đam mê với loài chim.

                                    </p>
                                    <div class="btn-wrap">
                                        <a href="detail?productID=460" class="btn btn-outline-accent btn-accent-arrow">Read More<i
                                                class="icon icon-ns-arrow-right"></i></a>
                                    </div>
                                </div><!--banner-content-->
                                <img src="images/main-banner2.jpg" alt="banner" class="banner-image">
                            </div><!--slider-item-->

                        </div><!--slider-->

                        <button class="next slick-arrow">
                            <i class="icon icon-arrow-right"></i>
                        </button>

                    </div>
                </div>
            </div>

        </section>

        <section id="client-holder" data-aos="fade-up">
            <div class="container">
                <div class="row">
                    <div class="inner-content">
                        <div class="logo-wrap">
                            <div class="grid">
                                <a href="#"><img src="images/client-image1.png" alt="client"></a>
                                <a href="#"><img src="images/client-image2.png" alt="client"></a>
                                <a href="#"><img src="images/client-image3.png" alt="client"></a>
                                <a href="#"><img src="images/client-image4.png" alt="client"></a>
                                <a href="#"><img src="images/client-image5.png" alt="client"></a>
                            </div>
                        </div><!--image-holder-->
                    </div>
                </div>
            </div>
        </section>

        <section id="featured-books" class="py-5 my-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="section-header align-center">
                            <div class="title">
                                <span>Some quality items</span>
                            </div>
                            <h2 class="section-title" style="color: red">Sách nổi bật</h2>
                        </div>

                        <div class="product-list" data-aos="fade-up">
                            <div class="row">
                                <c:forEach items="${listFeature}" var="c">
                                    <div class="col-md-3">
                                        <div class="product-item" style="margin-bottom: 200px;">
                                            <figure class="product-style">
                                                <a href="detail?productID=${c.productID}"><img src="${c.image_url}" alt="Books" class="product-item"></a>
                                                    <c:if test="${sessionScope.role != null}">
                                                    <button type="button" class="add-to-cart" data-product-tile="add-to-cart">
                                                        <a href="cart?id=${c.productID}">Thêm vào giỏ hàng</a></button>
                                                    </c:if>
                                                    <c:if test="${sessionScope.role == null}">
                                                    <button type="button" class="add-to-cart" data-product-tile="add-to-cart">
                                                        <a href="Login.jsp">Thêm vào giỏ hàng</a></button>
                                                    </c:if>
                                            </figure>
                                            <figcaption>
                                                <h3><a href="detail?productID=${c.productID}">${c.proName}</a></h3>
                                                <span>${c.authorName}</span>
                                                <div class="item-price">${c.price} VNĐ  </div>
                                            </figcaption>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div><!--ft-books-slider-->
                        </div>


                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                    </div>
                </div>
            </div>
        </section>

        <section id="best-selling" class="leaf-pattern-overlay" style="width: 100%; height: 50%">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="row" style="height: 400px">
                            <c:forEach items="${bestSeller}" var="c">
                                <div class="col-md-6">
                                    <figure class="products-thumb" class="product-style">
                                        <figure class="product-style" style="height: 500%">
                                            <a href="detail?productID=${c.productID}"><img src="${c.image_url}" alt="Books" class="product-item">
                                            </a>

                                            <c:if test="${sessionScope.role != null}">
                                                <button type="button" class="add-to-cart" data-product-tile="add-to-cart">
                                                    <a href="cart?id=${c.productID}">Thêm vào giỏ hàng</a></button>
                                                </c:if>
                                                <c:if test="${sessionScope.role == null}">
                                                <button type="button" class="add-to-cart" data-product-tile="add-to-cart">
                                                    <a href="Login.jsp">Thêm vào giỏ hàng</a></button>
                                                </c:if>
                                        </figure>
                                    </figure>
                                </div>

                                <div class="col-md-6">
                                    <div class="product-entry">
                                        <h6 class="section-title divider" style="color: red">Sách bán chạy</h6>
                                        <div class="products-content">
                                            <div class="col-md-3">
                                                <div class="product-item" style="margin-bottom: 200px;">
                                                    <figure class="product-style">
                                                    </figure>
                                                    <figcaption>
                                                        <h3><a href="detail?productID=${c.productID}">${c.proName}</a></h3>
                                                        <span>${c.authorName}</span>
                                                        <div class="item-price">${c.price} VNĐ  </div>
                                                    </figcaption>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </div>
                        <!-- / row -->
                    </div>
                </div>
            </div>
        </section>

        <!--        <section id="popular-books" class="bookshelf py-5 my-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="section-header align-center">
                                    <h4 class="section-title">Thể loại:</h4>
                                </div>
        
                                <ul class="tabs">
        <c:forEach items="${listCate}" var="o">
            <li data-tab-target="#all-genre" class="active tab">
                <a href="home?go=displayAll&cateID=${o.cateID}">${o.cateName}</a>
            </li>
        </c:forEach>
    </ul>
        <c:choose>
            <c:when test="${not empty proListByCate}">
                <div class="row">
                <c:forEach items="${proListByCate}" var="c">
                    <div class="col-md-3">
                        <div class="product-item" style="margin-bottom: 200px;">
                            <figure class="product-style">
                                <a href="detail?productID=${c.productID}">
                                    <img src="${c.image_url}" alt="Books" class="product-item">
                                </a>
                                <button type="button" class="add-to-cart" data-product-tile="add-to-cart">
                                    <a href="cart?id=${c.productID}">Add to Cart</a></button>
                            </figure>
                            <figcaption>
                                <h3><a href="detail?productID=${c.productID}">${c.proName}</a></h3>
                                <span>${c.authorName}</span>
                                <div class="item-price">${c.price} VNĐ</div>
                            </figcaption>
                        </div>
                    </div>
                </c:forEach>
            </div>
            </c:when>
            <c:otherwise>
                <div class="tab-content">
                    <div id="all-genre" data-tab-content class="active">
                        <div class="row">

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item1.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Portrait photography</h3>
                                        <span>Adam Silber</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item2.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Once upon a time</h3>
                                        <span>Klien Marry</span>
                                        <div class="item-price">$ 35.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item3.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Tips of simple lifestyle</h3>
                                        <span>Bratt Smith</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item4.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Just felt from outside</h3>
                                        <span>Nicole Wilson</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item5.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Peaceful Enlightment</h3>
                                        <span>Marmik Lama</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item6.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Great travel at desert</h3>
                                        <span>Sanchit Howdy</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item7.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Life among the pirates</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item8.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Simple way of piece life</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div id="business" data-tab-content>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item2.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Peaceful Enlightment</h3>
                                        <span>Marmik Lama</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item4.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Great travel at desert</h3>
                                        <span>Sanchit Howdy</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item6.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Life among the pirates</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item8.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Simple way of piece life</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div id="technology" data-tab-content>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item1.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Peaceful Enlightment</h3>
                                        <span>Marmik Lama</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item3.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Great travel at desert</h3>
                                        <span>Sanchit Howdy</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item5.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Life among the pirates</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item7.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Simple way of piece life</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="romantic" data-tab-content>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item1.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Peaceful Enlightment</h3>
                                        <span>Marmik Lama</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item3.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Great travel at desert</h3>
                                        <span>Sanchit Howdy</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item5.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Life among the pirates</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item7.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Simple way of piece life</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="adventure" data-tab-content>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item5.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Life among the pirates</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item7.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Simple way of piece life</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="fictional" data-tab-content>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item5.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Life among the pirates</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="product-item">
                                    <figure class="product-style">
                                        <img src="images/tab-item7.jpg" alt="Books" class="product-item">
                                        <button type="button" class="add-to-cart" data-product-tile="add-to-cart">Add to
                                            Cart</button>
                                    </figure>
                                    <figcaption>
                                        <h3>Simple way of piece life</h3>
                                        <span>Armor Ramsey</span>
                                        <div class="item-price">$ 40.00</div>
                                    </figcaption>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </c:otherwise>
        </c:choose>


    </div>inner-tabs

</div>
</div>
</section>-->




        <section id="latest-blog" class="py-5 my-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="section-header align-center">
                            <div class="title">
                                <span>Read our articles</span>
                            </div>
                            <br/>
                            <h4 class="section-title">Tin tức</h4>
                        </div>

                        <div class="row">

                            <div class="col-md-4">

                                <article class="column" data-aos="fade-up">

                                    <figure>
                                        <a href="#" class="image-hvr-effect">
                                            <img src="images/post-img1.jpg" alt="post" class="post-image">
                                        </a>
                                    </figure>

                                    <div class="post-item">
                                        <div class="meta-date">11 tháng 4, 2023</div>
                                        <h3><a href="https://giaoduc.net.vn/doc-sach-giup-cho-cuoc-song-cua-chung-ta-hanh-phuc-hon-post193692.gd">Đọc sách giúp cho cuộc sống của chúng ta hạnh phúc hơn</a></h3>

                                        <div class="links-element">
                                            <div class="categories">inspiration</div>
                                            <div class="social-links">
                                                <ul>
                                                    <li>
                                                        <a href="#"><i class="icon icon-facebook"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="icon icon-twitter"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="icon icon-behance-square"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div><!--links-element-->

                                    </div>
                                </article>

                            </div>
                            <div class="col-md-4">

                                <article class="column" data-aos="fade-up" data-aos-delay="200">
                                    <figure>
                                        <a href="#" class="image-hvr-effect">
                                            <img src="images/post-img2.jpg" alt="post" class="post-image">
                                        </a>
                                    </figure>
                                    <div class="post-item">
                                        <div class="meta-date">11 tháng 4, 2023</div>
                                        <h3><a href="https://vnexpress.net/nhung-loi-ich-it-nguoi-biet-cua-doc-sach-4560922.html">Những lợi ích của đọc sách</a></h3>

                                        <div class="links-element">
                                            <div class="categories">inspiration</div>
                                            <div class="social-links">
                                                <ul>
                                                    <li>
                                                        <a href="#"><i class="icon icon-facebook"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="icon icon-twitter"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="icon icon-behance-square"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div><!--links-element-->

                                    </div>
                                </article>

                            </div>
                            <div class="col-md-4">

                                <article class="column" data-aos="fade-up" data-aos-delay="400">
                                    <figure>
                                        <a href="#" class="image-hvr-effect">
                                            <img src="images/post-img3.jpg" alt="post" class="post-image">
                                        </a>
                                    </figure>
                                    <div class="post-item">
                                        <div class="meta-date">11 tháng 4, 2023</div>
                                        <h3><a href="https://www.facebook.com/photo/?fbid=122108229962092687&set=a.122108230802092687">
                                                Chuẩn bị ra mắt!</a></h3>

                                        <div class="links-element">
                                            <div class="categories">inspiration</div>
                                            <div class="social-links">
                                                <ul>
                                                    <li>
                                                        <a href="#"><i class="icon icon-facebook"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="icon icon-twitter"></i></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="icon icon-behance-square"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div><!--links-element-->

                                    </div>
                                </article>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <footer id="footer">
            <div class="container">
                <div class="row">

                    <div class="col-md-4">

                        <div class="footer-item">
                            <div class="company-brand">
                                <img src="images/main-logo.png" alt="logo" class="footer-logo">
                                <p>Đây là một trang web bán sách với mục tiêu và mong muốn người dùng có được trải nghiệm tốt nhất..</p>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-2">

                        <div class="footer-menu">
                            <h5>Khám phá</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="home">Trang chủ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="Allproduct">Sách</a>
                                </li>
                                <li class="menu-item">
                                    <a href="Allproduct"> Thể loại</a>
                                </li>
                                <li class="menu-item">
                                    <a href="#"> Tìm kiếm</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-md-2">
                        <% if (role == null) { %>
                        <div class="footer-menu">
                            <h5>Tài khoản</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="Login.jsp">Đăng nhập </a>
                                </li>
                                <li class="menu-item">
                                    <a href="Register.jsp">Đăng ký</a>
                                </li>
                            </ul>
                        </div>
                        <% } else { %> 
                        <div class="footer-menu">
                            <h5>Tài khoản</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="navbar?go=profile">Hồ sơ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="navbar?go=logout" ">Đăng xuất</a>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                    </div>
                    <div class="col-md-2">

                        <div class="footer-menu">
                            <h5>Trợ giúp</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="https://www.facebook.com/profile.php?id=61552780627883">Hỗ trợ bằng page</a>
                                </li>
                                <li class="menu-item">
                                    <a href="https://www.facebook.com/profile.php?id=61552780627883">Liên lạc với chúng tôi</a>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>
                <!-- / row -->

            </div>
        </footer>

        <div id="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="copyright">
                            <div class="row">

                                <div class="col-md-6">
                                </div>

                                <div class="col-md-6">
                                    <div class="social-links align-right">
                                        <ul>
                                            <li>
                                                <i class="icon icon-facebook"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-twitter"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-youtube-play"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-behance-square"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div><!--grid-->

                    </div><!--footer-bottom-content-->
                </div>
            </div>
        </div>

        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
        <script src="js/plugins.js"></script>
        <script src="js/script.js"></script>
        <script>
            document.querySelectorAll('.tabs a').forEach(tabLink => {
                tabLink.addEventListener('click', function (event) {
                    const scrollPosition = window.scrollY;
                    localStorage.setItem('scrollPosition', scrollPosition);
                });
            });

            window.addEventListener('load', () => {
                const savedScrollPosition = localStorage.getItem('scrollPosition');
                if (savedScrollPosition) {
                    window.scrollTo(0, savedScrollPosition);
                    localStorage.removeItem('scrollPosition');
                }
            });
        </script>
    </body>

</html>