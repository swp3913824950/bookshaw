<%-- 
    Document   : CustomerOrder
    Created on : Oct 13, 2023, 4:39:58 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "Model.*,Entity.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    </head>
    <body>
        <%
 String message = (String) request.getAttribute("message");
        %>

        <script>
            function showMessage() {
                alert('<%=message%>');
            }
        </script>
        <% if (message != null && !message.trim().equals("")) { %>
        <script>showMessage();</script> 
        <% } %>
        <%
        DAOFeedback dao = new DAOFeedback();
        DAOCustomer daoA = new DAOCustomer();

        Account a = (Account) session.getAttribute("account");
        int customerID = daoA.findCustomer(a.getAccountID()).getCustomerID();
        %>
        <%@include file="Header.jsp" %>
        <div  style="border-bottom: 1px solid white;">
            <form action="order" method="get">
                <table class="table table-success table-striped">
                    <thead>
                        <tr>
                            <th>ID đơn hàng</th>
                            <th>ID khách hàng</th>
                            <th>ID nhân viên</th>
                            <th>Địa chỉ</th>
                            <th>Ngày đặt hàng</th>
                            <th>Ngày giao hàng</th>
                            <th>Trạng thái</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="o" items="${olist}">
                            <tr>
                                <td>${o.orderID}</td>
                                <td>${o.customerID}</td>
                                <td>${o.sellerID}</td>
                                <td>${o.shipAddress}</td>
                                <td>${o.orderDate}</td>
                                <td>${o.shippedDate}</td>
                                <td>${o.status}</td>
                                <td><a href="home?go=orderDetail&orderID=${o.orderID}&status=${o.status}">Thông tin đơn hàng</a>
                                    <c:if test="${o.status eq 'Shipping'}">
                                        <a href="acceptOrder?orderid=${o.orderID}&status=accept">| Đã nhận được hàng</a>
                                        <a href="acceptOrder?orderid=${o.orderID}&status=reject">| Hoàn trả hàng</a>
                                    </c:if>
                                    <c:if test="${o.status eq 'Processing'}">
                                        <a href="acceptOrder?orderid=${o.orderID}&status=reject">| Hủy đơn hàng</a>
                                    </c:if>

                                    <c:if test="${o.status eq 'Received' || o.status eq 'Rejected'}">
                                        <c:set var="orderid" value="${o.orderID}" scope="page"/>
                                        <c:set var="customerid" value="<%= customerID %>" scope="page"/>
                                        <% 
                                        boolean hasFeedback = dao.hasUserFeedbackOnOrder(
                                        (int) pageContext.getAttribute("orderid"),
                                        (int) pageContext.getAttribute("customerid")
                                        );
                                        %>
                                        <c:set var="hasFeedback" value="<%= hasFeedback %>" scope="page"/>
                                        <c:choose>

                                            <c:when test="${hasFeedback}">
                                                <p>Đã thêm phản hồi</p>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="#" 
                                                   class="add-feedback"
                                                   data-toggle="modal" 
                                                   data-target="#addFeedbackModal"
                                                   data-order-id="${o.orderID}">
                                                    | Thêm phản hồi
                                                </a>
                                            </c:otherwise>

                                        </c:choose>

                                    </c:if>

                                </td>
                            </tr>
                        </c:forEach>          
                    </tbody>
                </table>
            </form>


            <div class="modal" id="addFeedbackModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Thêm phản hồi</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal Body -->
                        <div class="modal-body">
                            <form action="addFeedback" method="POST">
                                <input type="hidden" name="orderID" id="orderID">

                                <div class="form-group">
                                    <label for="content">Phản hồi:</label>
                                    <textarea class="form-control" name="content"></textarea>
                                </div>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ðóng</button>
                                <button type="submit" class="btn btn-primary">Thêm phản hồi</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer id="footer">
            <div class="container">
                <div class="row">

                    <div class="col-md-4">

                        <div class="footer-item">
                            <div class="company-brand">
                                <img src="images/main-logo.png" alt="logo" class="footer-logo">
                                <p>Đây là một trang web bán sách với mục tiêu và mong muốn người dùng có được trải nghiệm tốt nhất..</p>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-2">

                        <div class="footer-menu">
                            <h5>Khám phá</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="home">Trang chủ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="Allproduct">Sách</a>
                                </li>
                                <li class="menu-item">
                                    <a href="Allproduct"> Thể loại</a>
                                </li>
                                <li class="menu-item">
                                    <a href="#"> Tìm kiếm</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-md-2">
                        <% if (role == null) { %>
                        <div class="footer-menu">
                            <h5>Tài khoản</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="Login.jsp">Đăng nhập </a>
                                </li>
                                <li class="menu-item">
                                    <a href="Register.jsp">Đăng ký</a>
                                </li>
                            </ul>
                        </div>
                        <% } else { %> 
                        <div class="footer-menu">
                            <h5>Tài khoản</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="navbar?go=profile">Hồ sơ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="navbar?go=logout" ">Đăng xuất</a>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                    </div>
                    <div class="col-md-2">

                        <div class="footer-menu">
                            <h5>Trợ giúp</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="https://www.facebook.com/profile.php?id=61552780627883">Hỗ trợ bằng page</a>
                                </li>
                                <li class="menu-item">
                                    <a href="https://www.facebook.com/profile.php?id=61552780627883">Liên lạc với chúng tôi</a>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>
                <!-- / row -->

            </div>
        </footer>

        <div id="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="copyright">
                            <div class="row">

                                <div class="col-md-6">
                                </div>

                                <div class="col-md-6">
                                    <div class="social-links align-right">
                                        <ul>
                                            <li>
                                                <i class="icon icon-facebook"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-twitter"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-youtube-play"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-behance-square"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div><!--grid-->

                    </div><!--footer-bottom-content-->
                </div>
            </div>
        </div>
    </body>

    <script>
        $(document).ready(function () {
            $('.add-feedback').click(function (e) {
                e.preventDefault(); // Prevent the default link behavior
                var orderID = $(this).data('order-id');
                $('#orderID').val(orderID);
                $('#addFeedbackModal').modal('show');
            });
        });

    </script>


</html>
