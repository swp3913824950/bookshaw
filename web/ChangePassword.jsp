<%-- 
    Document   : Profile
    Created on : Sep 23, 2023, 8:29:14 PM
    Author     : staytogether
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="style.css"/>
    </head>

    <body>
        <%@include file="Header.jsp" %>
        <style>
            .profileDes{
                margin-left: 30%
            }
            .success{
                margin: 15px 150px 10px 15px;
                font-size: 17px;
            }
        </style>
        <div style="margin-left: 40%;margin-top: 50px"></div>
        <c:set var="n" value="${sessionScope.account.username}"/>
        <h1 style="text-align: center">Đổi mật khẩu</h1>
        <form action="changepassword" method="post" style="text-align: center" class="profileDes">
            <div class="row">
                <div class="col-md-6">
                    <div class="profile-head">
                        <input type="hidden" name="username" value="${sessionScope.account.username}"/>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Mật khẩu cũ</label>
                            </div>
                            <div class="col-md-6">
                                <input type="password" name="oldpass" value="${sessionScope.account.password}" required=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Mật khẩu mới</label>
                            </div>
                            <div class="col-md-6">
                                <input type="password" name="newpass" required=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Xác nhận mật khẩu mới</label>
                            </div>
                            <div class="col-md-6">
                                <input type="password" name="cf" required=""/>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Change Password"/>
                                <p>${msg}</p>
                            </div>
                        </div> 


                    </div>
                </div>


            </div>

        </form>

    </body>
</html>
