<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Seller Management</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 13px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #299be4;
                color: #fff;
                padding: 16px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }
            .table-title .btn {
                color: #566787;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #f2f2f2;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 12px 15px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 22px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }

        </style>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>
    <body>
        <!-- Modal -->
        <%@include file="MenuSeller.jsp" %>

        <div class="modal fade" id="addProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form action="add" method="post" id="productForm" enctype="multipart/form-data" onsubmit="return validateDate() && validateInput();">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h3 class="modal-title" id="exampleModalLabel" style="margin-left: 30%;color: red">Thêm sản phẩm mới</h3>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <!-- Thêm form nhập liệu cho sản phẩm ở đây -->
                            <div class="form-group">
                                <label for="productName">Tên sản phẩm</label>
                                <input type="text" class="form-control" id="productName" name="productName" placeholder="tên sản phẩm" required>
                            </div>
                            <div class="form-group">
                                <label for="productAuthor">Tác giả</label>
                                <input type="text" class="form-control" id="productName" name="auName" placeholder="tác giả" required>
                            </div>
                            <div class="form-group">
                                <label for="maniDate">Ngày nhập</label>
                                <input type="date" class="form-control" id="productName" name="date" placeholder="ngày nhập" required>
                            </div>
                            <div class="form-group">
                                <label for="price">Giá</label>
                                <input type="number" class="form-control" id="productName" name="price" placeholder="giá" required>
                            </div>
                            <div class="form-group">
                                <label for="quantity">Số lượng</label>
                                <input type="number" class="form-control" id="productName" name="quantity" placeholder="số lượng" required>
                            </div>
                            <div class="form-group">
                                <label>Thể loại</label>
                                <select name="category" class="form-select" aria-label="Default select example">
                                    <c:forEach items="${listCate}" var="o">
                                        <option value="${o.cateID}">${o.cateName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="img">Ảnh</label>
                                <input type="file" class="form-control" name="image" placeholder="ảnh sản phẩm">
                            </div>




                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                            <button type="submit" class="btn btn-primary">Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div style="text-align: center;margin-top: 30px">
            <form method="post" action="searchProduct">
                <label>Tên sản phẩm: </label>
                <input type="text" name="keyword" >
                <input type="submit" value="Tìm kiếm">
            </form>
        </div>
        <div class="container">
            <div class="table-responsive">
                <div class="table-wrapper">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-xs-5">
                                <h2>Quản lý sản phẩm</h2>
                            </div>
                            <div id="productForm" style="display: none;">
                            </div>
                            <div class="col-xs-7">
                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addProductModal"><i class="material-icons">&#xE147;</i> <span>Thêm sản phẩm mới</span></a>
                            </div>


                        </div>
                    </div>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tên sản phẩm</th>						
                                <th>Ảnh</th>
                                <th>Tác giả</th>
                                <th>Giá</th>
                                <th>Số lượng</th>
                                <th>Ngày nhập</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${listP}" var="o">
                                <tr>
                                    <td>${o.productID}</td>
                                    <td>
                                        <a href="#">
                                            ${o.proName}
                                        </a>
                                    </td>
                                    <td><img src="${o.image_url}" style="width: 150px">                                    </td>                        

                                    <td>${o.authorName}</td>
                                    <td>${o.price} VNĐ</td>
                                    <td>${o.quantity} </td>
                                    <td>${o.maniDate} </td>
                                    <td>
                                        <a href="updateProduct?id=${o.productID}" class="settings" title="Chỉnh sửa" data-toggle="tooltip"><i class="material-icons">&#xE8B8;</i></a>
                                        <a href="#" onclick="doDelete('${o.productID}')" class="delete" title="Xóa" data-toggle="tooltip"><i class="material-icons">&#xE5C9;</i></a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <script src="js/paging.js"></script>
                </div>
            </div>        
        </div>        
    </body>

</html>
<script>
                                            document.addEventListener("DOMContentLoaded", function () {
                                                const addButton = document.querySelector(".add-new-product-button");
                                                const productForm = document.getElementById("productForm");
                                                addButton.addEventListener("click", function () {
                                                    productForm.style.display = "block";
                                                });
                                            });
                                            let inputImg = document.getElementById('img');
                                            let imagePreview = document.getElementById('image-preview');
                                            inputImg.onchange = function () {
                                                if (inputImg.files.length > 0) {
                                                    // Display the selected image in the 'image-preview' element
                                                    imagePreview.src = URL.createObjectURL(inputImg.files[0]);
                                                } else {
                                                    // Clear the 'image-preview' if no image is selected
                                                    imagePreview.src = "";
                                                }
                                            }

                                            function doDelete(ID) {
                                                if (confirm("Bạn có chắc chắn xóa sản phẩm " + ID + " không?")) {
                                                    window.location.href = "delete?id=" + ID;
                                                }
                                            }

// Thêm hàm kiểm tra ngày
                                            function validateInput() {
                                                var quantity = parseInt(document.getElementsByName("quantity")[0].value);
                                                var price = parseInt(document.getElementsByName("price")[0].value);
                                                var productName = document.getElementsByName("productName")[0].value;
                                                var authorName = document.getElementsByName("auName")[0].value;
                                                var image = document.getElementsByName("image")[0].value;

                                                // Kiểm tra số lượng và giá
                                                if (quantity <= 0 || isNaN(quantity)) {
                                                    alert("Số lượng phải lớn hơn 0 và là một số nguyên");
                                                    return false;
                                                }

                                                if (price < 1000 || isNaN(price)) {
                                                    alert("Giá phải lớn hơn hoặc bằng 1000 và là một số nguyên");
                                                    return false;
                                                }

                                                // Kiểm tra xem ảnh có được chọn không
                                                if (image === "") {
                                                    alert("Vui lòng chọn ảnh sản phẩm");
                                                    return false;
                                                }

                                                // Kiểm tra khoảng trắng trong tên sản phẩm và tác giả
                                                if (productName.trim() === "" || authorName.trim() === "") {
                                                    alert("Tên sản phẩm và tác giả không được để trống hoặc chỉ chứa khoảng trắng");
                                                    return false;
                                                }

                                                return true;
                                            }

                                            function validateDate() {
                                                var currentDate = new Date();
                                                var inputDate = new Date(document.getElementsByName("date")[0].value);
                                                // Kiểm tra xem ngày nhập liệu có phải là ngày hiện tại hoặc không quá 2 năm so với ngày hiện tại không
                                                if (inputDate > currentDate || currentDate.getFullYear() - inputDate.getFullYear() > 2) {
                                                    alert("Ngày nhập phải là ngày hiện tại hoặc không quá 2 năm so với ngày hiện tại");
                                                    return false;
                                                }

                                                return true;
                                            }

</script>
