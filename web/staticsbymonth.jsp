<%-- 
    Document   : staticsbymonth
    Created on : Jul 15, 2023, 5:25:43 PM
    Author     : Admin
--%>
<%-- 
    Document   : dashboard
    Created on : Mar 14, 2022, 9:59:32 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Thống kê theo tháng</title>




        <!-- Bootstrap core CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <!-- Font Awesome Icon -->
        <script src="https://kit.fontawesome.com/3c84cb624f.js" crossorigin="anonymous"></script>

        <style>
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
            .bd-placeholder-img {
                font-size: 1.125rem;
                text-anchor: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
            }

            @media (min-width: 768px) {
                .bd-placeholder-img-lg {
                    font-size: 3.5rem;
                }
            }
            body {
                font-size: .875rem;
            }

            .feather {
                width: 16px;
                height: 16px;
                vertical-align: text-bottom;
            }

            /*
             * Sidebar
             */

            /*.sidebar {
              position: fixed;
              top: 0;
              bottom: 0;
              left: 0;
              z-index: 100;  Behind the navbar 
              padding: 48px 0 0;  Height of navbar 
              box-shadow: inset -1px 0 0 rgba(0, 0, 0, .1);
            }*/

            @media (max-width: 767.98px) {
                .sidebar {
                    top: 5rem;
                }
            }

            .sidebar-sticky {
                position: relative;
                top: 0;
                height: calc(100vh - 48px);
                padding-top: .5rem;
                overflow-x: hidden;
                overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
            }

            /*@supports ((position: -webkit-sticky) or (position: sticky)) {
              .sidebar-sticky {
                position: -webkit-sticky;
                position: sticky;
              }
            }*/

            .sidebar .nav-link {
                font-weight: 500;
                color: #333;
            }

            .sidebar .nav-link .feather {
                margin-right: 4px;
                color: #999;
            }

            .sidebar .nav-link.active {
                color: #007bff;
            }

            .sidebar .nav-link:hover .feather,
            .sidebar .nav-link.active .feather {
                color: inherit;
            }

            .sidebar-heading {
                font-size: .75rem;
                text-transform: uppercase;
            }

            /*
             * Navbar
             */

            .navbar-brand {
                padding-top: .75rem;
                padding-bottom: .75rem;
                font-size: 1rem;
                background-color: rgba(0, 0, 0, .25);
                box-shadow: inset -1px 0 0 rgba(0, 0, 0, .25);
            }

            .navbar .navbar-toggler {
                top: .25rem;
                right: 1rem;
            }

            .navbar .form-control {
                padding: .75rem 1rem;
                border-width: 0;
                border-radius: 0;
            }

            .form-control-dark {
                color: #fff;
                background-color: rgba(255, 255, 255, .1);
                border-color: rgba(255, 255, 255, .1);
            }

            .form-control-dark:focus {
                border-color: transparent;
                box-shadow: 0 0 0 3px rgba(255, 255, 255, .25);
            }


            .main-dashboard-row{
                display: flex;
            }
            .main-dashboard-row .infor-container{
                display: flex;
                align-items: center;
                height: 100px;
                background: #fff;
                margin: 10px;
                width: 50%;
                padding: 20px;
            }
            .main-dashboard-row .infor-container div:nth-child(2){
                margin-left: 10px;
                font-size: 2em;
                flex: 1;
                font-weight: 500;
            }
            .main-dashboard-row .infor-container div:last-child{
                font-size: 2em;
                margin-right: 20px;
            }
        </style>


        <!-- Custom styles for this template -->
        <link href="css/dashboard.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                    <div class="sidebar-sticky pt-3">
                        <ul class="nav flex-column">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                    <a class="nav-link " href="order">
                                        <span data-feather="home"></span>
                                        Hóa đơn 
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="listProduct">
                                        <span data-feather="home"></span>
                                        Danh sách sản phẩm
                                    </a>
                                </li>
                                 <li class="nav-item">
                                    <a class="nav-link" href="OrderFback">
                                        <span data-feather="home"></span>
                                        Phản hồi của khách hàng
                                    </a>
                                </li>
                            </ul>

                            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                                <span>Statistic</span>
                                <a class="d-flex align-items-center text-muted" href="#" aria-label="Add a new report">
                                    <span data-feather="plus-circle"></span>
                                </a>
                            </h6>
                            <ul class="nav flex-column mb-2">
                                <li class="nav-item">
                                    <a class="nav-link active" href="MonthStatics">
                                        <span data-feather="file-text"></span>
                                        Thống kê theo tháng trong năm <span class="sr-only">(current)</span>
                                    </a>
                                </li>

                            </ul>
                    </div>
                </nav>

                <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">                 
                    <input type="hidden" value="${requestScope.t1}" id="t1"/>
                    <input type="hidden" value="${requestScope.t2}" id="t2"/>
                    <input type="hidden" value="${requestScope.t3}" id="t3"/>
                    <input type="hidden" value="${requestScope.t4}" id="t4"/>
                    <input type="hidden" value="${requestScope.t5}" id="t5"/>
                    <input type="hidden" value="${requestScope.t6}" id="t6"/>
                    <input type="hidden" value="${requestScope.t7}" id="t7"/>
                    <input type="hidden" value="${requestScope.t8}" id="t8"/>
                    <input type="hidden" value="${requestScope.t9}" id="t9"/>
                    <input type="hidden" value="${requestScope.t10}" id="t10"/>
                    <input type="hidden" value="${requestScope.t11}" id="t11"/>
                    <input type="hidden" value="${requestScope.t12}" id="t12"/>

                    <canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>

                </main>
            </div>
        </div>


        <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery.slim.min.js"><\/script>')</script> <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>


        <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
        <script >
            //var arr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var txt = "Tháng 1;Tháng 2;Tháng 3;Tháng 4;Tháng 5;Tháng 6;Tháng 7;Tháng 8;Tháng 9;Tháng 10;Tháng 11;Tháng 12";
            var arr = txt.split(";");
            var ctx = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: arr,
                    datasets: [{
                            label: 'sale',
                            data: [document.getElementById('t1').value,
                                document.getElementById('t2').value,
                                document.getElementById('t3').value,
                                document.getElementById('t4').value,
                                document.getElementById('t5').value,
                                document.getElementById('t6').value,
                                document.getElementById('t7').value,
                                document.getElementById('t8').value,
                                document.getElementById('t9').value,
                                document.getElementById('t10').value,
                                document.getElementById('t11').value,
                                document.getElementById('t12').value],
                            backgroundColor: [
                                'rgba(216, 27, 96, 0.6)',
                                'rgba(3, 169, 244, 0.6)',
                                'rgba(255, 152, 0, 0.6)',
                                'rgba(29, 233, 182, 0.6)',
                                'rgba(156, 39, 176, 0.6)',
                                'rgba(84, 110, 122, 0.6)',
                                'red',
                                'green',
                                'brown',
                                'yellow',
                                'lightblue',
                                'lightgreen'
                            ],
                            borderColor: [
                                'rgba(216, 27, 96, 1)',
                                'rgba(3, 169, 244, 1)',
                                'rgba(255, 152, 0, 1)',
                                'rgba(29, 233, 182, 1)',
                                'rgba(156, 39, 176, 1)',
                                'rgba(84, 110, 122, 1)',
                                'red',
                                'green',
                                'brown',
                                'yellow',
                                'lightblue',
                                'lightgreen'
                            ],
                            borderWidth: 1
                        }]
                },
                options: {
                    legend: {
                        display: false
                    },
                    title: {
                        display: true,
                        text: 'Stats by month',
                        position: 'top',
                        fontSize: 16,
                        padding: 20
                    },
                    scales: {
                        yAxes: [{
                                ticks: {
                                    min: 75
                                }
                            }]
                    }
                }
            });
        </script>
    </body>
</html>


