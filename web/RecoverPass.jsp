<%-- 
    Document   : RecoverPass
    Created on : Jun 25, 2023, 4:44:17 PM
    Author     : Admin
--%>
<%-- 
    Document   : forgotpass
    Created on : Jun 25, 2023, 4:19:23 PM
    Author     : Admin
--%>

<%-- 
    Document   : Login2
    Created on : Jun 25, 2023, 3:51:10 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login Form</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="fonts/linearicons/style.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="wrapper">
            <div class="inner">
                <img src="images/image-1.png" alt="" class="image-1">
                <% String check = (String) request.getAttribute("check");
                if(check != null){
                %>
                <div class="alert alert-danger" role="alert">
                    ${check}
                </div>
                <%}%>
                <form action="checkOTP" method="post" id="loginForm">
                    <h3>Đổi mật khẩu</h3>
                    <div class="form-holder">
                        <span class="lnr lnr-user"></span>
                        <input type="text" name="number" id="username" class="form-control" placeholder="Mã của bạn" required>
                    </div>
                    <br>
                    <div class="form-group">
                        <input type="hidden" name="num" value="${sessionScope.number}">
                        <input type="hidden" name="user" value="${sessionScope.user}">
                        <div style="display: flex ;justify-content: space-between" >
                            <a href="forgotPass.jsp" >
                                <input type="button" value="Quay lại" class="btn float-right login_btn">
                            </a>
                            <input type="submit" value="Tiếp" class="btn float-right login_btn">
                        </div>
                    </div>
                </form>
                <img src="images/image-2.png" alt="" class="image-2">
            </div>
        </div>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
