<%-- 
    Document   : BuyNow
    Created on : Nov 1, 2023, 10:21:55 PM
    Author     : LuuTu
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import = "Entity.*" %>
<%@page import = "Model.*" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mua ngay</title>
    </head>
    <body>
        <%@ include file="Header.jsp" %>

        <h1>Thanh toán</h1>
        <%  
       Account checkoutAccount = (Account) session.getAttribute("account");
    DecimalFormat dcf = new DecimalFormat("#");
    request.setAttribute("dcf", dcf);
        %>
        <div>
            <h2>Thông tin sản phẩm:</h2>
            <table border="1" id="table">
                <tr>
                    <td>Tên sản phẩm</td>
                    <td style="width: 10%;">Ảnh</td>
                    <td>Giá gốc</td>
                    <td>Số lượng</td>
                    <td>Giảm giá</td>
                    <td>Tổng số tiền</td>
                </tr>
                <tr>
                    <td>${productInfor.proName}</td>
                    <td style="width: 10%;"><img src="${productInfor.image_url}" width="300" height="400"/></td>
                    <td>${dcf.format(productInfor.price)}</td>
                    <td>${quantity}</td>
                    <td>${productInfor.discount}</td>
                    <td>${dcf.format(productInfor.price * quantity * (1 - productInfor.discount))}</td>
                </tr>
            </table>
        </div>
        <div style="display: flex; justify-content: center;">
            <div style="margin-left: 50%">
                <form action="buynow" method="get">
                    <label for="sellerID">Nhân viên bán hàng:</label>
                    <select name="sellerID">
                        <c:forEach var="seller" items="${sellerList}">
                            <option value="${seller.sellerID}">${seller.fullname}</option>
                        </c:forEach>
                    </select>
                    <label for="name">Tên người nhận:</label>
                    <input type="text" name="name" id="name" value="<%=checkoutAccount.getFullname()%>" readonly>
                    <label for="phone">Số điện thoại:</label>
                    <input type="text" name="phone" id="phone" value="<%=checkoutAccount.getPhone()%>" readonly>
                    <label for="address">Địa chỉ:</label>
                    <input type="text" name="address" id="address" value="<%=checkoutAccount.getAddress()%>" required>
                    <input type="submit" value="Thanh toán">
                    <input type="hidden" name="productID" value="${productInfor.productID}">
                    <input type="hidden" name="quantity" value="${quantity}">
                    <input type="hidden" name="price" value="${productInfor.price}">
                </form>
            </div>
        </div>
    </body>
</html>

