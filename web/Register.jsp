<%-- 
    Document   : Register
    Created on : Sep 19, 2023, 8:56:07 PM
    Author     : LuuTu
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html >
    <head>
        <meta charset="utf-8">
        <title>Registration Form</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- LINEARICONS -->

        <!-- STYLE CSS -->
        <link rel="stylesheet" href="css/style.css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    </head>

    <body>


        <div class="wrapper">
            <div class="inner">
                <img src="images/image-1.png" alt="" class="image-1">
                <form id="registerForm" action="${pageContext.request.contextPath}/RegisterController" method="post">
                <h3>Tài khoản mới?</h3>
                <div class="form-holder">
                    <span class="lnr lnr-user"></span>
                    <input type="text" name="fullname" class="form-control" placeholder="Họ và tên" required
                           value="${requestScope.fullname}">
                </div>
                <div class="form-holder">
                    <span class="lnr lnr-user"></span>
                    <input type="text" class="form-control" placeholder="Tên đăng nhập" name="username" required
                           value="${requestScope.username}">
                </div>
                <div class="form-holder">
                    <span class="lnr lnr-lock"></span>
                    <input type="password" name="password" class="form-control" placeholder="Mật khẩu" required
                           value="${requestScope.password}">
                </div>
                <div class="form-holder">
                    <span class="lnr lnr-lock"></span>
                    <input type="password" name="re-enterPassword" class="form-control" placeholder="Nhập lại mật khẩu" required
                           value="${requestScope.reEnterPassword}">
                </div>

                <div class="form-holder">
                    <span class="lnr lnr-envelope"></span>
                    <input type="email" name="email" class="form-control" placeholder="Email" required
                           value="${requestScope.email}">
                </div>
                <div class="form-holder">
                    <span class="lnr lnr-envelope"></span>
                    <input type="date" name="dob" class="form-control" placeholder="Mail" required
                           value="${requestScope.dob}">
                </div>
                <div class="form-holder">
                    <span class="lnr lnr-phone-handset"></span>
                    <input type="text" name="phone" class="form-control" placeholder="Số điện thoại"
                           value="${requestScope.phone}">
                </div>
                <div class="form-holder">
                    <span class="lnr lnr-user"></span>
                    <input type="text" class="form-control" placeholder="Địa chỉ" name="address" required
                           value="${requestScope.address}">
                </div>

                <div class="login-button">
                    <button type="submit" name="submit">Đăng ký</button>
                </div>

                <div class="error" style="color:red">${error}</div>
                <div class="success" style="color:green">${success}</div>
                <p>Đã đăng ký<a href="${pageContext.request.contextPath}/navbar?go=login">Đăng nhập</a></p>
                </form>

            </div>
        </div>



        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>



