<%-- 
    Document   : order
    Created on : Oct 3, 2023, 9:53:35 PM
    Author     : staytogether
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!doctype html>
<html lang="en">
    <head>
        <title>Manage Order</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="css/style2.css">
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
        <style>
            .thead-primary thead th{
                font-weight: bold;
                
            }
    </style>
        <%@include file="MenuSeller.jsp" %>
    </head>
    
    <body>
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 text-center mb-4">
                        <h1 class="heading-section">Quản lí hóa đơn</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <% String viewStatus = request.getParameter("viewstatus");
if (viewStatus == null) viewStatus = "All";
                        %>

                        <form action="order" method="get" style="text-align: center">
                            <select name="viewstatus">
                                <option value="All" <%= viewStatus.equals("All") ? "selected" : "" %>>All</option>
                                <option value="Received" <%= viewStatus.equals("Received") ? "selected" : "" %>>Received</option>
                                <option value="Accepted" <%= viewStatus.equals("Accepted") ? "selected" : "" %>>Accepted</option>
                                <option value="Processing" <%= viewStatus.equals("Processing") ? "selected" : "" %>>Processing</option>
                                <option value="Shipping" <%= viewStatus.equals("Shipping") ? "selected" : "" %>>Shipping</option>
                                <option value="Rejected" <%= viewStatus.equals("Rejected") ? "selected" : "" %>>Rejected</option>
                            </select>
                            <input type="submit" value="Lọc">
                        </form>
                        <br/>
                        <div class="table-wrap">
                            <form action="order" method="get">

                                <table class="table">
                                    <thead class="thead-primary text-center">
                                        <tr>
                                            <th>OrderID</th>
                                            <th>Địa chỉ giao hàng</th>
                                            <th>Ngày đặt hàng</th>
                                            <th>Ngày yêu cầu giao hàng</th>
                                            <th>Ngày giao hàng</th>
                                            <th>Trạng thái</th>
                                            <th>Hành động</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="o" items="${olist}">
                                            <tr class="alert" role="alert">
                                                <td>
                                                    ${o.orderID}
                                                </td>
                                                <td>${o.shipAddress}</td>
                                                <td>${o.orderDate}</td>
                                                <td>${o.requiredDate}</td>
                                                <td>${o.shippedDate}</td>
                                                <td>${o.status}</td>
                                                <td>
                                                    <a href="#" onclick="deleteOrder(${o.orderID})"><i class="fa fa-trash"></i></a>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <a href="orderdetail?id=${o.orderID}"><i class="fa fa-eye"></i></a>
                                                    &nbsp;&nbsp;&nbsp;
                                                    <c:if test="${(o.status ne 'Received') && (o.status ne 'Rejected')}">
                                                        <a href="getorder?orderid=${o.orderID}" class="settings" title="Chỉnh sửa" data-toggle="tooltip"><i class="fa fa-edit"></i></a>
                                                    </c:if> 
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script>
            function deleteOrder(orderID) {
                if (confirm("Do you want to delete " + orderID + "?")) {
                    window.location.href = "deleteorder?orderID=" + orderID;
                }
            }
            document.addEventListener("DOMContentLoaded", function () {
                const addButton = document.querySelector(".add-new-product-button");
                const productForm = document.getElementById("orderForm");

                addButton.addEventListener("click", function () {
                    productForm.style.display = "block";
                });
            });
        </script>
        <script src="js/jquery.min.js"></script>
        <script src="js/popper.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>

