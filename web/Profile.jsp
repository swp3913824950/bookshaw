<%-- 
    Document   : Profile
    Created on : Sep 23, 2023, 8:29:14 PM
    Author     : staytogether
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Hồ sơ</title>
        <link rel="stylesheet" href="style.css"/>
    </head>

    <body>
        <%
String message = (String) request.getAttribute("message"); 
        %>

        <script>
            function showMessage() {
                let alertMessage = "<%=message%>";
                alert(alertMessage);
            }
        </script>
        <% if (message != null && !message.trim().equals("")) { %>
        <script>showMessage();</script>  
        <% } %>
        <%@include file="Header.jsp" %>
        <style>
            .profileDes{
                margin-left: 30%
            }
            .success{
                margin: 15px 150px 10px 15px;
                font-size: 17px;
            }
        </style>
        <div style="margin-left: 20%;margin-top: 50px"></div>
        <c:set var="n" value="${sessionScope.account.username}"/>
        <h2 style="text-align: center;color: red"> Thông tin của bạn:</h2>
        <form action="profile" method="post" style="text-align: center" class="profileDes">
            <div class="row">
                <div class="col-md-6">
                    <div class="profile-head">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Họ và tên</label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="fullname" value="${sessionScope.account.fullname}" required=""/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Mật khẩu</label>
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-3"></div>
                                <input type="password" name="pass" value="${sessionScope.account.password}" readonly=""/>
                                <br/>
                                <a style="color: blue; font-size: 15px"  href="changepassword">Đổi mật khẩu? </a><br/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Email</label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="email" value="${sessionScope.account.email}"/>
                            </div>
                        </div>       
                        <div class="row">
                            <div class="col-md-6">
                                <label>Địa chỉ</label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="address" value="${sessionScope.account.address}"/>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-6">
                                <label>Số điện thoại</label>
                            </div>
                            <div class="col-md-6">
                                <input type="text" name="phone" value="${sessionScope.account.phone}"/>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-md-6">
                                <label>Ngày sinh</label>
                            </div>
                            <div class="col-md-6">
                                <input type="date" name="dob" value="${sessionScope.account.dob}"/>
                            </div>
                        </div> 

                    </div>

                </div>
                <div class="col-md-2" style="margin: 50px">
                    <input type="submit" class="profile-edit-btn" name="btnAddMore" value="Cập nhật"/>
                </div>
            </div>

            <div class="col-md-4" >
                <div class="success" style="color:green;"> ${success} </div>
            </div>
            <div class="col-md-4" >
                <div class="success" style="color:red;"> ${error} </div>
            </div>
        </form>

    </body>
</html>