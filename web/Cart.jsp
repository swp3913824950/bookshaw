<%-- 
    Document   : index
    Created on : Sep 20, 2023, 4:01:34 PM
    Author     : ASUS
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "Entity.*" %>
<%@page import = "Model.*" %>
<%@page import = "java.util.*" %>
<%@page import = "java.text.DecimalFormat"%>

<!DOCTYPE html>

<%
    DecimalFormat dcf = new DecimalFormat("#");
    request.setAttribute("dcf", dcf);
    DAOCartDetail dao = new DAOCartDetail();
    DAOProduct dao1 = new DAOProduct();
    String username = (String) session.getAttribute("username");
    int cart_id = Integer.parseInt(request.getSession().getAttribute("cartID").toString());
    List<CartDetail> cart_detail = dao.findCartDetailByID(cart_id);
    int overall_cost = dao.getTotalCost(cart_detail);
%>
<html lang="vi">
    <head>
        <title>Giỏ hàng</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            table{
                width: 100%;
                text-align: center;
            }
            table td{
                text-align: center;
            }
        </style>
    </head>
    <body >
        <%
  String prefix = "Không thành công";
  String message = (String) request.getAttribute("message"); 
        %>

        <script>
            function showMessage() {
                let alertMessage = "<%=prefix%>" + "\n" + "<%=message%>";
                alert(alertMessage);
            }
        </script>
        <% if (message != null && !message.trim().equals("")) { %>
        <script>showMessage();</script>  
        <% } %>
        <%@include file="Header.jsp" %>
        <div  style="border-bottom: 1px solid white;">
        <h2 style="margin-left: 20px">Tổng thanh toán: <%=overall_cost%> VNĐ</h2>
        <table border=1 id="table">
            <tr>
                <td>Tên sản phẩm</td>
                <td style="width: 10%;">Ảnh</td>
                <td>Giá gốc</td>
                <td>Số lượng</td>
                <td>Giảm giá</td>
                <td>Tổng số tiền</td>

            </tr>
            <%
                if (cart_detail != null){
                   for (CartDetail c : cart_detail){
                       int product_id = c.getProductID();
                       Product pro = dao1.getProductByID(product_id);
                       String product_name = pro.getProName();
                       String image_url = pro.getImage_url();
                       double price_per_unit = pro.getPrice();
                       double discount = pro.getDiscount();
                       int quantity = c.getQuantity();
            %>
            <tr>
                <td><%=product_name%></td>
                <td style="width: 10%;"><img src="<%=image_url%>" width="300" height="400"/></td>
                <td><%=dcf.format(price_per_unit)%></td>
                <td>
                    <button style="width: 10px; background-color: #f3f2ec;"><a href="incDec?action=decrease&id=<%=product_id%>" style="color: black">-</a></button>
                    <input type="text" readonly value="<%=quantity%>"> 
                    <button style="width: 10px; background-color: #f3f2ec;"><a href="incDec?action=increase&id=<%=product_id%>" style="color: black">+</a></button>
                </td>
                <td><%=discount%></td>
                <td><%=dcf.format(price_per_unit * c.getQuantity() * (1 - discount))%></td>
                <td><button style="background-color: #f3f2ec;"><a href="remove?id=<%=c.getProductID()%>" style="color: black">Xóa</a></button></td>
            </tr>
            <%
                   }
                }
            %>
        </table>
        <p>
        <form action="checkout" method="post">
            <input type="hidden" name="go" value="start">
            <input type="submit" value="Mua hàng">
        </form>

        <p>
            </div>
        <footer id="footer">
            <div class="container">
                <div class="row">

                    <div class="col-md-4">

                        <div class="footer-item">
                            <div class="company-brand">
                                <img src="images/main-logo.png" alt="logo" class="footer-logo">
                                <p>Đây là một trang web bán sách với mục tiêu và mong muốn người dùng có được trải nghiệm tốt nhất..</p>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-2">

                        <div class="footer-menu">
                            <h5>Khám phá</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="home">Trang chủ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="Allproduct">Sách</a>
                                </li>
                                <li class="menu-item">
                                    <a href="Allproduct"> Thể loại</a>
                                </li>
                                <li class="menu-item">
                                    <a href="#"> Tìm kiếm</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-md-2">
                        <% if (role == null) { %>
                        <div class="footer-menu">
                            <h5>Tài khoản</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="Login.jsp">Đăng nhập </a>
                                </li>
                                <li class="menu-item">
                                    <a href="Register.jsp">Đăng ký</a>
                                </li>
                            </ul>
                        </div>
                        <% } else { %> 
                        <div class="footer-menu">
                            <h5>Tài khoản</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="navbar?go=profile">Hồ sơ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="navbar?go=logout" ">Đăng xuất</a>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                    </div>
                    <div class="col-md-2">

                        <div class="footer-menu">
                            <h5>Trợ giúp</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="https://www.facebook.com/profile.php?id=61552780627883">Hỗ trợ bằng page</a>
                                </li>
                                <li class="menu-item">
                                    <a href="https://www.facebook.com/profile.php?id=61552780627883">Liên lạc với chúng tôi</a>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>
                <!-- / row -->

            </div>
        </footer>

        <div id="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="copyright">
                            <div class="row">

                                <div class="col-md-6">
                                </div>

                                <div class="col-md-6">
                                    <div class="social-links align-right">
                                        <ul>
                                            <li>
                                                <i class="icon icon-facebook"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-twitter"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-youtube-play"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-behance-square"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div><!--grid-->

                    </div><!--footer-bottom-content-->
                </div>
            </div>
        </div>
    </body>
</html>
