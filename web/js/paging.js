/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

let thisPage = 1;
let limit = 5;
let list = document.qutorAll('.table-wrapper .table.table-striped tbody tr');
function loadItem(){
    let beginGet = limit - (thisPage - 1); // 5 sp - (trang 1 - 1) = 5
    let endGet = limit * thisPage - 1; // 5 sp * trang 1 - 1 = 4
    list.forEach((item, key) => {
        if(key >= beginGet && key <= endGet){ // tung sp 1 nam trong beginget va endget
            item.style.display = 'block'; // display
        }else{
            item.style.display = 'none'; // kerySelec display
        }
    });
    listPage();
}
loadItem();

function listPage(){
    let count = Math.ceil(list.length / limit);
    document.querySelector('.pagination').innerHTML = '';
    
    if(thisPage != 1){
        let prev = document.createElement('li');
        prev.innerText = 'Previous';
        prev.setAttribute('onclick', "changePage(" + (thisPage - 1) + ")");
        document.querySelector('pagination').appendChild(prev);
    }
    for (var i = 1; i <= count; i++) {
        let newPage = document.createElement('li');
        newPage.innerText = i;
        if(i == thisPage){
            newPage.classList.add('active');
        }
        newPage.setAttribute('onclick', "changePage(" + i + ")");
        document.querySelector('.pagination').appendChild(newPage);
    }
    if(thisPage != count){
        let next = document.createElement('li');
        next.innerText = 'Next';
        next.setAttribute('onclick', "changePage(" + (thisPage + 1) + ")");
        document.querySelector('pagination').appendChild(next);
    }
}
function changePage(i){
    thisPage = i;
    loadItem();
}

