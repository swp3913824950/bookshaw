<%-- 
    Document   : UpdateDate.jsp
    Created on : Oct 19, 2023, 12:19:59 AM
    Author     : staytogether
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <c:set var="d" value="${updateOrder}"/>
        <div class="alert alert-danger" role="alert">
            ${msg}
        </div>
        <div id="editEmployeeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="updateorder" method="post">
                        <div class="modal-header">						
                            <h4 class="modal-title">Chỉnh sửa ngày ship</h4>
                        </div>
                        <div class="modal-body">	
                            <div class="form-group">
                                <label>OrderID</label>
                                <input type="number" name="orderid" value="${d.orderID}" class="form-control" required readonly="">
                            </div>
                            <div class="form-group">
                                <label>Địa chỉ giao hàng</label>
                                <input type="text" name="address" value="${d.shipAddress}" class="form-control" required readonly="">
                            </div>
                            <div class="form-group">
                                <label>Ngày đặt hàng</label>
                                <input type="date" name="orderDate" value="${d.orderDate}" class="form-control" required readonly="">
                            </div>
                            <div class="form-group">
                                <label>Ngày yêu cầu đặt hàng</label>
                                <input type="date" name="requiredDate" value="${d.requiredDate}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Ngày ship hàng</label>
                                <input type="date" name="shippedDate" value="${d.shippedDate}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Trạng thái</label>
                                <select name="status" class="form-select" aria-label="Default select example">
                                    <c:forEach var="s" items="${stalist}">
                                         <c:if test="${(s.status ne 'Received')}">
                                            <option value="${s.status}" ${s.status eq d.status ? 'selected' : ''}>${s.status}</option>
                                        </c:if>
                                    </c:forEach>                                     
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="Cancel" href="order">Hủy</a>
                            <input type="submit" class="btn btn-info" name="button" value="Lưu">
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </body>
</html>
