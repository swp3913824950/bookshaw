<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta charset="utf-8">
        <title>Login Form</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="fonts/linearicons/style.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="wrapper">
            <div class="inner">
                <img src="images/image-1.png" alt="" class="image-1">
                <form action="forgotPass" method="post" id="loginForm">
                    <h3>Quên mật khẩu</h3>
                    <div class="form-holder">
                        <span class="lnr lnr-user"></span>
                    <input type="text" class="form-control" placeholder="username" name = "user" required="">
                    </div>
                    <div class="form-holder">
                        <span class="lnr lnr-user"></span>
                        <input type="text" name="email" id="username" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="login-button">
                        <button type="submit" name="submit">Đổi mật khẩu</button>
                    </div>
                    <br>
                    <div>
                        <a href="Login.jsp">Đăng nhập</a>
                    </div>
                    ${check}
                </form>
                <img src="images/image-2.png" alt="" class="image-2">
            </div>
        </div>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
