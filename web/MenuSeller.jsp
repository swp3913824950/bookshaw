<%-- 
    Document   : MenuSeller
    Created on : Oct 3, 2023, 5:58:48 PM
    Author     : FPT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style>
            /* CSS để tạo menu */
            ul.menu {
                list-style-type: none;
                margin: 0;
                padding: 0;
                background-color: #333; /* Màu nền của menu */
                overflow: hidden; /* Ngăn các phần tử con trôi theo chiều dọc */
            }

            ul.menu li {
                float: left; /* Hiển thị các mục liên kết theo chiều ngang */
            }

            ul.menu li a {
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

            ul.menu li a:hover {
                background-color: #555; /* Màu nền khi di chuột qua */
            }
        </style>
    </head>
    <body>
        <ul class="menu">
            <li><a href="listProduct">Danh sách sản phẩm</a></li>
            <li><a href="MonthStatics">Thống kê theo tháng trong năm</a></li>
            <li><a href="order">Hóa đơn</a></li>
            <li><a href="OrderFback">Phản hồi của khách hàng</a></li>
            <li><a href="navbar?go=profile" >Hồ sơ</a></li>
            <li><a href="navbar?go=logout">Đăng xuất</a></li>
        </ul>
    </body>
</html>
