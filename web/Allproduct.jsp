<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "Entity.*" %>
<%@page import = "Model.*" %>
<%@page import = "java.util.*" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>BookSaw - Free Book Store HTML CSS Template</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta name="description" content="">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">

        <link rel="stylesheet" type="text/css" href="css/normalize.css">
        <link rel="stylesheet" type="text/css" href="icomoon/icomoon.css">
        <link rel="stylesheet" type="text/css" href="css/vendor.css">
        <link rel="stylesheet" type="text/css" href="style.css">
        <style>
            .product-item {
                width: 300px;
                height: 400px;
            }
            .pagination {
                display: flex;
                justify-content: center;
                list-style: none;
            }

            .pagination a {
                color: black;
                text-decoration: none;
                padding: 5px 10px;
                border: 1px solid #ddd;
            }

            .pagination a.active {
                background-color: #4CAF50;
                color: white;
            }

            .pagination a:hover:not(.active) {
                background-color: #ddd;
            }
        </style>
    </head>

    <body data-bs-spy="scroll" data-bs-target="#header" tabindex="0">

        <%@include file="Header.jsp" %>

        <section id="featured-books" class="py-5 my-5">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <div class="col-md-5 align-items-start" style="margin-left: 64%">
                            <div style="display: flex;">
                                <% String sortProduct = request.getParameter("sort");
                                if (sortProduct == null) sortProduct = "Default";
                                String filterProduct = request.getParameter("cateID");
                                if (filterProduct == null) filterProduct = "0"; 
                                List<Category> listCate = (List<Category>) request.getAttribute("listCate");
                                %>
                                <form action="Allproduct" style="margin-left: 10px;">
                                    <div>
                                        <select name="sort">
                                            <option value="Default">Mặc định</option>                               
                                            <option value="ascprice" <%= sortProduct.equals("ascprice") ? "selected" : "" %>>Giá từ thấp đến cao</option>
                                            <option value="descprice" <%= sortProduct.equals("descprice") ? "selected" : "" %>>Giá từ cao đến thấp</option>
                                            <option value="latest" <%= sortProduct.equals("latest") ? "selected" : "" %>>Mới nhất</option>
                                            <option value="oldest" <%= sortProduct.equals("oldest") ? "selected" : "" %>>Cũ nhất</option> 
                                        </select>
                                        <input type="hidden" id="filterHiddenInput" name="cateID" value="<%= filterProduct %>">

                                        <input type="submit" value="Tìm kiếm">
                                    </div>
                                </form> 
                                <form action="Allproduct" style="margin-left: 10px;">
                                    <div>
                                        <select name="cateID">
                                            <option value="0" <%= filterProduct.equals("0") ? "selected" : "" %>>Mặc định</option>
                                            <%for(Category cate:listCate){%>
                                            <option value="<%=cate.getCateID()%>"
                                                    <%= filterProduct.equals(String.valueOf(cate.getCateID())) ? "selected" : "" %>>
                                                <%=cate.getCateName()%>
                                            </option>
                                            <%}%>
                                        </select>
                                        <input type="hidden" id="sortHiddenInput" name="sort" value="<%= sortProduct %>">

                                        <input type="submit" value="Tìm kiếm">
                                    </div>
                                </form>
                            </div>


                        </div>
                        <div class="section-header align-center">
                            <div class="title">
                                <span>Some quality items</span>
                            </div>
                            <h2 class="section-title" style="color: red">Danh sách sản phẩm </h2>
                        </div>
                        <div class="col-md-7 offset-sm-1"></div>

                    </div>
                    <div class="product-list" data-aos="fade-up">
                        <div class="row">
                            <c:forEach items="${listFeature}" var="c">
                                <div class="col-md-3">
                                    <div class="product-item" style="margin-bottom: 200px;">
                                        <figure class="product-style">
                                            <a href="detail?productID=${c.productID}"><img src="${c.image_url}" alt="Books" class="product-item"></a>
                                            <button type="button" class="add-to-cart" data-product-tile="add-to-cart">
                                                <a href="cart?id=${c.productID}">Add to
                                                    Cart</a></button>
                                        </figure>
                                        <figcaption>
                                            <h3><a href="detail?productID=${c.productID}">${c.proName}</a></h3>
                                            <span>${c.authorName}</span>
                                            <div class="item-price">${c.price} VNĐ  </div>
                                        </figcaption>
                                    </div>
                                </div>
                            </c:forEach>

                        </div><!--ft-books-slider-->
                        <ul class="pagination">
                            <c:forEach begin="1" end="${endP}" var="i">
                                <li>
                                    <a href="Allproduct?index=${i}" class="${tag == i ? 'active' : ''}">${i}</a>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>


                </div>

            </div>

        </section>




        <footer id="footer">
            <div class="container">
                <div class="row">

                    <div class="col-md-4">

                        <div class="footer-item">
                            <div class="company-brand">
                                <img src="images/main-logo.png" alt="logo" class="footer-logo">
                                <p>Đây là một trang web bán sách với mục tiêu và mong muốn người dùng có được trải nghiệm tốt nhất..</p>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-2">

                        <div class="footer-menu">
                            <h5>Khám phá</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="home">Trang chủ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="Allproduct">Sách</a>
                                </li>
                                <li class="menu-item">
                                    <a href="Allproduct"> Thể loại</a>
                                </li>
                                <li class="menu-item">
                                    <a href="#"> Tìm kiếm</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-md-2">
                        <% if (role == null) { %>
                        <div class="footer-menu">
                            <h5>Tài khoản</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="Login.jsp">Đăng nhập </a>
                                </li>
                                <li class="menu-item">
                                    <a href="Register.jsp">Đăng ký</a>
                                </li>
                            </ul>
                        </div>
                        <% } else { %> 
                        <div class="footer-menu">
                            <h5>Tài khoản</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="navbar?go=profile">Hồ sơ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="navbar?go=logout" ">Đăng xuất</a>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                    </div>
                    <div class="col-md-2">

                        <div class="footer-menu">
                            <h5>Trợ giúp</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="https://www.facebook.com/profile.php?id=61552780627883">Hỗ trợ bằng page</a>
                                </li>
                                <li class="menu-item">
                                    <a href="https://www.facebook.com/profile.php?id=61552780627883">Liên lạc với chúng tôi</a>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>
                <!-- / row -->

            </div>
        </footer>

        <div id="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="copyright">
                            <div class="row">

                                <div class="col-md-6">
                                </div>

                                <div class="col-md-6">
                                    <div class="social-links align-right">
                                        <ul>
                                            <li>
                                                <a href="#"><i class="icon icon-facebook"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="icon icon-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="icon icon-youtube-play"></i></a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="icon icon-behance-square"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div><!--grid-->

                    </div><!--footer-bottom-content-->
                </div>
            </div>
        </div>

        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"
                integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm"
        crossorigin="anonymous"></script>
        <script src="js/plugins.js"></script>
        <script src="js/script.js"></script>
        <script>
            document.querySelectorAll('.tabs a').forEach(tabLink => {
                tabLink.addEventListener('click', function (event) {
                    const scrollPosition = window.scrollY;
                    localStorage.setItem('scrollPosition', scrollPosition);
                });
            });

            window.addEventListener('load', () => {
                const savedScrollPosition = localStorage.getItem('scrollPosition');
                if (savedScrollPosition) {
                    window.scrollTo(0, savedScrollPosition);
                    localStorage.removeItem('scrollPosition');
                }
            });
        </script>
    </body>

</html>