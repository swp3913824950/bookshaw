<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <meta charset="utf-8">
        <title>Đăng nhập</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="fonts/linearicons/style.css">
        <link rel="stylesheet" href="css/style.css">
    </head>
    <body>
        <div class="wrapper">
            <div class="inner">
                <img src="images/image-1.png" alt="" class="image-1">
                <form action="${pageContext.request.contextPath}/login" method="post" id="loginForm">
                    <% String check = (String) request.getAttribute("check");
                      if(check != null){
                    %>
                    <div class="alert alert-danger" role="alert">
                        ${check}
                    </div>
                    <%}%>
                    <h3>Đăng nhập</h3>
                    <div class="form-holder">
                        <span class="lnr lnr-user"></span>
                        <input type="text" name="username" id="username" class="form-control" placeholder="Username" required>
                    </div>
                    <div class="form-holder">
                        <span class="lnr lnr-lock"></span>
                        <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                    </div>
                    <div class="error" style="color:red"> ${error} </div>    
                    <div class="login-button">
                        <button type="submit" name="submit">Đăng nhập</button>
                    </div>
                    <br>
                    <a href="https://accounts.google.com/o/oauth2/auth?scope=email%20profile
                       &redirect_uri=http://localhost:9999/PROJECTSWP391/LoginGoogleHandler&response_type=code
                       &client_id=171124682983-5hc33gbcj7td13bl6qad9ergpa9cmvdl.apps.googleusercontent.com&approval_prompt=force"><img src="https://i.pinimg.com/originals/74/65/f3/7465f30319191e2729668875e7a557f2.png" width="20px" alt="alt"/>Đăng nhập với google</a>
                    <p>Chưa có tài khoản? <a href="${pageContext.request.contextPath}/navbar?go=register" class="register">Đăng ký</a></p>
                    <a href="forgotPass.jsp" class="register">Quên mật khẩu?</a>
                </form>
                <img src="images/image-2.png" alt="" class="image-2">
            </div>
        </div>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
