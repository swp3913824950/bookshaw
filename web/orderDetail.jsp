<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <title>Manage Order</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="css/style2.css">
        <style>
            .thead-primary thead th{
                font-weight: bold;

            }
        </style>
    </head>
    <body>
        <%@include file="MenuSeller.jsp" %>
        <section class="ftco-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 mb-4">
                        <h1 class="heading-section text-center">Chi tiết đơn hàng</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <% String viewStatus = request.getParameter("viewstatus");
if (viewStatus == null) viewStatus = "All";
                        %>
                        <div class="table-wrap">
                            <form action="order" method="get">

                                <table class="table">
                                    <thead class="thead-primary text-center">
                                        <tr class="text-center">
                                            <th>ProductID</th>
                                            <th>OrderID</th>
                                            <th>Giá tiền</th>
                                            <th>Số lượng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="o" items="${detail}">
                                            <tr class="alert text-center" role="alert">
                                                <td>
                                                    ${o.productID}
                                                </td>
                                                <td>${o.orderID}</td>
                                                <td>${o.price}</td>
                                                <td>${o.quantity}</td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</html>