<%-- 
    Document   : Header
    Created on : Sep 19, 2023, 3:42:15 AM
    Author     : FPT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "Entity.*" %>
<%@page import = "Model.*" %>
<%@page import = "java.util.*" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Header</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">

        <link rel="stylesheet" type="text/css" href="css/normalize.css">
        <link rel="stylesheet" type="text/css" href="icomoon/icomoon.css">
        <link rel="stylesheet" type="text/css" href="css/vendor.css">
        <link rel="stylesheet" type="text/css" href="style.css">       
    </head>
    <body>
        <div id="header-wrap">

            <div class="top-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="social-links">
                                <ul>
                                    <li>
                                        <a href="#"><i class="icon icon-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon icon-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon icon-youtube-play"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon icon-behance-square"></i></a>
                                    </li>
                                </ul>
                            </div><!--social-links-->
                        </div>
                        <div class="col-md-6">
                            <div class="right-element">
                                <div class="account">
                                    <ul>
                                        <% String role = (String) session.getAttribute("role"); 
                                           Account account = (Account) session.getAttribute("account");
                                           if (role != null) { %>
                                        <a href="navbar?go=profile" class="user-account for-buy"><i
                                                class="icon icon-user"></i><span>Hồ sơ</span></a>
                                        <a href="navbar?go=logout" "><i
                                                class="icon icon-cancel"></i><span>Đăng xuất</span></a>

                                        <% } else { %> 
                                        <a href="navbar?go=login" class="user-account for-buy"><i
                                                class="icon icon-user"></i><span>Đăng nhập</span></a>
                                        <a href="navbar?go=register"><i
                                                class="icon icon icon-user"></i><span>Đăng ký</span></a>
                                                <% } %>
                                    </ul>
                                </div>
                                <c:if test="${sessionScope.role != 'Admin' && sessionScope.role != 'Nhân viên bán hàng'}">
                                    <c:if test="${sessionScope.role != null}">
                                        <a href="Cart.jsp" class="cart for-buy"><i class="icon icon-clipboard"></i><span>Giỏ hàng</span></a>
                                        <a href="home?go=order&id=<%=account.getAccountID()%>" style="margin-right: 20px"><i
                                                class="icon icon-clipboard"></i><span>Đơn hàng</span></a> 
                                            </c:if>

                                    <!--Search-->
                                    <div class="action-menu">
                                        <div class="search-bar">
                                            <a href="#" class="search-button search-toggle" data-selector="#header-wrap">
                                                <i class="icon icon-search" style="padding-top: 20px"></i>
                                            </a>
                                            <form action="${pageContext.request.contextPath}/SearchProduct" role="search" method="post" class="search-box" >
                                                <input class="search-field text search-input" placeholder="Search" type="search" name="search-box">
                                            </form>
                                        </div>
                                    </div>
                                </c:if>
                            </div><!--top-right-->
                        </div>

                    </div>
                </div>
            </div><!--top-content-->

            <header id="header">
                <div class="container-fluid">
                    <div class="row">
                        <c:if test="${sessionScope.role != 'Admin' && sessionScope.role != 'Nhân viên bán hàng'}">
                            <div class="col-md-2">
                                <div class="main-logo">
                                    <a href="home"><img src="images/main-logo.png" alt="logo"></a>
                                </div>
                            </c:if>
                            <c:if test="${sessionScope.role == 'Admin'}">
                                <div class="col-md-2">
                                    <div class="main-logo">
                                        <a href="dashboard"><img src="images/main-logo.png" alt="logo"></a>
                                    </div>
                                </c:if>
                            <c:if test="${sessionScope.role == 'Nhân viên bán hàng'}">
                                <div class="col-md-2">
                                    <div class="main-logo">
                                        <a href="listProduct"><img src="images/main-logo.png" alt="logo"></a>
                                    </div>
                            </c:if>
                            </div>

                            <div class="col-md-10">
                                <nav id="navbar">
                                    <div class="main-menu stellarnav">
                                        <ul class="menu-list">
                                            <c:if test="${sessionScope.role != 'Admin' && sessionScope.role != 'Nhân viên bán hàng'}">
                                                <li class="menu-item active"><a href="home">Trang chủ</a></li>
                                                <li class="menu-item">
                                                    <a href="https://www.facebook.com/profile.php?id=61552780627883" class="nav-link">Pages</a>
                                                </li>
                                                <li class="menu-item"><a href="#featured-books" class="nav-link">Sản phẩm tiêu biểu</a></li>
                                                <li class="menu-item"><a href="Allproduct" class="nav-link">Tất cả sản phẩm</a>
                                                </li>
                                                <%DAOCategory headerDao = new DAOCategory();
                                            
                                                List<Category> cateList = headerDao.getAllCategory();%>
                                                <li class="menu-item has-sub">

                                                    <a href="Allproduct" class="nav-link">Thể loại</a>
                                                    <ul>
                                                        <%for(Category cate:cateList){%>
                                                        <li><a href="Allproduct?cateID=<%=cate.getCateID()%>&sort=Default"><%=cate.getCateName()%> </a></li>
                                                            <%}%>
                                                    </ul>

                                                </li>
                                                <li class="menu-item"><a href="support" class="nav-link">Hỗ trợ</a></li>
                                                <li class="menu-item"><a href="ChatBox.jsp" class="">ChatBox</a></li>
                                            </ul>
                                        </c:if>
                                        <div class="hamburger">
                                            <span class="bar"></span>
                                            <span class="bar"></span>
                                            <span class="bar"></span>
                                        </div>

                                    </div>
                                </nav>

                            </div>

                        </div>
                    </div>
            </header>

        </div><!--header-wrap-->

        <style>

            .menu-item ul {
                max-width: 200px; /* Đặt giá trị tối đa cho chiều rộng của danh sách con */
            }
            .menu-item ul {
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between; /* Chia dãy thành 4 phần tử mỗi dãy */
            }
            .menu-item ul li {
                flex: 0 0 calc(25% - 10px); /* Đặt chiều rộng cố định cho mỗi phần tử và để lại khoảng cách */
            }

        </style>


    </body>
</html>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $(document).ready(function () {
        // Lấy tất cả các mục menu trong danh sách
        var menuItems = $(".menu-item");

        // Xử lý sự kiện khi mục menu được nhấp
        menuItems.click(function () {
            // Loại bỏ lớp "active" từ tất cả các mục menu
            menuItems.removeClass("active");
            // Thêm lớp "active" cho mục menu được nhấp
            $(this).addClass("active");

            // Chuyển hướng tới servlet hoặc URL tương ứng
            var href = $(this).find("a").attr("href");
            if (href) {
                window.location.href = href;
            }
        });
    });
</script>