<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Update Products</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="css/manager.css" rel="stylesheet" type="text/css"/>
        <link rel="icon" type="image/x-icon" href="assets/icons8-favicon-50.png" />
        <style>

            .cancel a{
                font: bold 11px Arial;
                text-decoration: none;
                background-color: white;
                color: black ;
                padding: 2px 6px 2px 6px;
                border-top: 1px solid #CCCCCC;
                border-right: 1px solid #333333;
                border-bottom: 1px solid #333333;
                border-left: 1px solid #CCCCCC;
                width: 53.55px;
                height: 26.6px;
                display: -webkit-inline-box;
                line-height: 21.6px;
            }

        </style>
    </head>
    <body>
        <!-- Edit Modal HTML -->
        <div id="editEmployeeModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="edit" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">
                        <div class="modal-header">						
                            <h4 class="modal-title">Chỉnh sửa sản phẩm</h4>
                        </div>
                        <div class="modal-body">					
                            <div class="form-group">
                                <label>ID</label>
                                <input type="text" name="id" value="${detailProduct.productID}" class="form-control" readonly required>
                            </div>
                            <div class="form-group">
                                <label>Tên sản phẩm</label>
                                <input type="text" name="productName" value="${detailProduct.proName}" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Ảnh</label><br/>
                                <img src="${detailProduct.image_url}" width="180px"alt="alt"/>
                                <input type="file" name="image" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tác giả</label>
                                <input type="text" name="auName" value="${detailProduct.authorName}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Giá</label>
                                <input type="text" name="price" value="${detailProduct.price}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Số lượng</label>
                                <input type="text" name="quantity" value="${detailProduct.quantity}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Ngày nhập</label>
                                <input type="date" name="date" value="${detailProduct.maniDate}" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Thể loại</label>
                                <select name="category" class="form-select" aria-label="Default select example">
                                    <c:forEach items="${listCate}" var="o">
                                        <option value="${o.cateID}" ${detailProduct.categoryID eq o.cateID ? 'selected' : ''}>${o.cateName}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="Cancel" href="listProduct">Hủy</a>
                            <input type="submit" class="btn btn-info" name="button" value="Lưu">
                        </div>
                        <p>${msg}</p>
                    </form>    
                </div>
            </div>
        </div>
    </body>
</html>
<script>
    function validateForm() {
        var quantity = document.getElementsByName("quantity")[0].value;
        var price = document.getElementsByName("price")[0].value;
        var dateInput = document.getElementsByName("date")[0].value;

        var currentDate = new Date();
        var inputDate = new Date(dateInput);
        var twoYearsAgo = new Date();
        twoYearsAgo.setFullYear(currentDate.getFullYear() - 2);

        // Check if quantity is a valid number
        if (isNaN(quantity) || quantity < 0) {
            alert("Số lượng phải >= 0 và phải là số nguyên dương");
            return false; // prevent form submission
        }
        
        // Check if price is a valid number
        if (isNaN(price) || price < 1000) {
            alert("Giá phải >= 1000 và phải là số nguyên dương");
            return false; // prevent form submission
        }

        if (inputDate > currentDate || inputDate < twoYearsAgo) {
            alert("Ngày nhập phải là ngày hiện tại hoặc ngày trong quá khứ nhưng không được quá 2 năm");
            return false; // prevent form submission
        }

        return true; // allow form submission
    }
</script>
