<%@ page contentType="text/html;charset=UTF-8" language="java" import="java.util.List" pageEncoding="UTF-8" %>
<html>

    <head>
        <meta charset="UTF-8">

        <style>
            body {
                font-family: Arial, sans-serif;
                background-color: BlanchedAlmond;
                margin: 0;
                padding: 0;
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                min-height: 100vh;
            }

            h2 {
                color: #333;
                text-align: center; /* Center the text */
            }

            p {
                margin: 5px 0;
            }

            form {
                margin-top: 20px;
            }

            label {
                display: block;
                margin-bottom: 5px;
            }

            input[type="text"] {
                width: 300px;
                padding: 5px;
            }

            input[type="submit"] {
                padding: 5px 10px;
                background-color: #4CAF50;
                color: #fff;
                border: none;
                cursor: pointer;
            }

            input[type="submit"]:hover {
                background-color: #45a049;
            }
            input[type="text"] {
                width: 500px; /* Độ rộng mới */
                height: 50px; /* Độ cao mới */
                padding: 10px;
                box-sizing: border-box;
            }
            body {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                min-height: 100vh; /* Đảm bảo nội dung ở giữa trang ngay cả khi nó không đủ lớn */
            }
        </style>
        <title>Chat History</title>
    </head>
    <body>
        <nav>
            <a href="home">Trang chủ</a>
            <a href="https://www.facebook.com/profile.php?id=61552780627883">Pages</a>
            <a href="Support.jsp">Hỗ trợ</a>
        </nav>
        <h2>Chat Box BookSaw:</h2>
        <%
            List<String> chatHistory = (List<String>) request.getAttribute("chatHistory");
            if (chatHistory != null) {
                for (String entry : chatHistory) {
        %>
        <p><%= entry %></p>
        <%
                }
            }
        %>

        <form action="ChatServlet" method="GET">
            <label for="message">Nhập câu hỏi:</label>
            <input type="text" name="message" id="message">
            <input type="submit" value="Gửi">
        </form>
    </body>
</html>
