<%-- 
    Document   : CustomerOrderDetail
    Created on : Oct 13, 2023, 6:05:50 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import = "Model.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

    </head>
    <body>
        <%@include file="Header.jsp" %>
        <div  style="border-bottom: 1px solid white;">
            <c:choose>
                <c:when test="${status eq 'Received' || status eq 'Rejected'}">
                    <table class="table table-success table-striped">
                        <thead>
                            <tr>
                                <th>ID sản phẩm</th>
                                <th>ID đơn hàng</th>
                                <th>Đơn giá</th>
                                <th>Số lượng</th>
                                <th>Bình luận</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="o" items="${olist}" varStatus="status">
                                <tr>
                                    <td><a href="detail?productID=${o.productID}">${o.productID}</td>
                                    <td>${o.orderID}</td>
                                    <td>${o.price} VNĐ</td>
                                    <td>${o.quantity}</td>
                                    <td>
                                        <c:set var="productID" value="${o.productID}" />
                                        <c:set var="customerID" value="${requestScope.customerID}" />
                                        <%
    DAOComment dao = new DAOComment();
    int customerID = (int) pageContext.getAttribute("customerID");
    int productID = (int) pageContext.getAttribute("productID");

    boolean ableToComment = dao.checkCommentPurchaseMatch(customerID, productID);
                                        %>


                                        <c:choose>
                                            <c:when test="<%=ableToComment%>">
                                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addCommentModal" data-product-id="${o.productID}">
                                                    Thêm bình luận
                                                </a>
                                            </c:when>
                                            <c:otherwise>

                                                <button class="btn btn-secondary" disabled>Đã bình luận</button>

                                            </c:otherwise>
                                        </c:choose>

                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:when>
                <c:otherwise>
                    <table class="table table-success table-striped">
                        <thead>
                            <tr>
                                <th>ID sản phẩm</th>
                                <th>ID đơn hàng</th>
                                <th>Đơn giá</th>
                                <th>Số lượng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="o" items="${olist}" varStatus="status">
                                <tr>
                                    <td><a href="detail?productID=${o.productID}">${o.productID}</td>
                                    <td>${o.orderID}</td>
                                    <td>${o.price} VNĐ</td>
                                    <td>${o.quantity}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </c:otherwise>
            </c:choose>


            <!-- Add Comment form -->
            <div class="modal fade" id="addCommentModal" tabindex="-1" role="dialog" aria-labelledby="addCommentModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form action="comment?go=add" method="post">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="addCommentModalLabel">Thêm bình luận</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <!-- Add your comment input fields here -->
                                <div class="form-group">
                                    <label for="comment">Bình luận</label>
                                    <textarea class="form-control" id="comment" name="comment" rows="4"></textarea>
                                </div>
                                <!-- Add input fields for other attributes -->
                                <div class="form-group">
                                    <label for="star">Đánh giá:</label>
                                    <select class="form-control" id="star" name="star">
                                        <option value="1">1 Sao</option>
                                        <option value="2">2 Sao</option>
                                        <option value="3">3 Sao</option>
                                        <option value="4">4 Sao</option>
                                        <option value="5">5 Sao</option>
                                    </select>
                                </div>
                                <input type="hidden" id="productID" name="productID">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Ðóng</button>
                                <button type="submit" class="btn btn-primary">Thêm bình luận</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <footer id="footer">
            <div class="container">
                <div class="row">

                    <div class="col-md-4">

                        <div class="footer-item">
                            <div class="company-brand">
                                <img src="images/main-logo.png" alt="logo" class="footer-logo">
                                <p>Đây là một trang web bán sách với mục tiêu và mong muốn người dùng có được trải nghiệm tốt nhất..</p>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-2">

                        <div class="footer-menu">
                            <h5>Khám phá</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="home">Trang chủ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="Allproduct">Sách</a>
                                </li>
                                <li class="menu-item">
                                    <a href="Allproduct"> Thể loại</a>
                                </li>
                                <li class="menu-item">
                                    <a href="#"> Tìm kiếm</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="col-md-2">
                        <% if (role == null) { %>
                        <div class="footer-menu">
                            <h5>Tài khoản</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="Login.jsp">Đăng nhập </a>
                                </li>
                                <li class="menu-item">
                                    <a href="Register.jsp">Đăng ký</a>
                                </li>
                            </ul>
                        </div>
                        <% } else { %> 
                        <div class="footer-menu">
                            <h5>Tài khoản</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="navbar?go=profile">Hồ sơ</a>
                                </li>
                                <li class="menu-item">
                                    <a href="navbar?go=logout" ">Đăng xuất</a>
                                </li>
                            </ul>
                        </div>
                        <% } %>
                    </div>
                    <div class="col-md-2">

                        <div class="footer-menu">
                            <h5>Trợ giúp</h5>
                            <ul class="menu-list">
                                <li class="menu-item">
                                    <a href="https://www.facebook.com/profile.php?id=61552780627883">Hỗ trợ bằng page</a>
                                </li>
                                <li class="menu-item">
                                    <a href="https://www.facebook.com/profile.php?id=61552780627883">Liên lạc với chúng tôi</a>
                                </li>
                            </ul>
                        </div>

                    </div>

                </div>
                <!-- / row -->

            </div>
        </footer>

        <div id="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="copyright">
                            <div class="row">

                                <div class="col-md-6">
                                </div>

                                <div class="col-md-6">
                                    <div class="social-links align-right">
                                        <ul>
                                            <li>
                                                <i class="icon icon-facebook"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-twitter"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-youtube-play"></i>
                                            </li>
                                            <li>
                                                <i class="icon icon-behance-square"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div><!--grid-->

                    </div><!--footer-bottom-content-->
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $('[data-toggle="modal"]').click(function () {
                    var target = $(this).data("target");


                    // Get the productID from the data attribute of the clicked element
                    var productID = $(this).data("product-id");


                    $('#productID').val(productID);
                    $(target).modal("show");
                });
            });
        </script>
    </body>
</html>
